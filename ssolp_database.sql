-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 09:49 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ssolp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `idCategory` int(11) NOT NULL,
  `name` varchar(90) CHARACTER SET utf8 NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`idCategory`, `name`, `activated`) VALUES
(1, 'Staro orodje', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Connection`
--

CREATE TABLE `Connection` (
  `idConnection` int(11) NOT NULL,
  `idKeyword1` int(11) NOT NULL,
  `idKeyword2` int(11) NOT NULL,
  `idTypeOfConnection` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Connection`
--

INSERT INTO `Connection` (`idConnection`, `idKeyword1`, `idKeyword2`, `idTypeOfConnection`) VALUES
(1, 1, 1, 5),
(3, 1, 2, 3),
(4, 1, 3, 3),
(6, 1, 4, 3),
(7, 1, 5, 3),
(8, 1, 6, 3),
(9, 1, 7, 3),
(11, 1, 8, 3),
(12, 1, 9, 3),
(2, 1, 10, 5),
(5, 1, 31, 5),
(10, 1, 75, 5),
(49, 2, 1, 2),
(50, 2, 1, 5),
(52, 2, 3, 1),
(51, 2, 10, 5),
(53, 2, 31, 5),
(54, 2, 75, 5),
(81, 3, 1, 2),
(82, 3, 1, 5),
(84, 3, 2, 1),
(83, 3, 10, 5),
(85, 3, 31, 5),
(86, 3, 75, 5),
(99, 4, 1, 2),
(100, 4, 1, 5),
(103, 4, 5, 1),
(101, 4, 10, 5),
(102, 4, 31, 5),
(104, 4, 75, 5),
(142, 5, 1, 2),
(143, 5, 1, 5),
(146, 5, 4, 1),
(144, 5, 10, 5),
(145, 5, 31, 5),
(147, 5, 75, 5),
(175, 6, 1, 2),
(176, 6, 1, 5),
(177, 6, 10, 5),
(178, 6, 31, 5),
(179, 6, 75, 5),
(208, 7, 1, 2),
(209, 7, 1, 5),
(210, 7, 10, 5),
(211, 7, 31, 5),
(212, 7, 75, 5),
(221, 8, 1, 2),
(222, 8, 1, 5),
(223, 8, 10, 5),
(224, 8, 31, 5),
(225, 8, 75, 5),
(226, 9, 1, 2),
(227, 9, 1, 5),
(228, 9, 10, 5),
(229, 9, 31, 5),
(230, 9, 75, 5),
(13, 10, 1, 4),
(14, 10, 7, 6),
(15, 10, 8, 6),
(16, 10, 9, 6),
(17, 11, 12, 6),
(18, 11, 14, 6),
(19, 11, 15, 6),
(20, 11, 30, 1),
(21, 12, 11, 6),
(22, 12, 14, 6),
(23, 12, 15, 6),
(24, 13, 16, 6),
(25, 14, 11, 6),
(26, 14, 12, 6),
(27, 14, 15, 6),
(28, 15, 11, 6),
(29, 15, 12, 6),
(30, 15, 14, 6),
(31, 16, 13, 6),
(32, 16, 16, 5),
(33, 16, 75, 5),
(34, 17, 17, 5),
(35, 17, 18, 3),
(36, 17, 19, 6),
(37, 17, 20, 3),
(38, 17, 21, 3),
(39, 17, 22, 3),
(40, 17, 24, 5),
(41, 17, 38, 5),
(42, 17, 39, 5),
(43, 18, 17, 2),
(44, 18, 17, 5),
(45, 18, 24, 5),
(46, 18, 38, 5),
(47, 18, 39, 5),
(48, 19, 17, 6),
(55, 20, 17, 2),
(56, 20, 17, 5),
(57, 20, 21, 1),
(58, 20, 22, 1),
(59, 20, 24, 5),
(60, 20, 38, 5),
(61, 20, 39, 5),
(62, 21, 17, 2),
(63, 21, 17, 5),
(64, 21, 20, 1),
(65, 21, 22, 1),
(66, 21, 24, 5),
(67, 21, 38, 5),
(68, 21, 39, 5),
(69, 22, 17, 2),
(70, 22, 17, 5),
(71, 22, 20, 1),
(72, 22, 21, 1),
(73, 22, 24, 5),
(74, 22, 38, 5),
(75, 22, 39, 5),
(76, 24, 17, 4),
(77, 26, 28, 4),
(78, 28, 26, 5),
(79, 28, 29, 5),
(80, 29, 28, 4),
(87, 30, 11, 1),
(88, 31, 1, 4),
(89, 32, 37, 6),
(90, 32, 40, 6),
(91, 32, 41, 6),
(92, 37, 32, 6),
(93, 37, 40, 6),
(94, 37, 41, 6),
(95, 38, 17, 4),
(96, 38, 39, 2),
(97, 39, 17, 4),
(105, 40, 32, 6),
(106, 40, 37, 6),
(107, 40, 41, 6),
(108, 41, 32, 6),
(109, 41, 37, 6),
(110, 41, 40, 6),
(111, 44, 44, 5),
(112, 44, 45, 5),
(113, 44, 46, 5),
(114, 44, 46, 6),
(115, 44, 57, 6),
(116, 44, 60, 5),
(117, 44, 61, 5),
(118, 44, 62, 5),
(119, 44, 64, 6),
(120, 44, 65, 6),
(121, 44, 67, 6),
(122, 44, 68, 6),
(123, 45, 44, 4),
(124, 45, 47, 6),
(125, 45, 48, 6),
(126, 45, 50, 6),
(127, 46, 44, 4),
(128, 46, 44, 6),
(129, 46, 57, 6),
(130, 46, 60, 5),
(131, 46, 64, 6),
(132, 46, 65, 6),
(133, 46, 67, 6),
(134, 46, 68, 6),
(135, 47, 45, 6),
(136, 47, 48, 6),
(137, 47, 50, 6),
(138, 48, 45, 6),
(139, 48, 47, 6),
(140, 48, 50, 6),
(141, 49, 49, 5),
(148, 50, 45, 6),
(149, 50, 47, 6),
(150, 50, 48, 6),
(151, 50, 58, 1),
(152, 50, 59, 1),
(153, 50, 59, 5),
(154, 51, 52, 6),
(155, 52, 51, 6),
(157, 54, 55, 5),
(158, 54, 56, 5),
(159, 54, 56, 6),
(160, 54, 66, 6),
(238, 54, 79, 5),
(161, 55, 39, 5),
(162, 55, 54, 4),
(163, 56, 54, 4),
(164, 56, 54, 6),
(165, 56, 66, 6),
(166, 57, 44, 6),
(167, 57, 64, 6),
(168, 57, 65, 6),
(169, 57, 67, 6),
(170, 57, 68, 6),
(171, 58, 50, 1),
(172, 58, 59, 5),
(173, 59, 50, 1),
(174, 59, 59, 5),
(180, 60, 44, 4),
(181, 61, 44, 4),
(182, 62, 44, 4),
(183, 63, 44, 4),
(184, 64, 44, 6),
(185, 64, 57, 6),
(186, 64, 65, 6),
(187, 64, 67, 6),
(188, 64, 68, 6),
(189, 65, 44, 6),
(190, 65, 57, 6),
(191, 65, 64, 6),
(192, 65, 67, 6),
(193, 65, 68, 6),
(194, 66, 54, 6),
(195, 66, 56, 6),
(196, 67, 44, 6),
(197, 67, 46, 6),
(198, 67, 57, 6),
(199, 67, 64, 6),
(200, 67, 65, 6),
(201, 67, 68, 6),
(202, 68, 44, 6),
(203, 68, 46, 6),
(204, 68, 57, 6),
(205, 68, 64, 6),
(206, 68, 65, 6),
(207, 68, 67, 6),
(213, 70, 70, 5),
(214, 73, 74, 3),
(215, 74, 73, 2),
(216, 75, 1, 4),
(217, 75, 16, 4),
(218, 75, 76, 4),
(219, 76, 1, 2),
(220, 76, 75, 5),
(235, 78, 37, 6),
(236, 78, 41, 6),
(234, 78, 77, 6),
(237, 79, 54, 4);

-- --------------------------------------------------------

--
-- Table structure for table `Keyword`
--

CREATE TABLE `Keyword` (
  `idKeyword` int(11) NOT NULL,
  `idSubcategory` int(11) NOT NULL,
  `word` varchar(63) CHARACTER SET utf8 NOT NULL,
  `pronunciation` varchar(63) CHARACTER SET utf8 NOT NULL,
  `grammar` varchar(63) CHARACTER SET utf8 NOT NULL,
  `meaning` varchar(255) CHARACTER SET utf8 NOT NULL,
  `etymology` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `Keyword`
--

INSERT INTO `Keyword` (`idKeyword`, `idSubcategory`, `word`, `pronunciation`, `grammar`, `meaning`, `etymology`, `activated`) VALUES
(1, 1, 'plenkača', 'plenkǻːča -e', 'ž', '1. plankača, plenkača |dvoročna tesarska sekira s tankim in dolgim rezilom ter ravnim držajem; pri tesanju z njo se stoji na tramu|; 2. železni del plenkače', 'Ⓘ (plankača <) planka, prek srvnem. planke, nem. Planke ‛deska, ograja’', 1),
(2, 1, 'plenkača za levega', 'plenkǻːča zə leˈga -e -ga', 'ž', 'plenkača za levičarja', NULL, 1),
(3, 1, 'leva plenkača', 'léːva plenkǻːča -e -e', 'ž', 'plenkača za levičarja', NULL, 1),
(4, 1, 'plenkača za desnega', 'plenkǻːča zə disənga -e -ga', 'ž', 'plenkača za desničarja', NULL, 1),
(5, 1, 'desna plenkača', 'disna plenkǻːča -e -e', 'ž', 'plenkača za desničarja', NULL, 1),
(6, 1, 'straničarka', 'stranìːčarka -e', 'ž', 'plankača, plenkača |dvoročna tesarska sekira s tankim in dolgim rezilom ter ukrivljenim držajem; pri tesanju z njo se stoji na tleh|', NULL, 1),
(7, 1, 'lična plenkača', 'líːčna plenkǻːča -e -e', 'ž', 'plenkača z ravnim licem ', NULL, 1),
(8, 1, 'tričetrt lična plenkača', 'trìːčetːrt líːčna plenkǻːča -e -e', 'ž', 'plenkača z rahlo upognjenim licem', NULL, 1),
(9, 1, 'pol lična plenkača', 'pùː líːčna plenkǻːča -e -e', 'ž', 'plenkača z upognjenim licem', NULL, 1),
(10, 1, 'lice', 'líːce -a', 's', 'notranja stran plenkače |stran, ki je pri tesanju ob tramu|', NULL, 1),
(11, 1, 'tesanje', 'tesːjne -a', 's', 'tesanje |delo, ki ga opravlja tesač|', NULL, 1),
(12, 1, 'tesač', 'təsǻːč -a', 'm', 'tesač |človek, ki se ukvarja s tesanjem lesa|', NULL, 1),
(13, 1, 'sekač', 'səkǻːč -a', 'm', 'sekač |človek, ki se ukvarja s sekanjem dreves v gozdu|', NULL, 1),
(14, 1, 'tesan', 'tesǻːn -a -u', 'prid.', 'tesan |obdelan s plenkačo|', NULL, 1),
(15, 1, 'tesati', 'tːsat tišem', 'nedov.', 'tesati |s plenkačo obdelovati hlode in izdelovati trame|', NULL, 1),
(16, 1, 'sekira', 'səkira -e', 'ž', '1. sekira |orodje za sekanje|; 2. železni del tega orodja', NULL, 1),
(17, 1, 'žaga', 'žǻːga -e', 'ž', 'žaga |1. orodje z nazobčanim listom za žaganje; 2. list žage|', 'Ⓘ stvnem. saga ‛žaga’', 1),
(18, 1, 'amerikanka', 'merikːjnka -e', 'ž', 'žaga za dva človeka z dvema snemljivima držaloma, rožičkoma', NULL, 1),
(19, 1, 'žagati', 'žːgat -am', 'nedov.', 'žagati |z žago podirati drevje ali delati kose, dele debla|', NULL, 1),
(20, 1, 'amerikanka samica', 'merikːjnka samíːca -e -e', 'ž', 'žaga za enega človeka, ki ima lesen ročaj na eni strani in snemljivo držalo, rožiček, na drugi ', NULL, 1),
(21, 1, 'samica', 'samíːca -e', 'ž', 'žaga za enega človeka, ki ima lesen ročaj na eni strani in snemljivo držalo, rožiček, na drug', NULL, 1),
(22, 1, 'spuščavnica', 'spüščːənca -e', 'ž', 'žaga za enega človeka, ki ima lesen ročaj na eni strani in snemljivo držalo, rožiček, na drugi ', NULL, 1),
(23, 1, 'kajla', 'kːjla -e ', 'ž', 'zagozda |proti enemu koncu zožujoč se kos kovine, lesa za cepljenje in podiranje dreves|', 'Ⓘ nem. Keil ‛klin, zagozda’', 1),
(24, 1, 'rožiček', 'rožìːčək -čka ', 'm', 'snemljivi držaj pri žagi', NULL, 1),
(25, 1, 'klamfa', 'klːmfa -e ', 'ž', 'penja, skoba |železna priprava za začasno spenjanje ali pritrjevanje lesenih delov|', 'Ⓘ bav. srvnnem. *klampfe > bav. nem. Klampfe ‛spona’', 1),
(26, 1, 'žnora', 'žnùọra -e', 'ž', 'obarvana vrvica za označevanje mere trama', 'Ⓘ srvnem. snour ‛vrvica, vez’', 1),
(27, 1, 'pisa', 'píːsa -e', 'ž', 'proga, črta, narejena z obarvano vrvico, žnoro', NULL, 1),
(28, 1, 'farbenka', 'fərbìẹnka -e', 'ž', 'posoda z barvo, v kateri se barva vrvica, žnora', 'Ⓘ nem. Farbe ‛barva’', 1),
(29, 1, 'ročka', 'ručka -e', 'ž', 'ročaj, roč pri posodi za barvo, farbenki', NULL, 1),
(30, 1, 'tramarija', 'tramarìːja -e', 'ž', 'tesanje |delo, ki ga opravlja tesač|', 'Ⓘ srvnem. trām, drām ‛tram, bruno, zapah’, iz česar se je razvilo današnje bav. nem. Tram, Traum, Dram ‛tram, bruno, hlod’', 1),
(31, 1, 'rajf', 'rːjf -a', 'm', 'ostrina plenkače', NULL, 1),
(32, 1, 'brusiti', 'brǘːsət -əm', 'nedov.', 'brusiti |ostriti z oslo|', NULL, 1),
(33, 1, 'meter', 'mːtər -tra', 'm', 'meter |priprava za merjenje iz spetih in zložljivih tankih lesenih deščic z označenimi centimetri, decimetri, metri, navadno dolga 1 do 2 m|', NULL, 1),
(34, 1, 'banka', 'bːnka -e', 'ž', 'banjka |ploščata lesena posoda za prenašanje tekočin|', NULL, 1),
(35, 1, 'kubikpikelj', 'kubìːkpíːkəl -na', 'm', 'knjižica za vpisovanje mer in okroglin tramov', 'Ⓘ nem. Kubus in lat. cubus iz gr. kýbos ‛kocka’; Ⓘpikelj nejasno', 1),
(36, 1, 'pralica', 'prǻːlca -e', 'ž', 'orodje za odstranjevanje lubja z drevesnih debel', NULL, 1),
(37, 1, 'brusač', 'brüsǻːč -a', 'm', 'brusač, brusilec |človek, ki brusi rezila|', NULL, 1),
(38, 1, 'kočnik', 'kučnək~kučnk -a', 'm', 'nižji, čistilni zob pri žagah za podiranje in razžagovanje dreves', NULL, 1),
(39, 1, 'zob', 'zùọp zobː', 'm', '1. zob |koničast del na listu žage|; 2. višji, rezilni zob pri žagah za podiranje in razžagovanje dreves ', NULL, 1),
(40, 1, 'nabrusiti', 'nebrǘːsət -əm', 'dov.', 'nabrusiti |narediti ostro|', NULL, 1),
(41, 1, 'pobrusiti', 'pobrǘːsət -əm', 'dov.', 'pobrusiti |nekoliko obrusiti|', NULL, 1),
(42, 1, 'mašinica', 'mašìːnca -e', 'ž', 'priprava za brušenje žag', NULL, 1),
(43, 1, 'pila', 'píːla -e', 'ž', 'pila |orodje za brušenje|', NULL, 1),
(44, 2, 'kosa', 'kːsa kosìẹ', 'ž', '1. kosa |orodje za košenje trave, detelje, krompirjevke itd.|; 2. železni del tega orodja', NULL, 1),
(45, 2, 'klep', 'klìẹp -a', 'm', 'ostrina kose, rezilo', NULL, 1),
(46, 2, 'kosje', 'kúːsje -a', 's', 'kosišče |leseni del kose| ', NULL, 1),
(47, 2, 'sklepati', 'sklːpat skliplem', 'dov.', 'sklepati |z udarci kladiva stanjšati, narediti rezilo kose|', NULL, 1),
(48, 2, 'klepati', 'klːpat kliplem', 'nedov.', 'klepati |z udarci kladiva tanjšati, delati  rezilo kose|', NULL, 1),
(49, 2, 'kladivo', 'klǻːdu -dva', 'm', '1. kladivo |orodje za klepanje|; 2. železni del tega orodja Ⓚ Obstajata dve vrsti: z ravnim in zožanim udarjalnim delom; s prvim se kleplje na zgoraj zožani, z drugim na ravni babici.', NULL, 1),
(50, 2, 'klepanje', 'klepǻːjne -a ', 's', 'klepalnik |naprava za klepanje| Ⓚ Obstajata dve vrsti: za klepanje na tleh in na stolčku.', NULL, 1),
(51, 2, 'oselnik', 'usonək -a ', 'm', 'oselnik |posoda z vodo za shranjevanje osle pri košnji|', NULL, 1),
(52, 2, 'osla', 'ːsla oslìẹ', 'ž', 'osla |podolgovat kos brusilnega kamna za brušenje kose, srpa itd.|', NULL, 1),
(53, 2, 'ognjilo', 'ogníːlu -a', 's', 'jeklena naprava za poravnavanje ostrine, klepa pri kosi', NULL, 1),
(54, 2, 'grablje', 'grǻːble grːbəl', 'ž mn.', 'grablje |orodje za grabljenje itd.|', NULL, 1),
(55, 2, 'čeljust', 'čelǜːst -ə', 'ž', 'čeljust |del grabelj z vstavljenimi zobmi|', NULL, 1),
(56, 2, 'grabljišče', 'grǻːbəlšče -a ', 's', 'grabljišče |držaj pri grabljah|', NULL, 1),
(57, 2, 'kosec', 'kːsəc -sca ', 'm', 'kosec |moški, ki kosi travo|', NULL, 1),
(58, 2, 'kuzla', 'kùːzla -e ', 'ž', 'klepalnik |priprava iz stolčka in babice, namenjena za klepanje|', NULL, 1),
(59, 2, 'babica', 'bǻːpca -e ', 'ž', '1. zgornji, železni del klepalnika, kuzle; 2.klepalnik |priprava za klepanje, ki se zabije v tla|', NULL, 1),
(60, 2, 'ročaj', 'ročːj -a ', 'm', 'ročaj, držaj (lahko dva različna) na kosišču, kosju', NULL, 1),
(61, 2, 'rink', 'ˈrənk ríːnka', 'm', 'obroč pri kosi, ki se nasadi na kosišče, kosje', 'Ⓘ nem. Ring ‛obroč’', 1),
(62, 2, 'šrajf', 'šrːjf -a ', 'm', 'vijak za pritrditev kose na kosišče, kosje', 'Ⓘ avstr. nem. Schraufe, Schraufe ‛vijak’', 1),
(63, 2, 'zagozda', 'zegùːjzda -e ', 'ž', 'zagozda |proti enemu koncu zožujoč se kos kovine, lesa za cepljenje, trdno nameščanje česa|', NULL, 1),
(64, 2, 'kositi', 'kːsət kosíːm', 'nedov.', 'kositi |s koso, kosilnico rezati travo, žito|', NULL, 1),
(65, 2, 'kosica', 'kosíːca -e ', 'ž', 'kosica |ženska, ki kosi (travo, žito)|', NULL, 1),
(66, 2, 'grabljica', 'grablíːca -e ', 'ž', 'grabljica |ženska, ki grabi| ', NULL, 1),
(67, 2, 'košnja', 'kːšna -e ', 'ž', 'košnja |1. čas, ko se kosi; 2. delo s koso|', NULL, 1),
(68, 2, 'kosilnica', 'kosìːlca -e ', 'ž', 'kosilnica |stroj za košenje|', NULL, 1),
(69, 2, 'red', 'rìẹt redìː', 'ž', 'red |pokošena trava v vrsti, kakršna nastaja ob košenju|', NULL, 1),
(70, 2, 'vile', 'víːle vìːl', 'ž mn.', '1. vile |orodje z roglji in dolgim držajem za nabadanje, premetavanje trave, gnoja|; 2. železni del vil', NULL, 1),
(71, 2, 'kopica', 'kopíːca -e ', 'ž', 'kopica |kup sena za čez noč|', NULL, 1),
(72, 2, 'ostrnica', 'ostərníːca -e', 'ž', '1. ostrnica |tanjše smrekovo deblo, ki se zabije v zemljo in uporablja za sušenje trave|; 2. na tanjše smrekovo deblo s prisekanimi vejami navešena trava za sušenje na travniku, njivi ', NULL, 1),
(73, 2, 'lojtre', 'lùːjtre -tər', 'ž mn.', 'lestev |priprava s prečnimi klini ali deščicami za vzpenjanje ali sestopanje|', 'Ⓘ bav. srvnem. lǫiter, nem. Leiter ‛lestev’', 1),
(74, 2, 'zdevalne lojtre', 'zdejvːne lùːjtre -əx -tər', 'ž mn.', 'lestev za obdevanje ostrnic', 'Ⓘ bav. srvnem. lǫiter, nem. Leiter ‛lestev’', 1),
(75, 1, 'držaj', 'dəržǻːj -a ', 'm', 'držaj |leseni del orodja, namenjen za držanje z rokami|', NULL, 1),
(76, 1, 'žatlaka', 'žǻːtlaka -e', 'ž', 'plenkači podobno enoročno orodje z zaokroženim rezilom in kratkim ročajem; pri tesanju z njo se stoji na tleh', 'Ⓘ bav. nem. *Schȃtl-hacke < *Scheitl-hacke iz scheiten (oz. dem. scheitln) ‛cepiti’ in Hacke ‛sekira’ (Be IV, 438)', 1),
(77, 2, 'brusiti (2)', 'brǘːsət -əm ', 'nedov.', 'brusiti |delati rezilo ostro|', NULL, 1),
(78, 2, 'nabrusiti (2)', 'nəbrúːsət -əm', 'dov.', 'nabrusiti |z oslo narediti koso ostro|', NULL, 1),
(79, 2, 'zob (2)', 'zùọp zobː', 'm', 'zob |leseni ali železni klin pri grabljah|', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Keyword_Record`
--

CREATE TABLE `Keyword_Record` (
  `idKeywordRecord` int(11) NOT NULL,
  `idKeyword` int(11) NOT NULL,
  `idRecord` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Keyword_Record`
--

INSERT INTO `Keyword_Record` (`idKeywordRecord`, `idKeyword`, `idRecord`) VALUES
(1, 1, 1),
(2, 1, 2),
(92, 1, 34),
(3, 2, 2),
(91, 2, 34),
(4, 3, 2),
(90, 3, 34),
(7, 4, 2),
(5, 5, 2),
(6, 6, 2),
(89, 7, 40),
(88, 8, 40),
(87, 9, 40),
(86, 10, 45),
(8, 11, 3),
(85, 12, 33),
(84, 13, 33),
(9, 14, 4),
(10, 15, 4),
(83, 16, 35),
(82, 17, 41),
(81, 18, 41),
(11, 19, 6),
(80, 20, 36),
(79, 21, 36),
(78, 22, 36),
(77, 23, 42),
(76, 24, 43),
(75, 25, 37),
(74, 26, 37),
(73, 27, 37),
(72, 28, 37),
(71, 29, 37),
(70, 30, 37),
(69, 31, 44),
(68, 32, 44),
(12, 33, 7),
(13, 34, 8),
(14, 34, 9),
(15, 35, 10),
(67, 36, 38),
(66, 37, 39),
(65, 38, 39),
(64, 39, 39),
(63, 40, 39),
(62, 41, 39),
(61, 42, 39),
(60, 43, 39),
(16, 44, 11),
(17, 44, 13),
(59, 44, 48),
(18, 45, 12),
(58, 45, 48),
(57, 46, 48),
(19, 47, 13),
(56, 47, 48),
(20, 48, 13),
(54, 48, 50),
(55, 49, 49),
(53, 50, 50),
(52, 51, 46),
(51, 52, 46),
(21, 53, 21),
(50, 53, 46),
(48, 54, 47),
(47, 55, 47),
(46, 56, 47),
(22, 57, 14),
(23, 57, 15),
(24, 57, 16),
(25, 58, 17),
(44, 58, 50),
(26, 59, 17),
(27, 60, 18),
(28, 61, 19),
(29, 62, 19),
(30, 63, 20),
(31, 64, 22),
(33, 65, 15),
(34, 66, 23),
(35, 66, 27),
(36, 67, 24),
(37, 68, 25),
(38, 69, 26),
(39, 70, 28),
(40, 71, 29),
(41, 72, 30),
(42, 73, 31),
(43, 74, 32),
(93, 77, 22),
(94, 78, 46),
(95, 79, 47);

-- --------------------------------------------------------

--
-- Table structure for table `Picture`
--

CREATE TABLE `Picture` (
  `idPicture` int(11) NOT NULL,
  `idKeyword` int(11) DEFAULT NULL,
  `name` varchar(63) CHARACTER SET utf8 NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `Picture`
--

INSERT INTO `Picture` (`idPicture`, `idKeyword`, `name`, `activated`) VALUES
(1, 4, 'F_001_001.jpg', 1),
(2, 1, 'F_001_002.jpg', 1),
(3, 11, 'F_001_003.jpg', 1),
(4, 17, 'F_001_004.jpg', 1),
(5, 24, 'F_001_005.jpg', 1),
(6, 13, 'F_001_006.jpg', 1),
(7, 31, 'F_001_007.jpg', 1),
(8, 35, 'F_001_008.jpg', 1),
(9, 36, 'F_001_009.jpg', 1),
(10, 38, 'F_001_010.jpg', 1),
(11, 14, 'F_001_011.jpg', 1),
(12, 16, 'F_001_012.jpg', 1),
(13, 76, 'F_001_013.jpg', 1),
(14, 45, 'F_002_001.jpg', 1),
(15, 44, 'F_002_002.jpg', 1),
(16, 49, 'F_002_003.jpg', 1),
(17, 50, 'F_002_004.jpg', 1),
(18, 58, 'F_002_005.jpg', 1),
(19, 64, 'F_002_006.jpg', 1),
(20, 66, 'F_002_007.jpg', 1),
(21, NULL, 'F_002_008.jpg', 1),
(22, 49, 'F_002_009.jpg', 1),
(23, 72, 'F_002_010.jpg', 1),
(24, 5, 'F_001_001.01.jpg', 1),
(25, 6, 'F_001_001.02.jpg', 1),
(26, 8, 'F_001_001.03.jpg', 1),
(27, 9, 'F_001_001.04.jpg', 1),
(28, 10, 'F_001_001.05.jpg', 1),
(29, 19, 'F_001_001.06.jpg', 1),
(30, 20, 'F_001_001.07.jpg', 1),
(31, 21, 'F_001_001.08.jpg', 1),
(32, 22, 'F_001_001.09.jpg', 1),
(33, 23, 'F_001_001.10.jpg', 1),
(34, 25, 'F_001_001.11.jpg', 1),
(35, 26, 'F_001_001.12.jpg', 1),
(36, 27, 'F_001_001.13.jpg', 1),
(37, 28, 'F_001_001.14.jpg', 1),
(38, 29, 'F_001_001.15.jpg', 1),
(39, 32, 'F_001_001.16.jpg', 1),
(40, 33, 'F_001_001.17.jpg', 1),
(41, 34, 'F_001_001.18.jpg', 1),
(42, 37, 'F_001_001.19.jpg', 1),
(43, 40, 'F_001_001.20.jpg', 1),
(44, 41, 'F_001_001.21.jpg', 1),
(45, 42, 'F_001_001.22.jpg', 1),
(46, 43, 'F_001_001.23.jpg', 1),
(47, 15, 'F_001_001.24.jpg', 1),
(48, 2, 'F_001_002.01.jpg', 1),
(49, 3, 'F_001_002.02.jpg', 1),
(50, 7, 'F_001_002.03.jpg', 1),
(51, 12, 'F_001_003.01.jpg', 1),
(52, 18, 'F_001_003.02.jpg', 1),
(53, 30, 'F_001_003.03.jpg', 1),
(54, 18, 'F_001_004.01.jpg', 1),
(55, 30, 'F_001_004.02.jpg', 1),
(56, 16, 'F_001_006.01.jpg', 1),
(57, 18, 'F_001_006.02.jpg', 1),
(58, 75, 'F_001_007.01.jpg', 1),
(59, 39, 'F_001_010.01.jpg', 1),
(60, 12, 'F_001_011.01.jpg', 1),
(61, 46, 'F_002_001.01.jpg', 1),
(62, 47, 'F_002_001.02.jpg', 1),
(63, 48, 'F_002_001.03.jpg', 1),
(64, 51, 'F_002_001.04.jpg', 1),
(65, 52, 'F_002_001.05.jpg', 1),
(66, 53, 'F_002_001.06.jpg', 1),
(67, 40, 'F_002_001.07.jpg', 1),
(68, 55, 'F_002_001.08.jpg', 1),
(69, 56, 'F_002_001.09.jpg', 1),
(70, 79, 'F_002_001.10.jpg', 1),
(71, 60, 'F_002_001.11.jpg', 1),
(72, 61, 'F_002_001.12.jpg', 1),
(73, 62, 'F_002_001.13.jpg', 1),
(74, 63, 'F_002_001.14.jpg', 1),
(75, 32, 'F_002_001.15.jpg', 1),
(76, 65, 'F_002_001.16.jpg', 1),
(77, 68, 'F_002_001.17.jpg', 1),
(78, 70, 'F_002_001.18.jpg', 1),
(79, 71, 'F_002_001.19.jpg', 1),
(80, 73, 'F_002_001.20.jpg', 1),
(81, 54, 'F_002_002.01.jpg', 1),
(82, 57, 'F_002_002.02.jpg', 1),
(83, 66, 'F_002_002.03.jpg', 1),
(84, 59, 'F_002_003.01.jpg', 1),
(85, 59, 'F_002_004.01.jpg', 1),
(86, 50, 'F_002_005.01.jpg', 1),
(87, 57, 'F_002_006.01.jpg', 1),
(88, 67, 'F_002_006.02.jpg', 1),
(89, 69, 'F_002_006.03.jpg', 1),
(90, 50, 'F_002_009.01.jpg', 1),
(91, 59, 'F_002_009.02.jpg', 1),
(92, NULL, 'F_002_009.03.jpg', 1),
(93, 74, 'F_002_010.01.jpg', 1),
(94, 77, 'F_002_001.21.jpg', 1),
(95, 78, 'F_002_001.22.jpg', 1),
(96, 79, 'F_002_001.23.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Record`
--

CREATE TABLE `Record` (
  `idRecord` int(11) NOT NULL,
  `name` varchar(63) CHARACTER SET utf8 NOT NULL,
  `transcription` longtext CHARACTER SET utf8 NOT NULL,
  `translation` longtext CHARACTER SET utf8 NOT NULL,
  `type` varchar(1) COLLATE utf8_slovenian_ci NOT NULL COMMENT '''a'' = audio, ''v'' = video',
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `Record`
--

INSERT INTO `Record` (`idRecord`, `name`, `transcription`, `translation`, `type`, `activated`) VALUES
(1, 'A_001_001.mp3', 'Plenkǻːče so pa rːzne.', 'Plenkače so pa različne. ', 'a', 1),
(2, 'A_001_002.mp3', 'Zetùː pa – plenkǻːče so pa: léːva, disna pa stranìːčarka.', 'Zato pa – plenkače so pa leva, desna in straničarka.', 'a', 1),
(3, 'A_001_003.mp3', 'ˈNo, tùː je tesːjne.', 'No, to je tesanje.', 'a', 1),
(4, 'A_001_004.mp3', 'ˈJəst imam še ˈtamlej trːme tesǻːne, so pokrìːtə pot tìːstəm, kə səm ta zːdne tːsa. ', 'Tamle imam pokrite še ročno tesane trame, ki sem jih nazadnje tesal.', 'a', 1),
(5, 'A_001_005.mp3', 'Pˈrec ot krǻːja ˈne, zətùː kə pˈrec ot krǻːja néːs tesǻːč, sə pərprːnik, pːli səm začìẹ pa tːsat, səm bi pa samostːjən.', 'Takoj od začetka ne, zato ker takoj na začetku nisi tesač, si pripravnik, potlej sem začel pa tesat, sem bil samostojen.', 'a', 1),
(6, 'A_001_006.mp3', 'sta pa žːgala', 'sta pa žagala', 'a', 1),
(7, 'A_001_007.mp3', 'Tùː so mːtrə. Tùː-j ot pokːjnəga očita.', 'To so metri. To je od pokojnega očeta. ', 'a', 1),
(8, 'A_001_008.mp3', 'Tùː je ze vodùọ, tàː je mogla bət obavéːznu, bːnka. ', 'To je za vodo, ta je morala biti obvezno, banjka.', 'a', 1),
(9, 'A_001_009.mp3', 'Gozdǻːrjə smo prej mel bːnko dvːjset lìːtru. De s-jo ne xːrptə nisu, kə-s bi mǻːjxən, ˈpa še dve  rːkax. In se néːsmo mivǻːl ce kèːdən ˈnəč. Je blu škuda, sə tri kilomːtre déːləč vodùọ nːsu, ˈtam ne blu mivːjna č, rokìẹ so blìẹ pa zmìẹrej ot smolìẹ reskǜːžene.', 'Gozdarji smo nekdaj imeli dvajsetlitrsko banjko. Da si nesel na hrbtu, ker si bil majhen, pa še dve v rokah. Cel teden se nismo nič umivali. Vode je bilo škoda, ker si jo nosil tri kilometre daleč; tam ni bilo nič umivanja, roke pa so bile zmeraj razkužene od smole.', 'a', 1),
(10, 'A_001_010.mp3', 'Tùː smo jméːl mìː kubìːkpíːkəl /../ Tùː so blìẹ ˈse mire nutrə. In tùː je pa sˈtḁr (VS prebere: 1870). Tùː so ˈse mire, okroglíːne in trːmu.', 'Imeli smo posebno knjižico za vpisovanje mer in okroglin tramov.', 'a', 1),
(11, 'A_002_001.mp3', 'Tìːst je kːsa [železni del], kùːkər je mira, ˈne, pərblìːžnu sindəsət, nekjìẹ  svèːtə ìːmaje dǻːlše, pa mːnjša kúːsja, ˈne.', 'Tisto je kosa, kakor je mera, a ne, približno 70 cm, ponekod imajo daljše kose pa manjša kosišča, ne.', 'a', 1),
(12, 'A_002_002.mp3', 'Tùː-j klìẹp, ˈne, ga vìːtte? Se svéːtə. Tː se mǻːl znǘːca /../ pa ga griš nːga naréːdət ne tùː ˈlej. ', 'To je klep, a ne, ga vidite? Se sveti. Ta se malo zrabi /../ pa greš novega narediti.', 'a', 1),
(13, 'A_002_003.mp3', 'Sklːpat. Al pa če-j bi kːsəc lèːn, je ˈšu kléːpat, pa-j səˈdu. ˈNe? ˈTəga j-blu dːst. ˈJəst vam bom povéːdo zgodovíːno z Gorìẹ. Z Gorìẹ. Sva šˈla sə soséːdam nərːčət parùọčno oblèːko. Z Ložǻːnovəm, ˈne. ˈPa-j bi tùː, tùː je bi stːr ːča ot tìẹ, kə-j ma Gùọlčək. Stːr ːča, z Gorìẹ. Je bi ku an antekrìːst. In smo blə ˈtam, pa-j bi pər ščirə, ˈne, starèːšə člóːk. In ˈon je riku, de je skleːpo  nədéːle sidəm kosòː. Zə sːk-dan ǻːno, ǻːno pa zə rəzːrvo, čə se stˈre.', 'Sklepati. Če je bil kosec len, je šel klepat in je sedel. A ne? Tega je bilo veliko. Povedal vam bom zgodbo z Gore. Z Gore. S sosedom sva šla naročit poročno obleko. Z Ložanovim, a ne. In to je bil stari oče od te, ki jo ima Golček. Je bil kot en antikrist. In smo bili tam, pa je bil pri hčeri, ne, satrejši človek. In je rekel, da je v nedeljo sklepal sedem kos. Za vsak dan eno, eno pa za rezervo, če se stre.', 'a', 1),
(14, 'A_002_004.mp3', 'ˈJəst səm bi zə kːsca dobˈlen.', 'Jaz sem bil najet za kosca.', 'a', 1),
(15, 'A_002_005.mp3', 'Kːsəc. Al pa kosíːca. Pùːnce so dːst kosìːle, pːlij pa dùːve. Pa fìːnu, ˈne.', 'Kosec. ali pa kosica. Dekleta so veliko kosile, potem pa vdove. Pa dobro, a ne.', 'a', 1),
(16, 'A_002_006.mp3', 'Tùː-j blu pa tokùː: ˈjəst imam zˈdej košeníːco ˈtamlejgːrə, se upisǜːje zə subvːncijo štìẹr xəktːrje. Tùː se je réːklu šəstnːjst kosˈcuˑ. Parcːla. En dːn, ce dːn, od zːre do mrːka šestnːjst kosˈcuˑ.', 'To je bilo pa tako: tamgori imam sedaj košenico, ki se za subvencijo vpisuje štiri hektarje. To je parcela za šestnajst koscev. En cel dan, od zore do mraka, jo je kosilo šestnajst koscev. ', 'a', 1),
(17, 'A_002_007.mp3', 'Tùː-j kùːzla klepːjne, ˈne, ˈtəmə prːmo mi kùːzla. Dərgáːčə j-blu pa ˈtüt bǻːpca, j-blu pa drùːgu, se-j pa zebíːlu. Mam jəst obùːje tùː. Tùː zgːraj je ostrùː, ˈne, ima rùọp, klǻːdu j-pa ronùː. Bǻːpca je pa gˈlix nerùọbe – k-se pa  zìẹmlo zebìːje, ˈneˑ, je pa ronː, klǻːdu je pa špíːčest. Tùː [kuzla] je zej že novːjša, tùː so že žinske  puvːjsknəm čːsə, k-so bli dìė pobìːtə ˈsə, bəl začìẹle kléːpat na tùː, ˈne. Tùː je ˈbəl nevːdnu, ˈbəl ǻːjnfox, tùː so ˈse dùːve klepǻːle. In tùː fìːnu klepǻːle, kə žinske so samìẹ ostǻːle, ˈne. Tùː je pəršlùː po vːjskə, ˈne. Ùːnu [babico] smo jméːl pa prèːj. Sə pa  lːzə zebùː, nèː, mǻːl met ˈkašne koreníːne, in sə se mogu ne tˈla ˈsest, pːl s-pa nːjdu  gːrmə tǻːko rogovíːlco, de-j kːsa stǻːla gːr, de-s kléːpo. Pa mǻːl vodìẹ je moglu bət zdrːvən, ˈne. ˈTü tréːba pa ˈnəč. ', 'To je kuzla, klepalnik za klepanje, ne, temu pravimo mi kuzla. Drugače je bila pa tudi babica, je bilo pa drugo, se je pa zabilo (v zemljo). Jaz imam oboje. Tu zgoraj je ostro, ne, ima rob, kladivo je pa prisekano. Kuzla je zdaj že novejša; na njej se po vojni, potem ko so bili moški pobiti, začele klepat ženske. To je bilo bolj enostavno in so klepale vdove. In so dobro klepale, ker so same ostale. To je prišlo po vojski. Ono (babico) smo imeli pa prej. Si jo pa v lazu zabil med kakšne korenine in si moral sesti na tla, potem pa si v grmu našel kakšno rogovilico, da je kosa stala gori, da si klepal. Zraven pa je moralo biti malo vode, a ne. Pri tej (kuzli) pa ni treba.', 'a', 1),
(18, 'A_002_008.mp3', 'Tùː-j kúːsje, ˈne, ročːji. ', 'To je kosje, ne, ročaji.', 'a', 1),
(19, 'A_002_009.mp3', 'Ìːma pa tˈle rìːnk, ˈrənk tːk, kːsa jmː pa  /../ tːk nèːt, də mùọre ˈmət kúːsje lǜːkənco, pː pa šrːjf – ˈrənk čìẹs príːde, pa zešrːjfa se.', 'Tule ima obroč, kosa ima pa tak net, tako zakovico, da mora imeti kosje luknjico, potem pa vijak – obroč pride čez pa privije se.', 'a', 1),
(20, 'A_002_010.mp3', 'Zegùːjzda je pa, če-j kːsa preˈveč u tˈla, ˈne, de preˈveč u žìːvu rèːže, se jo ne zegùːjzdo dine. ˈNe. De jo nǻːgneš. Tùː-j sìẹna zegùːjzda, jː.', 'Zagozda se pa da, če je kosa nagnjena preveč v tla, in če preveč v živo reže, se jo dene na zagozdo. Ne. Da jo nagneš. To je lesena zagozda, ja.', 'a', 1),
(21, 'A_002_011.mp3', 'ˈSej íːmaš ogníːlu, ˈne, in maš oslùọ, in ogníːlu prːmo tǻːku jəklinu, de poronǻːš, ˈne, če zedineš. ', 'Saj imaš ognilo, ne, in imaš oslo. Ognilu pravimo taki jekleni napravi, ki z njo poravnaš, ne, če zadeneš.', 'a', 1),
(22, 'A_002_012.mp3', 'Tìːst je bi ku en nːrc ze kosìːt. Tìːst je zǘːtrej zepìẹ in je mːgla kːsa kːsət ce-dːn. ˈNəč klːpat! Ker de kiẹr xuọt klːpat, je lèːn! Tùː muọrš kosùọ mirkat, ːmpək so blie kamnìːte košeníːce, ˈne, ˈtə. In mìː smo tüt bəl slabùː bərsíːlə, al kǻːj-st vem kǻːj. Nevelìːčo sə se, ˈpa-s šu mǻːl poˈtočt. ˈNe. Jː. Al pa zeˈdu sə, ˈne, zeˈdu sə, pa .. ˈno. ', 'Tisti je bil pravi norec za kosit. Zjutraj je začel z vso silo in je morala kosa kositi cel dan. Nič klepati! Ker da kdor hodi klepat je len! A ne? Koso je treba paziti, ampak košenice so bile tu kamnite. Mi pa smo tudi bolj slabo brusili ali ne vem kaj. Naveličal si se, pa si šel malo poklepat.', 'a', 1),
(23, 'A_002_013.mp3', 'kːsce, pː-pa grablíːce', 'kosce, potem pa grabljice', 'a', 1),
(24, 'A_002_014.mp3', '/../ Kːšna se začˈne pərblìːžnu – čətːrdga júːlija smo zečil, kə-j bi dopùːst. Pa do zəmìẹ. Tapːru je sənùː, trǻːva, pːl príːde pa otːva, pa krampirjeca pa – jː, ˈse žíːvu, ˈne. Pa nestíːlu pa ˈse. ', 'Košnja se začne približno – začeli smo četrtega julija, ko se je začel dopust. Pa do zime. Najprej je seno, trava, potem pride otava pa krompirjevica pa – ja, vse mogoče, a ne. Pa nastilj in vse drugo.', 'a', 1),
(25, 'A_002_015.mp3', 'Tapːru smo jméːl stǻːre néːmške kosìːlce /../, jìẹru sǻːgərjə /../, tìẹ so ble pa taljːnke – bəčːske. Tìːste so ble bùːlše kosìːlce. ', 'Najprej smo imeli stare nemške kosilnice /../, nato pa italijanske – bečeske, BCS. Te so bile boljše.', 'a', 1),
(26, 'A_002_016.mp3', 'Tùː je tokùː, ˈne. /../ Po roníːnax déːvajo tùː u redìː, ˈne. Kosíːš, u redìː, rːtajo. Če je pa xribovíːtu pa tǻːku, griš pa bəl tokùː, də néː ˈč ze tebùọ ze roˈnat. /../ Ditele pa velìːka trǻːva se u redìː kosíː. Pa se pː reskopːva tìːstu.', 'To je tako, a ne. /../ Po ravninah devajo travo v redi. Kosiš v redi, nastanejo redi. Če pa je hribovito, pa kosiš tako, da za tabo ni potrebno ravnati, trositi. /../ Detelja in velika trava se kosita v redi.', 'a', 1),
(27, 'A_002_017.mp3', 'Mːškə so kosìːlə, /../ so ble pa grablíːce, ˈne /../. ', 'Moški so kosili, ženske so bile pa grabljice, a ne.', 'a', 1),
(28, 'A_002_018.mp3', 'ˈAmpak prez vìːl, sìẹnəx, ne blu ˈnəč. Zˈdej j-pa poberǻːlka, zˈdej pa poberǻːlka pobéːre, ne gledìẹ kǻːj: i kǻːmən i kərtíːne i ˈse žíːvu. /../ Al pa bǻːle zvíːjejo, je ˈtüt ˈse sùọrte nùọtrə, pərtə prːj. Prːj se-j ˈse z rokǻːmə néːslu u jːsle, ˈne, zˈdej so ˈsam víːle.', 'Ampak brez lesenih vil ni bilo nič. Zdaj je pobiralka, zdaj poberalka pobere, ne glede kaj: in kamen in krtine in vse živo. Ali pa bale zvijejo, je tudi notri vse mogoče, glede na prej. Prej se je vse z rokami neslo v jasli, a ne, zdaj pa samo z vilami.', 'a', 1),
(29, 'A_002_019.mp3', '/../ Kokər prːj, se je kosìːlu in grǻːblu, pa  kopíːce dejvǻːlu. Kopíːce prː, obavːznu so moglə  kopíːce ˈdet čez nùːč, pa ˈtüt če-j blu lepùː vráːme. Zetùː kə tìːstu se-j mal čez nùːč prepǻːrlu, drǜːg-dån so restrislə, ogrːpke nerìẹdlə, obračǻːlə, je blu prːj səxùː. Pa zelːnu je blùː, ˈne. Zˈdej je pa tu ˈse drǜːgu, zˈdej je pa tu – trǻːktorjə ˈse bìːjejo, tok de-j bːgu blagùː, kə zdej tu jéː. Véːste. Prːj j-blu pa dərgǻːčə, ˈne. Prːj j-blu tùː ˈse rːčnu nerjǻːnu, ˈse bəl sːčnu pa tǻːku, zˈdej je ˈse bìːtu. Pa zemlìẹ, pa ˈsega. ', '/../ Prej se je kosilo in grabilo ter zdevalo v kopice. Za čez noč so morale biti obvezno kopice, četudi je bilo lepo vreme. Zato ker se je seno čez noč malo preparilo, naslednji dan so raztrosili, naredili ograbke, obračali, da je bilo prej suho.', 'a', 1),
(30, 'A_002_020.mp3', 'Níːve so se tùː -ostərníːce dejvǻːle. /../ Níːve so se dèːle  ostərníːce, ˈne. Košeníːce ˈne. Je premːjxnu, ˈne.', 'Trava z njiv se je tu dajala v ostrnice, s košenic pa ne. Je premajhno, ne.', 'a', 1),
(31, 'A_002_021.mp3', 'Tùː so lùːjtre. Tǻːke lùːjtre imam jəst ˈtamle dːl nərjǻːne desìẹt mːtro dːge. So ble ze ne stréːxo. ', 'To je lestev. Tako lestev imam narejeno tam doli in je dolga deset metrov. Je bila za na streho.', 'a', 1),
(32, 'A_002_022.mp3', 'Pa zdejvːne lùːjtre so bˈle. Ze ostərníːce. So ble pa ze zdéːvat. Je bla pa tǻːka ku tùː, pa tˈle je jmèːla bəl šrːcga təgː, pa lǜːkna ne sréːdə pa drːk, de se-j tok postǻːlu, ˈne. Tùː je otpǻːlu, təgː ne ˈveč. Lùːjtre so ˈše. ', 'Pa zdevalna lestev je bila. Za ostrnice. Je bila za zdevati. Je bila pa taka kot to (kaže sliko), pa tule je imela to širše, pa luknjo na sredi pa drog, da se je tako postavilo, a ne. To delo je odpadlo, tega ni več. Lestev je še.', 'a', 1),
(33, 'V_001_001.mp4', 'Tùːle je orùọdje /../ za təsǻːča al pa za səkǻːča. Kə səm bi ˈjəst mlːd, mə je ːča  ˈgost péːlo /../. In mùːj ːča je riku, de tùː ne bùọ falìːlu, kə se rːče po patùọšku, nìːgdar. De tùː je tradíːcija ˈveč ku stu-lèːt, de se tùː prǘːme. ˈNo, tùː je tesːjne. Muj ːča je bi tesǻːč, je bi sˈtar uọsəmənšìẹzdeset lèːt, ˈjəst pa šestnǻːjst, k-sva zəčìẹla. ˈNo, in tùː je zə, ˈkar se tíːče zə cíːmpre, zə tramarìːjo.', 'Tole je orodje /../ za tesača ali pa sekača. Ko sem bil jaz mlad, me je oče peljal v gozd /../. In moj oče je rekel, da to ne bo propadlo, falilo, kot se reče po potoško, nikoli. Da je to več kot stoletna tradicija, da se to ohranja. No, to je tesanje. Moj oče je bil tesač, star je bil oseminšestdeset let, jaz pa šestnajst, ko sva začela. No, in to je za izdelavo ostrešja,  tramovja.', 'v', 1),
(34, 'V_001_002.mp4', 'Tùː je plenkǻːča zə leˈga. Prǘːme se tokùː, pərblìːžnu je tːška blíːzə štiẹr-kìːle, in tùː se je metǻːlu od zːre do mrːka.  muj-dːbə, ˈne. Pˈrec ot krǻːja ˈne, zətùː kə pˈrec ot krǻːja néːs tesǻːč, sə pərprːnik, pːli səm začìẹ pa tːsat, səm bi pa samostːjən. ˈNo, tùː je plenkǻːča, kə príːde zdrːvən, so tˈle dvèː mojìẹ.', 'To je plenkača za levičarja. Prime se tako, težka je približno štiri kilograme in z njo se je delalo od zore do mraka. V moji dobi. Takoj od začetka ne, zato ker takoj na začetku nisi tesač, si pripravnik, potlej sem začel pa tesat, sem bil samostojen. No, to je plenkača, ki pride zraven. Sta tu dve moji.', 'v', 1),
(35, 'V_001_003.mp4', 'Tùː je səkira, je ːbavːzna zdrːvən, se nèː sméːlu s tùọ [s plenkačo] zəsəkːvat gːrče, jo jə blu škuda, je bla tàː glːna. Tː-j spotsəkovːla in ˈsəskəp, ˈne. ', 'To je sekira, je zraven obvezna, saj se s to (plenkačo) ni smelo zasekavati grč, ker jo je bilo škoda, je bila ta glavna. Ta je spodsekovala in vse skupaj, a ne.', 'v', 1),
(36, 'V_001_004.mp4', 'Tùː so pa žǻːge. Tùː je merikːjnka samíːca, tùː je ze-nˈga. Tùː je bi saˈmu éːdən, kə je šu  ˈgost, pa smo ponevǻːdə, kə-s nisu očìẹtə kosíːlu al pa júːžno pràːmo pər ˈnas, je ˈmu zmìẹrej še tːle rožìːčək zdrːvən. Se strːn zéːme, ˈne; čə je bi sːm, je blu pˈrez rožìːčka, ˈkokər s-mə dː pa zə ˈjəst, ˈtam je poˈju, je pa pˈrec ta rožìːčək zdrːvən pəršrːjfo, ˈne, de sə mə pomǻːgo poséːkat ane pːr smrèːk. ˈDe ne sːm, ˈne. Zətùː kə sːm, je blu tokùː, ˈne: ˈəlčt ˈsəm, čə sta pa dvː, je pa ùːnə, pa čeˈtüd je bi mːjxən, potignu ˈke. ˈNo, tùː ˈje, tùː je, se réːče de je samíːca al pa spüščːənca po merikːjnsku. Tùː se rːče, ˈkar se tíːče zə poˈsẹk.', 'To so žage. To je amerikanka samica, to je za enega. To je bil samo eden, ki je šel v gozd, pa smo ponavadi, ko si nesel očetu kosilo ali pa južino, pravimo pri nas, je imel zmeraj zraven še tale rožiček. Se vzame stran, a ne, če je bil sam, je bilo brez rožička, kakor si mu dal pa za jesti, tam je pojedel, pa je takoj privil zraven ta rožiček, a ne, da si mu pomagal posekati nekaj smrek. Da ni sam, a ne. Zato ker sam je bilo tako, a ne: vleči sem, če sta pa dva, je pa oni, pa četudi je bil majhen, potegnil tja. No, To je, to je, se reče, da je samica ali pa po amerikansko  spuščavnica. To se reče, kar zadeva posek.', 'v', 1),
(37, 'V_001_005.mp4', 'Zə pər tramarìːjə se rǻːbə klːmfa, de se zəbìːje, okroglíːna de se ne vərtíː. Tùː je zə tesːjne. Pːlij tùː so /../ žnùọre, de se pocǻːxna, de se vìːdə píːsa, ˈne. Žnùọro sva midvː pːl zmìẹrej – midvː z očitam sva zmìẹrej [neraz.] uklːdala u tìːstə, ugu. ːmpək ˈne tåk ugu. Tːk ugu, de smo ːgin nərìẹdlə, de je jalovíːna zgorèːla, pa kə-j ponìẹxala, smo déːlə  eno tǻːko /../ – fərbìẹnka, je bla ana-taljːnska pːrcija, z rùọčko pər nːma skùːzə, ˈin smo tistu ugle potːklə, pa tìẹ nəmočíːlə, de je bla píːsa, zətùː kə tùː jə blu ˈnarbəl tüt zə rokìẹ zdrǻːvu. Bːrva ne gri strːn od rùọk. Tùː je, ˈkar se tíːče mire zə trːm. ', 'Pri tesanju se rabi penja ali skoba, da se pritrdi, da se okroglo deblo ne vrti. To je za tesanje. Potlej so to /../ obarvane vrvice, da se označi in se vidi črta, a ne. Vrvico sva midva zmeraj – midva z očetom sva [neraz.] vlagala v tisto .. oglje. Ampak ne tako navadno oglje. Pač pa tako oglje, da smo naredili ogenj in ko je jelovina zgorela, ko je ponehala goreti, smo deli v tako /../ – farbenka, je bila neka italijanska posoda z ročko ves čas pri naju, in smo tisto oglje potolkli, namočili, da je bila črta, zato ker je bilo to tudi najbolj zdravo za roke. Barva ne gre stran od rok. To je, kar zadeva mere za tram.', 'v', 1),
(38, 'V_001_007.mp4', 'Kar se tíːče pa kə posːke – tùː smo réːklə prǻːlca. Tùː jə prǻːlca zə lèːtnə poˈsẹk. Tùː se je smréːka – se je lupìːla s ˈtəm. In ˈso, tistə obùːdə so blə zə črešlovíːno. Tùː je zə lèːtno dːbo. Kədər je məžinu, ˈne. Tùː je pa zˈdej pa že novːjše blùː, pəršlùː, je pa zíːmska prǻːlca. Zətùː kə zə mojùọ dːbo, ˈkar səm ˈjəst xːdu po gùọzdə, je ˈmoglu bət ˈse obéːlenu. ˈTam ne mu lubːdar ˈkej zə nːmi isˈkat. Zˈdej j-pa drǜːga. Zˈdej j-pa ˈse  kùọrə, pa ˈtam ostːne, je pa za danǻːšnə svèːt pa lubːdar pərˈšu ne vːrsto. ', 'Kar zadeva poseko – temu smo rekli pralica. To je pralica za letni posek. Smreka se je lupila s tem. Tisti obodi so bili za čreslovino. To je za letno dobo. Kadar je meženo, a ne. To je pa že novejše bilo, prišlo, je pa zimska pralica. Zato ker za mojo dobo, ko sem jaz hodil po gozdu, je bilo vse obeljeno. Tam ni imel lubadar kaj iskat za nami. Zdaj je pa druga (pesem). Zdaj je pa vse v skorji, lubju, pa tam ostane, je pa v današnjem času pa lubadar prišel na vrsto.', 'v', 1),
(39, 'V_001_008.mp4', 'ˈse orùọdje smo mìː bərsíːl samìː. ːče j-bi spˈlox poklìːcnə brüsǻːč tǜːdə, ˈne. In tùː se brǘːsə – tùː so kučnikə. Tùː so pa zobjìẹ. Zùọb je nebrǘːšən – tː, k-je ne ùːno strǻːn respelːn, se tokùː nəbrǘːsə, pa tokùː. Tː je zə ùːno strǻːn. ˈNo,  kučnək se pa tokùː nebrǘːsə, ˈne, zə kučnk smo-jméːl pa – kučnək muọre bət nìːžj od zobː. Mːmo pa tǻːkoˈlej mašìːnco, de-j gːr dineš, nə kučnək, pa s pìːlo pobrǘːsəš, de je mḁlu nìːžj od ùːnga. Tùː je rezìːlnə, tùː je pa čistìːlc.  ˈTəmə smo ráːkəl kučnk, ˈne, in tː muọre bət pa tǜːlku porèːzan, de-j mːjxnu nìːžji. Ùːnə porèːžəjo, tː pa ščìːstə. Tùː je zə rùọčnu déːlu, ˈne.', 'Vse orodje smo brusili sami. Oče je nasploh bil tudi poklicni brusač, a ne. In to se brusi – to so kočniki. To so pa zobje. Zob je nabrušen – ta, ki je na ono stran razpeljan, se tako pa tako nabrusi. Ta je za na ono stran. No, kočnik se pa tako nabrusi, a ne, za kočnik smo imeli pa – kočnik mora biti nižji od zoba. Imamo pa takole napravico, da jo deneš gor, na kočnik, pa s pilo pobrusiš, da je malo nižji od onega (zoba). Ta je rezilni, ta je pa čistilec. Temu smo rekli kočnik, ne, in ta mora biti pa tako porezan, da je nekoliko nižji. Oni porežejo, ta pa čisti. To je za ročno delo, ne.', 'v', 1),
(40, 'V_001_009.mp4', 'Plenkǻːče so pa rːzne. ˈJe líːčna – ˈjəst səm iˈmu ˈrat líːčne – tùː je pˈro, de je zːt rːna, pːl so pùː líːčne, pa trìːčetːrt; tùːk de je mǻːl ta ostríːna pugnena. Líːce se prː tùː, de je tùː skoˈrej prǻːktičnu pˈro ronùː. Tː je ˈbəl tokùː opːsna zə tesːjne, či-j pa pùː-líːčna, je pa mǻːl zvìːtu, ˈne. Mùọrš pa sːk pùọt, zə sːk zəˈmax popərjǻːmat. ', 'Plenkače so pa razne. je lična – jaz sem imel rad lične – to je taka, da je zadaj ravna, potem so pol lične in tričetrt; tako da je nekolikanj ostrina upognjena. Lice se pravi tu, da je tu skoraj povsem ravno. Ta je bolj nevarna za tesanje, če je pol lična, je pa malo zvito, a ne. Moraš pa vsakič, za vsak zamah poprijemati.', 'v', 1),
(41, 'V_001_016.mp4', 'Čə sva bla pa obː sˈkəp [z očetom], sva uporǻːblala pa tùọle žǻːgo. Tùː je pa merikːjnka. Kə sva bla obː  gùọzdə sˈkəp, ˈne. Jə bla tː žǻːga skùːzə  gùọzdə. Cəlùː léːtu. Sprːləna u vːjəx. Tùː je zə dvː žǻːga.', 'Če sva bila oba (z očetom), sva uporabljala pa tole žago. To je pa amerikanka. Ko sva bila v gozdu oba, a ne. Ta žaga je bila ves čas v gozdu. Celo leto. Spravljena v vejah. To je žaga za dva.', 'v', 1),
(42, 'V_001_017.mp4', 'In tùː je pa nə dvéːjəx rožìːčkax, pa se-j poséːkalu, čə-j blu debilu drəvùː, je tǜːkej ˈše-n rožìːčək – sːk na-n strǻːn sta léːkla, ˈne – in ˈkədər je blu kːnc rèːza, də-s ləx do krǻːje, pa zːd zabìːte tǻːkeˈlej kːjle, smo-jméːl tǻːkeˈlej kːjle, sìẹne, kùːl plǻːstəčnəx. Tùː-j zˈdej pəršlùː,  novːjšə dːbə. ˈIn smo-jx tüt ˈtam nərìẹdəl, so se pa šǜːšle, in tùː je bla – ˈdəš jo néː-smu zmːčət, ˈne.', 'In to je pa na dveh rožičkih, pa se je posekalo, če je bilo drevo debelo, je tukaj še en rožiček – vsak na eni strani sta vlekla, a ne – in kadar je bilo konec reza, da si lahko {prišel} do konca, pa zadaj zabite take zagozde, smo imeli take zagozde, lesene, nikoli plastičnih. To je prišlo zdaj, v novejši dobi. In smo jih tudi tam naredili, so se pa usušile in to je bila – dež je ni smel zmočit.', 'v', 1),
(43, 'V_001_018.mp4', 'ˈNo, tùː sta pa dvː rožìːčka, ˈne, sta pa žːgala, in sta víːdla, kük mǻːnka do kːnčnəga, ˈne – zːt je bla kːjla – se-j en rožìːčək otšrːjfo, se-j žǻːga ˈən spəlìːla, ˈne, ˈin se-j negníːlu, de-j drəvùː pǻːlu. Tùː ne bə blu tokˈlej. ˈKar se tíːče tu tesːjne, bə blu tùː sesˈkəp. ', 'No, to sta pa dva rožička, a ne, sta pa žagala in sta videla, da je še malo do konca (debla), a ne – zadaj je bila zagozda – se je en rožiček odvil, žaga se je spulila ven, a ne, in se je nagnilo, da je drevo padlo. To naj bi bilo takole. Kar se tiče tesanja, bi bilo to vse.', 'v', 1),
(44, 'V_001_019.mp4', 'Plenkǻːča pa prːmo – tùː je rːjf, ˈne. Plenkǻːča se brǘːsə saˈmu od ane straníː. Zːt, tùː je pa líːce. Tùː se ne brǘːsə ˈnəč, ˈne, tùː se saˈmu [neraz.] ːbcìːga. ', 'Plenkača pa pravimo – to je ostri rob, rajf, a ne. Plenkača se brusi samo na eni strani. Zadaj, to je pa lice. To se ne brusi nič, a ne. To se samo (neraz.) potegne z gladilnikom, abciga.', 'v', 1),
(45, 'V_001_020.mp4', 'Líːce se prː tùː, de je tùː skoˈrej prǻːktičnu pˈro ronùː.', 'Lice se pravi tu, da je tu skoraj povsem ravno.', 'v', 1),
(46, 'V_002_002.mp4', 'ˈNo, tùː pər kːsə je – usonək je ːbavːzən, u usonəkə je vːda, tùː je ːsla, kə kosùọ nəbrúːsmo, tùː je pa ogníːlu. Zˈdej vam bom pa pokǻːzo, kúːk se kːsa nebrǘːsə /../ [brusi] Tokùː se kːsa nəbrǘːsə, če se jo pa zədine  kǻːmən, se pa s ˈtəmlej poronː – ogníːlu je zə poroˈnat. ˈNọ, tùː je ˈseskəp.', 'No, tu pri kosi je – oselnik je obvezen, v oselniku je voda, tu je osla, ko koso nabrusimo, to je pa ognjilo. Zdaj vam bom pa pokazal, kako se kosa nabrusi, če se jo pa zadene v kamen, se pa s tem poravna – ognjilo za porovnati. No, to je vse.', 'v', 1),
(47, 'V_002_003.mp4', 'Tìẹ príːdejo pːlej nə vːrsto [vzame grablje], ze pogrǻːbət al pa ze redìː reskːpat al pa ˈkar-čte. Grǻːble. Grǻːble ìːmajo pa grǻːbəlšče, čelǜːst pa zobjìẹ. Tǻːkəx grːbəl boste víːdlə  skədˈnə – jix je pətnǻːjst. ', 'Te pridejo na vrsto potlej (vzame grablje), za pograbit ali pa za redi razkopat ali pa kar hočete. Grablje. Grablje imajo pa grabljišče, čelust in zobe. Takih grablej boste videli v skednju – jih je petnajst.', 'v', 1),
(48, 'V_002_022.mp4', 'ˈNo, zˈdej grimo pa ne košeníːce. ˈNo. Tùː je ručna kːsa. /../ Tùː je kːsa, tùː se prǻːvə klìẹp. Klìẹp se pa nerdíː, de se kosùọ skliple. Tùː je kúːsje, tùː-j pa kːsa, ˈne. Sklipĺemo jo pa – tùː je ne bǻːpco, je klǻːdu tǻːk, de-j kːntra.', 'No, zdaj gremo pa na košenice. No. To je ročna kosa. /../ to je kosa, temu se pravi klep. Klep se naredi {tako}, da se koso skleplje. To je kosišče, to pa je kosa. Sklepljemo jo pa – to je na babico, je kladivo tako, da je kontra, obratno.', 'v', 1),
(49, 'V_002_024.mp4', 'Tu-j glǻːtka, klǻːdu zə bǻːpco muọra bət pa tǻːk, ku-j tːle, di-j ma pa rùọp, ˈne. Tùː je, se zebìːje u zìẹmlo. Pa še tǻːku sə se nestːvə, de kːsa gːr stojíː.', 'To je gladka, kladivo za babico mora biti pa tako, kot je to, da ima rob, a ne. To se zabije v zemljo. Pa še tako se nastavi, da stoji kosa gori.', 'v', 1),
(50, 'V_002_025.mp4', 'Tùː je pa drǜːgu klepǻːjne, tùː-j novːjši tìːp. ˈNọˑ. Tùː so dːstə zəčìẹle žinske klːpat, po vːjnə, k-je bla síːla, k-néː-blu mːškəx. Tùː-j pa drǜːgu klepǻːjne, jə pa kːntra obːrnjena kːsa. Pər ùːnmə klepǻːjnə se tokùː kliple, de tokùː stojíː, pa se tokùː tːče po bǻːpcə, ˈne. Pər təˈmə, ˈvam bom pa pˈro pokǻːzo, se pa tːčnu obərˈne – ˈvam bom višíːno ˈdu – se pa tokùː kliple, ˈneˑ.', 'To je pa druge vrste priprave za klepanje, to je novejši tip. No. Po vojni so precej začele klepati ženske, ker je bila sila, ker ni bilo moških. To je pa druge vrste priprave za klepanje {kuzla}, je pa kosa obrnjena obratno. Na tisti pripravi se tako kleplje, da tako stoji in se tako tolče po babici, a ne. Pri tem, vam bom prav pokazal, se pa ravno obrne – vam bom dal višino – se pa tako kleplje, a ne.', 'v', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Subcategory`
--

CREATE TABLE `Subcategory` (
  `idSubcategory` int(11) NOT NULL,
  `idCategory` int(11) NOT NULL,
  `name` varchar(90) CHARACTER SET utf8 NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `Subcategory`
--

INSERT INTO `Subcategory` (`idSubcategory`, `idCategory`, `name`, `activated`) VALUES
(1, 1, 'Orodja za sekača in tesača', 1),
(2, 1, 'Orodja za spravilo sena', 1),
(3, 1, 'Orodja za obdelovanje polj', 0),
(4, 1, 'Konjska in volovska oprema', 0),
(5, 1, 'Orodja za izdelavo pohištva (mizarstvo)', 0),
(6, 1, 'Orodja za obdelavo lanu', 0),
(7, 1, 'Orodja za izdelavo volne', 0);

-- --------------------------------------------------------

--
-- Table structure for table `TypeOfConnection`
--

CREATE TABLE `TypeOfConnection` (
  `idTypeOfConnection` int(11) NOT NULL,
  `name` varchar(63) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TypeOfConnection`
--

INSERT INTO `TypeOfConnection` (`idTypeOfConnection`, `name`, `activated`) VALUES
(1, 'Sopomenke', 1),
(2, 'Nadpomenke', 1),
(3, 'Vrste', 1),
(4, 'Orodje', 1),
(5, 'Sestavni deli', 1),
(6, 'Besedje z istim korenom', 1);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `idUser` int(11) NOT NULL,
  `name` varchar(63) CHARACTER SET utf8 NOT NULL,
  `surname` varchar(63) CHARACTER SET utf8 NOT NULL,
  `email` varchar(63) CHARACTER SET utf8 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `type` varchar(1) CHARACTER SET utf8 NOT NULL COMMENT '''s'' - super admin, ''a'' - admin, ''b'' - basic user',
  `resetPwToken` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `resetPwExpiration` int(11) DEFAULT NULL,
  `resetPwUsed` tinyint(1) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`idUser`, `name`, `surname`, `email`, `password`, `type`, `resetPwToken`, `resetPwExpiration`, `resetPwUsed`, `activated`) VALUES
(1, 'Super', 'Admin', 'superadmin@test', '$2y$10$6QZqRs9p/3mOUEVSnu60MuVBeYa24mt945zAIZaWj0amcNwsw7uxC', 's', NULL, NULL, NULL, 1),
(2, 'Admin', 'Admin', 'admin@test', '$2y$10$XBRN5DoVCZbULNkQoj1wL.UUQ33IKHKnhoRuUtk3dAcA4Yb9nWd8C', 'a', '$2y$10$X5xVaqpOl/lP6wL.n5On2eVryezgF4NMDu8vttYN2cK18v/pEtsgq', 1531620500, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`idCategory`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `Connection`
--
ALTER TABLE `Connection`
  ADD PRIMARY KEY (`idConnection`),
  ADD UNIQUE KEY `Connection_UNIQUE` (`idKeyword1`,`idKeyword2`,`idTypeOfConnection`),
  ADD KEY `fk_Connection_1_idx` (`idKeyword1`),
  ADD KEY `fk_Connection_2_idx` (`idKeyword2`),
  ADD KEY `fk_Connection_3_idx` (`idTypeOfConnection`);

--
-- Indexes for table `Keyword`
--
ALTER TABLE `Keyword`
  ADD PRIMARY KEY (`idKeyword`),
  ADD UNIQUE KEY `Keyword_UNIQUE` (`word`,`idSubcategory`),
  ADD KEY `fk_Keyword_1_idx` (`idSubcategory`);

--
-- Indexes for table `Keyword_Record`
--
ALTER TABLE `Keyword_Record`
  ADD PRIMARY KEY (`idKeywordRecord`),
  ADD UNIQUE KEY `idKeyword_Record_UNIQUE` (`idKeyword`,`idRecord`),
  ADD KEY `fk_Keyword_Record_2_idx` (`idRecord`);

--
-- Indexes for table `Picture`
--
ALTER TABLE `Picture`
  ADD PRIMARY KEY (`idPicture`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_Picture_1_idx` (`idKeyword`);

--
-- Indexes for table `Record`
--
ALTER TABLE `Record`
  ADD PRIMARY KEY (`idRecord`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `Subcategory`
--
ALTER TABLE `Subcategory`
  ADD PRIMARY KEY (`idSubcategory`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_Subcategory_1_idx` (`idCategory`);

--
-- Indexes for table `TypeOfConnection`
--
ALTER TABLE `TypeOfConnection`
  ADD PRIMARY KEY (`idTypeOfConnection`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Category`
--
ALTER TABLE `Category`
  MODIFY `idCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Connection`
--
ALTER TABLE `Connection`
  MODIFY `idConnection` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT for table `Keyword`
--
ALTER TABLE `Keyword`
  MODIFY `idKeyword` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `Keyword_Record`
--
ALTER TABLE `Keyword_Record`
  MODIFY `idKeywordRecord` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `Picture`
--
ALTER TABLE `Picture`
  MODIFY `idPicture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `Record`
--
ALTER TABLE `Record`
  MODIFY `idRecord` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `Subcategory`
--
ALTER TABLE `Subcategory`
  MODIFY `idSubcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Connection`
--
ALTER TABLE `Connection`
  ADD CONSTRAINT `fk_Connection_1` FOREIGN KEY (`idKeyword1`) REFERENCES `Keyword` (`idKeyword`),
  ADD CONSTRAINT `fk_Connection_2` FOREIGN KEY (`idKeyword2`) REFERENCES `Keyword` (`idKeyword`),
  ADD CONSTRAINT `fk_Connection_3` FOREIGN KEY (`idTypeOfConnection`) REFERENCES `TypeOfConnection` (`idTypeOfConnection`);

--
-- Constraints for table `Keyword`
--
ALTER TABLE `Keyword`
  ADD CONSTRAINT `fk_Keyword_1` FOREIGN KEY (`idSubcategory`) REFERENCES `Subcategory` (`idSubcategory`);

--
-- Constraints for table `Keyword_Record`
--
ALTER TABLE `Keyword_Record`
  ADD CONSTRAINT `fk_Keyword_Record_1` FOREIGN KEY (`idKeyword`) REFERENCES `Keyword` (`idKeyword`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Keyword_Record_2` FOREIGN KEY (`idRecord`) REFERENCES `Record` (`idRecord`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Picture`
--
ALTER TABLE `Picture`
  ADD CONSTRAINT `fk_Picture_1` FOREIGN KEY (`idKeyword`) REFERENCES `Keyword` (`idKeyword`);

--
-- Constraints for table `Subcategory`
--
ALTER TABLE `Subcategory`
  ADD CONSTRAINT `fk_Subcategory_1` FOREIGN KEY (`idCategory`) REFERENCES `Category` (`idCategory`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
