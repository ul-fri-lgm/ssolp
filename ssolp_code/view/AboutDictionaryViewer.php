<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
	
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "about.css" ?>">
        </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container mt-5">
            <div class="row">
				<div class=".col-md-12">
					<h2 class="SvetloRjava FontCabin NotBold">Predstavitev slovarja</h2>
					
					<p><span class="BoldItalic">Slovar starega orodja v govoru Loškega Potoka (SSOLP)</span> je:
						<ol>
							<li><span class="Bold">narečni slovar</span> govorcev loškopotoškega krajevnega govora dolenjskega narečja dolenjske narečne skupine;</li>
							<li><span class="Bold">tematski slovar</span>, saj je gradivo urejeno po širših in ožjih tematskih oziroma pomenskih poljih;</li>
							<li><span class="Bold">rastoči slovar</span>, ker aplikacija omogoča stalno dopolnjevanje že obstoječih slovarskih sestavkov, dodajanje novih znotraj obstoječih tem in dodajanje novih tem;</li>
							<li><span class="Bold">interaktivni spletni slovar</span>, saj omogoča ogled slikovnega in filmskega gradiva ter poslušanje zvočnih posnetkov.</li>
						</ol>
					</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Vsebinski sklopi</h4>
					<p>V aplikaciji <span class="BoldItalic">Slovar starega orodja v govoru Loškega Potoka</span> je trenutno pod gumbom <span class="Bold">Stara orodja</span> možno izbirati med dvema ožjima pomenskima poljema, med dvema ožjima (pod)temama: <span class="Bold">Orodja za sekača in tesača</span> ter <span class="Bold">Orodja za spravilo sena</span>, ki sta v postopku polnjenja. <span class="Bold">O projektu</span> podaja dodatne informacije o poteku izgradnje slovarja. </p>
					<p>Do slovarskega sestavka pridemo tudi na naslovni strani slovarja z vpisom leksema v iskalno okence; enako možnost imamo tudi na začetku naslovne strani vsake (pod)teme.</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Pojavno okno prevzetega leksema</h4>
					<img class="Picture" src="<?=LOGOS_URL."AboutFig1.png"?>"/>
					<p>S klikom na rjavi gumb s puščico pod sliko katere koli iztočnice odpremo osnovno <span class="Bold">zgradbo slovarskega sestavka</span>: na levi strani vidimo naslov sestavka in sliko predmeta, pod njo ikone za fotografije, video in avdio posnetke, na desni strani pa besedilni del slovarskega sestavka z rjavim gumbom s puščico, ki omogoča dostop do v (pod)temi že obstoječih iztočnic za lekseme, ki so z naslovnim povezane pomensko ali besedotvorno; pri prevzetih besedah je dodan še izvor.</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Zgradba slovarskega stavka</h4>
					<p><span class="Bold">Na desni strani slovarskega sestavka</span> se prikaže besedilni del slovarskega sestavka.</p>
					<p>V prvi vrstici se nahaja <span class="Bold">glasovno poknjižena iztočnica</span> v krepkem tisku, npr. <span class="text-muted2 Bold">plenkača</span>; to je tista, ki je izpisana pod sliko, ko odpremo določeno (pod)temo, oziroma preko katere vstopamo do celotnega slovarskega sestavka.</p>
					<p>Sledita dve obvezni sestavini vsakega slovarskega sestavka; za čim lažje razumevanje širšemu krogu uporabnikov je prva poimenovana izgovor in slovnica, druga pomen:
					<ul>
						<li>pri sestavini <span class="Bold">izgovor in slovnica</span> je zapisana narečna beseda v sodobni slovenski fonetični/fonološki transkripciji, in sicer v osnovni in drugih slovarskih oblikah (pri samostalnikih rodilniška oblika) z dodanim oblikoslovnim označevalnikom (pri samostalnikih za spol, ki hkrati pomeni tudi besedno vrsto samostalnik): <span class="text-muted2 Bold">žǻːtlaka -e, ž</span>;</li>
						<li>pri sestavini <span class="Bold">pomen</span> je pripisan pomen besede (eden ali več); opis je vzet iz <span class="Italic">Slovarja slovenskega knjižnega jezika</span>, <span class="Italic">Pleteršnikovega slovarja</span> ali drugih slovarjev, dostopnih na spletnem portalu Fran; a le v primeru, če se beseda v njih nahaja in če se pomen v loškopotoškem govoru z opisom ujema; včasih je bilo potrebno kakšen pomen tudi dodati, ga natančneje opredeliti, tudi spremeniti, novo dokumentirane besede pa na novo pomensko opredeliti.</li>
					</ul>
					</p>
					<p>Četrta sestavina, tj. <span class="Bold">izvor</span>, je dodan le pri prevzetih besedah, da pokažemo na jezik, iz katerega je bila beseda prevzeta in kaj je v njem (v času prevzema) pomenila (večinoma po slovarju Marka Snoja <span class="Italic">Slovenski etimološki slovar</span>, dostopnem na spletnem portalu Fran, a tudi po drugih virih).</p>
					<p>Peta sestavina, tj. <span class="Bold">sopomenke</span>, je obvezna le v primeru, če za leksem v iztočnici obstaja ena ali več sopomenk, s klikom na ta zelo temno rjavi gumb odpremo slovarski sestavek sopomenke (npr. pri samici amerikanka samica in spuščavnica).</p>
					<p>Do morebitnih <span class="Bold">pomensko ali besedotvorno</span> povezanih slovarskih sestavkov dostopamo s pomočjo rjavega gumba s puščico pod zgornjimi sestavinami; s klikom nanj se nam v različnih barvnih gumbih s puščico prikažejo tiste povezave, ki so bile v slovarju že dokumentirane: nadpomenke, vrste, orodje, sestavni deli in besede z istim korenom:
					<ul>
						<li>če ima beseda <span class="Bold">nadpomenko</span>, se ta izpiše v temno rjavem gumbu (npr. pri amerikanki samici žaga), s klikom pridemo do njenega slovarskega sestavka;</li>
						<li>če obstaja več <span class="Bold">vrst</span> tega orodja, se prikažejo gumbi rjave barve, v njih pa iztočnice, do katerih pridemo s klikom (npr. pri plenkači plenkača za levega, straničarka, lična plenkača ...);</li>
						<li>če ima orodje s tem leksemom <span class="Bold">sestavne dele</span>, se prikažejo gumbi oker barve, v njih pa iztočnice, do katerih pridemo s klikom (npr. pri amerikanki samici kočnik, rožiček, žaga, zob);</li>
						<li>če je leksem poimenovanje za sestavni del kakšnega <span class="Bold">orodja</span>, se prikažejo gumbi svetlo rjave barve, v njih pa iztočnice teh orodij, do katerih pridemo s klikom (npr. pri držaju plenkača, sekira, žatlaka);</li>
						<li>če se v slovarju nahaja več <span class="Bold">besed z istim korenom</span>, se prikažejo gumbi zelene barve, v njih pa iztočnice s temi leksemi, do katerih pridemo s klikom (npr. pri tesaču tesan, tesati, tesanje).</li>
					</ul>
					</p>
					<p>Besedilnega <span class="Bold">ponazarjalnega gradiva</span> v samem slovarskem sestavku na desni strani ni, ker je to večpredstavno podano na levi strani slovarskega sestavka (glej zgoraj).
					</p>
										
					<h4 class="SvetloRjava FontCabin NotBold">Ponazarjalno slikovno, filmsko in zvočno gradivo</h4>
					<p><span class="Bold">Na levi strani</span> slovarskega sestavka se nahaja ponazarjalno slikovno, filmsko in zvočno gradivo k leksikografskemu opisu leksema v iztočnici.</p>
					<p><img class="InlinePicture" src="<?=LOGOS_URL."image.png"?>"/>Fotografije, ki predstavljajo lahko samo naslovni predmet, predmet in ljudi, predmet v funkciji, del predmeta itd., so dveh virov: barvne so bile večinoma posnete v okviru projekta, večina črno-belih je iz digitalnega arhiva Vlada Moharja in so vanj prišle iz družinskih arhivov prebivalcev Loškega Potoka. Ista fotografija se lahko pojavi pri več različnih slovarskih sestavkih.
					</p>
					<img class="Picture" src="<?=LOGOS_URL."AboutFig2.png"?>"/>
					<p><img class="InlinePicture" src="<?=LOGOS_URL."camera.png"?>"/>Video posnetki prikazujejo predmete, njihov opis, sestavne dele, vzdrževanje, uporabo itd. Isti posnetek ali njegov del se lahko pojavi pri več različnih slovarskih sestavkih. Na desni strani je v prvem stolpcu besedilo posnetka v transkribirani obliki, v drugem pa prevedeno v knjižni jezik (z rahlimi odkloni v skladenjski zgradbi, ki zaradi lažje primerljivosti delov besedila bolj sledi narečni). Trenutno pripeti video posnetki so bili narejeni z gospodom Jožetom Anzeljcem iz vasi Mali Log, poklicnim tesačem in kmetom, rojenim 29. 2. 1932, v času snemanja je imel 86 let.
					</p>
					<img class="Picture" src="<?=LOGOS_URL."AboutFig3.png"?>"/>
					<p><img class="InlinePicture" src="<?=LOGOS_URL."audio.png"?>"/>Zvočni posnetki so nastali v vodenih pogovorih z lokalnimi informatorji, tj. s terenskim delom za zbiranje narečnega jezikovnega gradiva za slovar in opis loškopotoškega krajevnega govora. Isti posnetek ali njegov del se lahko pojavi pri več različnih slovarskih sestavkih. Pojavno okno z zvočnim posnetkom je drugačno kot pri filmskem gradivu: na desni strani slovarski sestavek ostane, na levi, na mestu, kjer je v prejšnjem pojavnem oknu video, se v ovalnem okencu pojavi trak s posnetkom, pred njim pa puščica, ki ga s klikom sproži; med njima je tudi sekundni prikaz trajanja in časovnega poteka posnetka. Pod okencem je, enako kot pri videu na desni strani, v prvem stolpcu besedilo posnetka v transkribirani obliki, v drugem pa prevedeno v knjižni jezik.</p>
					<img class="Picture" src="<?=LOGOS_URL."AboutFig4.png"?>"/>
					
					<h4 class="SvetloRjava FontCabin NotBold">Pojavno okno leksema s sopomenkami</h4>
					<img class="Picture" src="<?=LOGOS_URL."AboutFig5.png"?>"/>
					<p>Pojavno okno leksema s sopomenkami in spodaj gumbom, ki odpre iztočnice leksemov, ki so z naslovno iztočnico medleksemsko povezane.</p>
					<p>Obstoj leksemov v slovarju <span class="Bold">ni enak</span> obstoju leksemov v loškopotoškem govoru, ampak je odraz <span class="Bold">trenutnega vnosa</span> v aplikacijo; prav tako so od obstoječega vnosa odvisne povezave na <span class="Bold">medleksemska razmerja</span>.
					</p>
				</div>
			</div>
        </div>
    </body>
</html>
