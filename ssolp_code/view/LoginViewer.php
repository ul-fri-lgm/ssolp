<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style2.css" ?>">
    </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card card-login">
                        <div class="card-header">
                            <form action="<?= BASE_URL . "forgottenPassword" ?>" method="get">
                                Prijava
                                <button class="btn-xs btn-primary float-sm-right" type="submit">Pozabljeno geslo</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <?php if(isset($status)): ?>
                                <div class="alert alert-<?= ($status === "Failure") ? "danger" : (($status === "Success") ? "success" : "info") ?> alert-dismissible" role="alert">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <?= $message ?>
                                </div>
                            <?php endif; ?>
                            <form action="<?= BASE_URL . $formAction ?>" method="post">
                                <div class="form-group">
                                    <label for="email">Vnesi elektronski naslov</label>
                                    <input class="form-control" id="email" name="email" type="email" placeholder="Elektronski naslov" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="password">Vnesi geslo</label>
                                    <input class="form-control" id="password" name="password" type="password" placeholder="Geslo" required>
                                </div>
                                <button id="btn" class="btn btn-primary btn-block" type="submit">Prijavi se</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>