<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
        <script>
            function reload(){
                $("#About").removeClass("invisible");
                $("#oSlovarju").removeClass("invisible");
                $("#DropdownStaraOrodja").removeClass("BorderRight");
            }
        </script>
    </head>
    <body onload="reload()">
        <?php include("view/includes/header.php"); ?>
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-4 mx-auto my-auto" id="SearchContainer">
                    <h1 class="text-center MainText NotBold"> SLOVAR STAREGA <br> ORODJA </h1>
                    <h3 class="text-center MainText NotBold"> v govoru Loškega Potoka </h2>
                    <form id="SearchForm" action="<?= BASE_URL ?>Keyword" method="GET" autocomplete="off">
                        <div class="input-group">
                            <input type="hidden" name="KeywordID" id="KeywordID" value=""/>
                            <input type="hidden" name="KeywordName" id="KeywordName" value=""/>
                            <input type="text" class="form-control rounded" id="SearchInput" placeholder="Iskanje" autofocus>
                            <span class="input-group-btn">
                                <button type="submit" id="SubmitSearch" class="btn btn-default">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        <div id="SearchResultsContainer">
                            <ul id="SearchResults" class="form-control">

                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer id="footer" class="footer bg-white py-2" style="display:none;">
                <div class="row h-100">
                    <div class="col-12 d-lg-flex text-center d-block justify-content-between">
                        <span class="col-12 col-lg-2 d-lg-flex d-block align-items-center text-muted text-center text-lg-left ml-2"> FINANCERJI </span>
                        <div class="">
                            <img class="img-fluid2" src="<?= LOGOS_URL."Financerji/javni_stipendijski.png" ?>" />
                            <img class="mx-0 mx-md-4 img-fluid2" src="<?= LOGOS_URL."Financerji/mizs.png" ?>" />
							<img class="mx-0 mx-md-4 img-fluid2" src="<?= LOGOS_URL."Financerji/EKP.png" ?>" />
                            <img class="mx-0 mx-md-4 img-fluid2" src="<?= LOGOS_URL."Financerji/UL.png" ?>" />
                            <img class="mx-0 mx-md-4 img-fluid2" src="<?= LOGOS_URL."Financerji/os.png" ?>" />
                        </div>   
                    </div>
                </div>
        </footer>
    </body>
</html>