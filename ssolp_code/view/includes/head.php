<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta http-equiv="x-ua-compatible" content="ie=edge">
<title><?= APP_NAME ?></title>

<link rel="icon" type="image/png" href="<?= LOGOS_URL . "favicon-32x32.png" ?>" sizes="32x32" />
<link rel="icon" type="image/png" href="<?= LOGOS_URL . "favicon-16x16.png" ?>" sizes="16x16" />

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "bootstrap.min.css" ?>">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "navbar.css" ?>">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "search.css" ?>">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "font-awesome.min.css" ?>">
<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "datatables.min.css" ?>">

<script type="text/javascript" src="<?= JS_URL . "jquery-3.3.1.min.js" ?>"/></script>
<script type="text/javascript" src="<?= JS_URL . "bootstrap.min.js" ?>"/></script>
<script type="text/javascript" src="<?= JS_URL . "script.js" ?>"></script>
<script type="text/javascript" src="<?= JS_URL . "search.js" ?>"/></script>
<script type="text/javascript" src="<?= JS_URL . "customComparator.js" ?>"></script>
<script type="text/javascript" src="<?= JS_URL . "datatables.min.js" ?>"></script>