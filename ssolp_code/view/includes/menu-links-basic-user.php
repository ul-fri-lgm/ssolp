<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<ul class="navbar-nav ml-auto">
    <li id="DropdownStaraOrodja" class="nav-item dropdown text-center BorderRight">
        <a class="nav-link dropdown-toggle px-5" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">STARA ORODJA</a>
        <div id="DroppDown" class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php foreach($subcategories as $subcategory): ?>
                <a class="dropdown-item align-items-center d-flex justify-content-center" href="<?= BASE_URL . "subcategories/keywords/" . $subcategory["idSubcategory"] ?>"><?= strtoupper($subcategory["SubcategoryName"]) ?></a>
            <?php endforeach; ?>
        </div>
    </li>
    <li id="About" class="nav-item invisible"><a class="nav-link text-center px-5" href="<?= BASE_URL . "aboutTheProject" ?>">O PROJEKTU</a></li>      
    <li id="oSlovarju" class="nav-item invisible"><a class="nav-link text-center px-5" href="<?= BASE_URL . "aboutTheDictionary" ?>">O SLOVARJU</a></li>
    <li class="nav-item"><a class="nav-link text-center px-3" href="<?= BASE_URL . "login" ?>"><i class="fa fa-user" aria-hidden="true"></i></a></li>
</ul>