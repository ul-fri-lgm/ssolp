<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
	
<ul class="navbar-nav mr-auto col-lg-7 offset-lg-2">
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "categories" ?>">Kategorije</a></li>
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "subcategories" ?>">Podkategorije</a></li>
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "keywords" ?>">Gesla</a></li>
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "uploadFile" ?>">Naloži datoteko</a></li>
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "pictures" ?>">Slike</a></li>
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "records" ?>">Posnetki</a></li>
    <!-- Temporarily disable import file menu item for bulk upload of images, audio or video (has to be tested on Linux first) 
	<li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "importData" ?>">Naloži podatke</a></li>
	-->
</ul>

<ul class="navbar-nav mr-auto col-lg-3">
    <li class="nav-item"><a class="nav-link" href="<?= BASE_URL . "editProfile" ?>">Posodobi svoj profil</a></li>
    <li class="nav-item col-lg text-lg-right"><a class="nav-link" href="<?= BASE_URL . "logout" ?>">Odjavi se</a></li>
</ul>
