<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<header>
    <nav class="navbar navbar-expand-lg navbar-light navbar-color">
        <a class="navbar-brand ml-lg-5 ml-1" href="<?= BASE_URL ?>"><img width="100" height="50" src="<?=LOGOS_URL.'logo.png'?>"/></a>
        <button class="navbar-toggler mr-1" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <?php
                if (User::isLoggedIn()):
                    if (User::isLoggedInAsSuperAdmin()):
                        include("menu-links-super-admin.php");
                    elseif (User::isLoggedInAsAdmin()):
                        include("menu-links-admin.php");
                    endif;
                else:
                    include("menu-links-basic-user.php");
                endif;
            ?>
        </div>
    </nav>
</header>
