<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-4" id="SearchContainer">
        <form id="SearchForm" action="<?= BASE_URL . "Keyword" ?>" method="GET" autocomplete="off">
            <div class="input-group">
                <input type="hidden" name="KeywordID" id="KeywordID" value=""/>
                <input type="hidden" name="KeywordName" id="KeywordName" value=""/>
                <input type="text" class="form-control rounded" id="SearchInput" placeholder="Iskanje" autofocus>
                <span class="input-group-btn">
                    <button type="submit" id="SubmitSearch" class="btn btn-default">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </span>
            </div>
            <div id="SearchResultsContainer">
                <ul id="SearchResults" class="form-control">

                </ul>
            </div>
        </form>
    </div>
</div>