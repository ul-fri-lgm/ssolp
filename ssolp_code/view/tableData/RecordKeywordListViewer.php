<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
<head>
    <?php include("view/includes/head.php"); ?>
    <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style2.css" ?>">
    <script>
        $(document).ready(function() {
            // Override the default sorting with a custom one
            jQuery.fn.dataTableExt.oSort["slo-desc"] = function (x, y) {
                return sloCompare(y,x);
            };
            jQuery.fn.dataTableExt.oSort["slo-asc"] = function (x, y) {
                return sloCompare(x,y);
            };
            var oTable = $("#table-record-keyword-list").DataTable({
                // Custom definition for every column
                "aoColumns": [
                    {"sClass": "center", "bSortable": false},
                    {"sClass": "center", "bSortable": true, "sType":"slo"},
                    {"sClass": "center", "bSortable": false}
                ],
                // Ordering in the first column
                "order": [[ 1, 'asc' ]]
            });
            // Dynamic ordering, on sort paramether change
            oTable.on('order.dt search.dt', function() {
                oTable.column(0, {search: 'applied', order: 'applied'})
                .nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
</head>
<body>
<?php include("view/includes/header.php"); ?>
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="content-panel">

                <div class="row">
                    <h2><?= $pageTitle ?></h2>
                    <form action="<?= BASE_URL . $formAction . "/add" ?>" method="get">
                        <div class="col-xs-12 col-md-12">
                            <input class="btn btn-primary btn-block" type="submit" value="Ustvari novo povezavo" />
                        </div>
                    </form>
                </div>
                <?php if(isset($status)): ?>
                    <div class="alert alert-<?= ($status === "Failure") ? "danger" : (($status === "Success") ? "success" : "info") ?> alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= $message ?>
                    </div>
                <?php endif; ?>
                <table id="table-record-keyword-list" class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Ime</th>
                        <th>Izbriši</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($chosenKeywordsOfRecord as $chosenKeywordOfRecord): ?>
                        <tr>
                            <td></td>
                            <td><?= $chosenKeywordOfRecord['word']; ?></td>
                            <td>
                                <form action="<?= BASE_URL . $formAction . "/" . $chosenKeywordOfRecord['idKeyword'] . "/delete" ?>" method="post">
                                    <input class="btn btn-danger btn-sm" type="submit" value="Izbriši" onclick="return confirm('Pozor! S klikom na \'OK\' boste izbrišali povezavo!');"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>
</body>
</html>