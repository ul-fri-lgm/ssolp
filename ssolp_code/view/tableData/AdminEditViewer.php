<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style2.css" ?>">
    </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="content-panel">
                    
                        <h2 class="text-center"><?= $pageTitle ?></h2>
                        <?php if(isset($status)): ?>
                            <div class="alert alert-<?= ($status === "Failure") ? "danger" : (($status === "Success") ? "success" : "info") ?> alert-dismissible" role="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?= $message ?>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-lg-6">
                                <h3>Posodobi podatke izbranega uporabnika</h3>
                                <form action="<?= BASE_URL . $formAction . "/attributes/" . $data['idUser'] ?>" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="name">Spremeni ime</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Ime" value="<?= $data['name'] ?>" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Spremeni priimek</label>
                                        <input type="text" class="form-control" id="surname" name="surname" placeholder="Priimek" value="<?= $data['surname'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Spremeni elektrosnki naslov</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Elektrosnki naslov" value="<?= $data['email'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Potrdi svoje geslo</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Potrdi geslo" required>
                                    </div>
                                    <button id="btn1" class="btn btn-primary btn-block" type="submit">Spremeni</button>
                                </form>
                            </div>

                            <div class="col-lg-6">
                                <h3>Spremeni geslo izbranega uporabnika</h3>
                                <form action="<?= BASE_URL . $formAction . "/password/" . $data['idUser'] ?>" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="password2">Potrdi svoje geslo</label>
                                        <input type="password" class="form-control" id="password2" name="password" placeholder="Potrdi geslo" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="new-password">Vnesi novo geslo</label>
                                        <input type="password" class="form-control" id="new-password" name="new-password" placeholder="Novo geslo" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="retype-password">Potrdi novo geslo</label>
                                        <input type="password" class="form-control" id="retype-password" name="retype-password" placeholder="Novo geslo" required>
                                    </div>
                                    <button id="btn2" class="btn btn-primary btn-block" type="submit">Spremeni</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>
    </body>
</html>