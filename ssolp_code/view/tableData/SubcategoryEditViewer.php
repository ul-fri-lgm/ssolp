<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
<head>
    <?php include("view/includes/head.php"); ?>
    <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style2.css" ?>">
</head>
<body>
<?php include("view/includes/header.php"); ?>
<div class="container">
    <div class="row">

        <div class="col-lg-6 offset-lg-3">
            <div class="content-panel">

                <h2><?= $pageTitle ?></h2>
                <?php if(isset($status)): ?>
                    <div class="alert alert-<?= ($status === "Failure") ? "danger" : (($status === "Success") ? "success" : "info") ?> alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= $message ?>
                    </div>
                <?php endif; ?>

                <form action="<?= BASE_URL . $formAction . "/" . $data['idSubcategory'] ?>" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label for="name">Spremeni ime</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Ime" value="<?= $data['name'] ?>" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="idCategory">Spremeni kategorijo</label>
                        <select class="form-control" id="idCategory" name="idCategory" required>
                            <?php foreach ($allCategories as $category):
                                if ($data['idCategory'] == $category["idCategory"]): ?>
                                    <option selected value="<?= $category["idCategory"] ?>"><?= $category["name"] ?></option>
                                <?php else: ?>
                                    <option value="<?= $category["idCategory"] ?>"><?= $category["name"] ?></option>
                                <?php endif;
                            endforeach; ?>
                        </select>
                    </div>
                    <button id="btn" class="btn btn-primary btn-block" type="submit">Spremeni</button>
                </form>

            </div>
        </div>

    </div>
</div>
</body>
</html>