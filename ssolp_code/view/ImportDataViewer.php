<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style2.css" ?>">
    </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container">
            <div class="row">
                
                <div class="col-lg-6 offset-lg-3">
                    <div class="content-panel">
                    
                        <h2><?= $pageTitle ?></h2>
                        <?php if(isset($status)): ?>
                            <div class="alert alert-<?= ($status === "Failure") ? "danger" : (($status === "Success") ? "success" : "info") ?> alert-dismissible" role="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?= $message ?>
                            </div>
                        <?php endif; ?>
                        
                        <form action="<?= BASE_URL . $formAction ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="fileToUpload">Izberi datoteko za nalaganje:</label>
                                <input type="file" class="form-control" name="fileToUpload" id="fileToUpload" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="choose">Izberi tip</label>
                                <select class="form-control" id="choose" name="choose" required>
                                    <option selected disabled hidden></option>
                                    <option value="a">Audio</option>
                                    <option value="v">Video</option>
                                    <option value="p">Slike</option>
                                </select>
                            </div>
                            
                            <input class="btn btn-primary btn-block" type="submit" value="Submit" />
                        </form>

                    </div>
                </div>
                
            </div>
        </div>
    </body>
</html>