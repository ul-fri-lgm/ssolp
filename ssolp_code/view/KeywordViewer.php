<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->

<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "keyword.css" ?>">
        <script type="text/javascript" src="<?= JS_URL . "AVIManagment.js" ?>"/></script>
        </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container">
            <?php include("view/includes/search.php"); ?><br>
            <?php if (empty($data)): ?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="alert alert-info text-center Barva ml-0">Vaše iskanje ni bilo uspešno.</div>
                    </div>
                </div>
            <?php else: ?>
            <div class="row mb-3">
                <div class="col-4 col-lg-3 NavigationDiv d-flex justify-content-center Kategorija ml-3" id="">
                    <a class="Navigation px-3 align-self-center" href="<?= BASE_URL ?>">1. <?= mb_strtolower($data[0]["category"], "utf-8") ?></a>
                </div>
                <div class="col-4 col-lg-3 NavigationDiv d-flex justify-content-center Podkategorija">
                    <a class="Navigation px-3 align-self-center" href="<?= BASE_URL . "subcategories/keywords/" . $data[0]["idSubcategory"]  ?>">2. <?= mb_strtolower($data[0]["subcategory"], "utf-8") ?></a>
                </div>
                <div class="col-4 col-lg-3 NavigationDiv d-flex justify-content-center Beseda">
                    <a class="Navigation px-3 align-self-center" href="<?= BASE_URL . "Keyword?KeywordID=" . $data[0]["idKeyword"] ?>">3. <?= mb_strtolower($data[0]["word"], "utf-8") ?></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="SvetloRjava"> <?= ucfirst($data[0]["word"]) ?> </h1>
                </div>
                <div class="col-12 col-md-6 px-0" id="AVIContainer">
                    <div id="AVIContainerInner">
                        <!-- Pictures -->
                        <?php if(count($pictures) > 1){ ?>
                            <div id="PictureSlideDiv" class="mt-2 text-left">
                                <img id="PictureSlide" class="ml-0 ml-md-3 img-fluid RoundedPicture" src="<?=IMAGES_URL.$pictures[0]["Picture"]?>"/>
                            </div>
                            <div id="PicturesCarousel" class="carousel slide ml-0 ml-md-3 px-sm-3 py-sm-1 px-0 py-0 d-none text-center" data-ride="carousel">
                                <button id="ClosePicturesCarousel" type="button" class="close float-right" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <!-- Slider -->
                                <div class="carousel-inner px-4 py-1 text-center">
                                    <?php 
                                        for ($x = 0; $x < count($pictures); $x++) {
                                            if($x == 0){
                                                echo '
                                                    <div class="carousel-item text-center active">
                                                        <img class="img-fluid img-carousel" src="'.IMAGES_URL.$pictures[0]["Picture"].'">
                                                    </div>';
                                            }else{
                                                echo '
                                                    <div class="carousel-item text-center">
                                                        <img class="img-fluid img-carousel" src="'.IMAGES_URL.$pictures[$x]["Picture"].'">
                                                    </div>';
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="offset-xs-3 col-xs-6">
                                    <ul class="carousel-indicators list-inline mt-3 w-100">
                                        <?php 
                                            for ($x = 0; $x < count($pictures); $x++) {
                                                if($x == 0){
                                                    echo '
                                                        <li class="list-inline-item active">
                                                            <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#PicturesCarousel">
                                                                <img src="'.IMAGES_URL.$pictures[0]["Picture"].'" class="img-fluid fix">
                                                            </a>
                                                        </li>';
                                                }else{
                                                    echo '
                                                        <li class="list-inline-item">
                                                            <a id="carousel-selector-'.$x.'" data-slide-to="'.$x.'" data-target="#PicturesCarousel">
                                                                <img src="'.IMAGES_URL.$pictures[$x]["Picture"] .'" class="img-fluid fix">
                                                            </a>
                                                        </li>';                                            
                                                }
                                            }
                                        ?>
                                    </ul>
                                    
                                </div>
                            </div>
                        <?php } else if(isset($pictures[0]["Picture"]) && $pictures[0]["Picture"] != "") { ?>
                            <div class="ml-3 mt-2 text-left" id="OnePicture">
                                <img id="OnePictureImage" class="img-fluid RoundedPicture" width="auto" height="100%" src="<?=IMAGES_URL.$pictures[0]["Picture"]?>"/>
                            </div>
                        <?php } else if(!isset($pictures[0]["Picture"])) { ?>
                            <div class="ml-3 mt-2 text-left" id="NoPicture">
                                <img id="NoPictureImage" class="img-fluid RoundedPicture" width="auto" height="100%" src="<?=IMAGES_URL."no_picture.png"?>"/>
                            </div>
                        <?php } ?>
                        <!-- Videos -->
                        <?php if(count($videos) > 1){ ?>
                            <div id="VideosCarousel" class="carousel slide ml-3 d-none" data-ride="carousel" data-interval="false">
                                <!-- Slider -->
                                <div class="carousel-inner">
                                    <?php 
                                        for ($x = 0; $x < count($videos); $x++) {
                                            if($x == 0){
                                                echo '<div class="carousel-item active">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <video class="embed-responsive-item KeywordVideo" src="'.VIDEOS_URL.$videos[0]["Record"].'" type="video/mp4" controls></video>
                                                        </div>
                                                      </div>';
                                            }else{
                                                echo '
                                                    <div class="carousel-item">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <video class="embed-responsive-item KeywordVideo" src="'.VIDEOS_URL.$videos[$x]["Record"].'" type="video/mp4" controls></video>
                                                        </div>
                                                    </div>';                                            
                                            }
                                        }
                                    ?>
                                    
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#VideosCarousel" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#VideosCarousel" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                                </div>
                            </div>
                        <?php } else if(isset($videos[0]["Record"])) { ?>
                            <div id="VideosCarousel" class="embed-responsive embed-responsive-16by9 ml-3 d-none">
                                <video class="embed-responsive-item" src="<?=VIDEOS_URL.$videos[0]["Record"]?>" type="video/mp4" controls></video>
                            </div>
                        <?php } else{ ?>
                            <div id="VideosCarousel" class="NoBackground d-none">
                                <div class="ml-3 mt-2">
                                    <img id="VideoImage" class="img-fluid RoundedPicture" width="auto" height="100%" src="<?=IMAGES_URL. "no_video.jpg"?>"/>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- Audios -->
                        <?php if(count($audios) > 1){ ?>
                            <div id="AudiosCarousel" class="carousel slide ml-3 px-lg-5 py-lg-4 px-sm-3 py-sm-4 px-0 py-0 d-none" data-ride="carousel" data-interval="false">
                                <div class="row">
                                    <div class="col-12 float-right">
                                        <button type="button" class="close float-right mr-2" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <!-- Audio Slider -->
                                <div class="carousel-inner mt-5">
                                    <?php 
                                        for ($x = 0; $x < count($audios); $x++) {
                                            if($x == 0){
                                                echo '<div class="carousel-item active">
                                                        <audio class="ml-4 KeywordAudio" controls>
                                                            <source src="'.AUDIOS_URL.$audios[0]["Record"].'" type="audio/mp3">
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                        <div class="row ml-2 pb-3">
                                                                <div class="col-5 mt-4">
                                                                    <p class="AudioNaslov text-left"> Narečno </p>
                                                                </div>
                                                                <div class="col-5 mt-4 ml-1">
                                                                    <p class="AudioNaslov text-left"> Knjižno </p>
                                                                </div>
                                                                <div class="col-11 mt-3 AudioTra">
                                                                    <div class="row">
                                                                        <div class="col-5 pr-0">
                                                                            <div class="TextAudio text-left">
                                                                                <p class="ZRCola">
                                                                                    '. $audios[0]["RecordTranscription"] .'
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pl-0 offset-1 col-5">
                                                                            <div class="TextAudio text-left">
                                                                                <p class="ZRCola">
                                                                                    '. $audios[0]["RecordTranslation"] .'
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                            }else{
                                                echo '
                                                    <div class="carousel-item text-left">
                                                        <audio class="ml-3 KeywordAudio" controls>
                                                            <source src="'.AUDIOS_URL.$audios[$x]["Record"].'" type="audio/mp3">
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                        <div class="row ml-2 pb-3">
                                                            <div class="col-5 mt-4">
                                                                <p class="AudioNaslov text-left"> Narečno </p>
                                                            </div>
                                                            <div class="col-5 mt-4 ml-1">
                                                                <p class="AudioNaslov text-left"> Knjižno </p>
                                                            </div>
                                                            <div class="col-11 mt-3 AudioTra">
                                                                <div class="row">
                                                                    <div class="col-5 pr-0">
                                                                        <div class="TextAudio text-left">
                                                                            <p class="ZRCola">
                                                                                '. $audios[$x]["RecordTranscription"] .'
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pl-0 offset-1 col-5">
                                                                        <div class="TextAudio text-left">
                                                                            <p class="ZRCola">
                                                                                '. $audios[$x]["RecordTranslation"] .'
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>';                                            
                                            }
                                        }
                                    ?>
                                
                                </div>
                                    
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#AudiosCarousel" data-slide="prev">
                                    <span id="AudiosCarouselPrev" class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#AudiosCarousel" data-slide="next">
                                    <span id="AudiosCarouselNext" class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                        <?php } else if(isset($audios[0]["Record"])) { ?>
                            <div id="AudiosCarousel" class="d-none">
                                <div class="row">
                                    <div class="col-12 float-right">
                                        <button type="button" class="close float-right mr-2" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="text-left">
                                    <audio class="KeywordAudio ml-3" controls>
                                        <source src="<?= AUDIOS_URL.$audios[0]["Record"] ?>" type="audio/mp3">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                                <div class="row ml-2 pb-3">
                                    <div class="col-5 mt-4">
                                        <p class="AudioNaslov text-left"> Narečno </p>
                                    </div>
                                    <div class="col-5 mt-4 ml-1">
                                        <p class="AudioNaslov text-left"> Knjižno </p>
                                    </div>
                                    <div class="col-11 mt-3 AudioTra">
                                        <div class="row">
                                            <div class="col-5 pr-0">
                                                <div class="TextAudio text-left">
                                                    <p class="ZRCola">
                                                        <?= $audios[0]["RecordTranscription"] ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="pl-0 offset-1 col-5">
                                                <div class="TextAudio text-left">
                                                    <p class="ZRCola">
                                                        <?= $audios[0]["RecordTranslation"] ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else{ ?>
                            <div id="AudiosCarousel" class="NoBackground d-none">
                                <div id="NoAudio" class="ml-3 mt-2">
                                    <img id="AudioImage" class="img-fluid RoundedPicture" width="auto" height="100%" src="<?=IMAGES_URL. "no_audio.jpg"?>"/>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <br>
                    <div class="row text-center" id="AVI">
                        <div class="col-6">
                            <div class="row">
                                <div class="SmallAVI col-4" id="Image">
                                    <img id="Foto" src="<?=LOGOS_URL.'image.png'?>"/>
                                </div>
                                <div class="SmallAVI col-4" id="Video">
                                    <img id="Kamera" src="<?=LOGOS_URL.'camera.png'?>"/>
                                </div>
                                <div class="SmallAVI col-4" id="Audio">
                                    <img id="Zvok" src="<?=LOGOS_URL.'audio.png'?>"/>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 offset-md-1 col-md-5 mt-1" id="KeywordDetails">
                    <p class="FontBookman"><span class="text-muted2 ZRColaBold"><?=$data[0]["word"]?></span></p>
                    <p class="FontBookman">izgovor in slovnica: <span class="text-muted2 ZRColaBold"> <?= trim($data[0]["pronunciation"]) ?>, <?= $data[0]["grammar"] ?> </span></p>
                    <p class="">pomen:<span class="text-muted2 ZRColaBold"> <?= $data[0]["meaning"] ?> </span></p>
                    <?php if(!empty($data[0]["etymology"])) { ?>
                        <p class="">izvor: <span class="text-muted2 ZRColaBold"><?= $data[0]["etymology"] ?></span></p>
                    <?php } ?>
                    <?php if(!empty($Sopomenke)){ ?>
                    <p class="">sopomenke:<?php
                        foreach ($Sopomenke as $sopomenka => $idSopomenka){
                            echo "<a href='".BASE_URL."Keyword?KeywordID=".$idSopomenka."' class='sopomenke d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idSopomenka'>".str_replace(' ', '&nbsp', $sopomenka)."</a> ";
                        }
                    ?></p>
                    <?php } ?>
                    <?php if(!empty($SestavniDeli) || !empty($Nadpomenke) || !empty($OrodjaZaVzdrzevanje) || !empty($BesedeIstiKoren) || !empty($Vrste)){ ?>
                    <div id="MoreInfoDiv" class="MoreInfo text-right vertical-center">
                        <img id="ImgMoreInfo" class="img-fluid" src="<?= LOGOS_URL."biggerthan.png" ?>"/>
                    </div>
                    <?php } ?>
                    <div id="MoreInfoKeyword" class="d-none">
                        <?php if(!empty($Nadpomenke)) { ?>
                        <p class="">nadpomenke: <?php
                            foreach ($Nadpomenke as $Nadpomenka => $idNadpomenka){
                                echo "<a href='".BASE_URL."Keyword?KeywordID=".$idNadpomenka."' class='nadpomenke d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idNadpomenka'>".str_replace(' ', '&nbsp', $Nadpomenka)."</a> ";
                            }
                        ?></p>
                        <?php } ?>
                        <?php if(!empty($Vrste)) { ?>
                        <p class="">vrste: <?php
                            foreach ($Vrste as $Vrst => $idVrst){
                                echo "<a href='".BASE_URL."Keyword?KeywordID=".$idVrst."' class='vrste d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idVrst'>".str_replace(' ', '&nbsp', $Vrst)."</a> ";
                            }
                        ?></p>
                        <?php } ?>
                        <?php if(!empty($OrodjaZaVzdrzevanje)) { ?>
                        <p class="">orodja: <?php
                            foreach ($OrodjaZaVzdrzevanje as $Orodje => $idOrodje){
                                echo "<a href='".BASE_URL."Keyword?KeywordID=".$idOrodje."' class='OrodjaZaVzdrzevanje d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idOrodje'>".str_replace(' ', '&nbsp', $Orodje)."</a> ";
                            }
                        ?></p>
                        <?php } ?>
                        <?php if(!empty($SestavniDeli)) { ?>
                        <p class="">sestavni deli: <?php
                            foreach ($SestavniDeli as $SestavniDel => $idSestavniDel){
                                echo "<a href='".BASE_URL."Keyword?KeywordID=".$idSestavniDel."' class='sestavniDeli d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idSestavniDel'>".str_replace(' ', '&nbsp', $SestavniDel)."</a> ";
                            }
                        ?></p>
                        <?php } ?>
                        <?php if(!empty($BesedeIstiKoren)) { ?>
                        <p class="FontBookman">besedje z istim korenom: <?php
                            foreach ($BesedeIstiKoren as $IstiKoren => $idIstiKoren){
                                echo "<a href='".BASE_URL."Keyword?KeywordID=".$idIstiKoren."' class='BesedeIstiKoren d-inline-block px-3 mt-1 rounded-keyword clickable' id='$idIstiKoren'>".str_replace(' ', '&nbsp', $IstiKoren)."</a> ";
                            }
                        ?></p>
                        <?php } ?>
                    </div>
                </div>
                <?php if(isset($videos[0]["Record"])){ ?>
                <div class="col-12 ml-3 ml-md-0 col-md-5 mt-2 d-none" id="VideoDetails">
                    <div class="row">
                        <div class="col-12 float-right">
                            <button type="button" class="close float-right" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <p class="Cabin">Narečno</p>
                        </div>
                        <div class="col-6 pl-2">
                            <p class="Cabin">Knjižno</p>
                        </div>
                    </div>
                    <div class="row mt-0" id="VideoDetailsText">
                        <?php
                            for ($x = 0; $x < count($videos); $x++) {
                                if($x == 0){ ?>
                                    <div id="SlideText<?= $x ?>1" class="col-6 SlideText">
                                        <p class="ZRCola"> <?= $videos[$x]["RecordTranscription"] ?> </p>
                                    </div>
                                    <div id="SlideText<?= $x ?>2" class="col-6 SlideText">
                                        <p class="ZRCola"> <?= $videos[$x]["RecordTranslation"] ?> </p>
                                    </div>
                        <?php   
                                }else{
                                ?>
                                    <div id="SlideText<?= $x ?>1" class="col-6 SlideText d-none">
                                        <p class="ZRCola"> <?= $videos[$x]["RecordTranscription"] ?> </p>
                                    </div>
                                    <div id="SlideText<?= $x ?>2" class="col-6 SlideText d-none">
                                        <p class="ZRCola"> <?= $videos[$x]["RecordTranslation"] ?> </p>
                                    </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php endif; ?>
        </div>
    </body>
</html>
