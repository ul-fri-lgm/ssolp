<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "keyword.css" ?>">
        <script type="text/javascript" src="<?= JS_URL . "AVIManagment.js" ?>"/></script>
        <script type="text/javascript" src="<?= JS_URL . "ImageManagment.js" ?>"/></script>
        </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container">
            <?php include("view/includes/search.php"); ?>
            <br>
            <div class="row mb-3">
                <div class="col-6 col-md-3 NavigationDiv d-flex justify-content-center Kategorija ml-3" id="">
                    <a class="Navigation px-3 align-self-center" href="<?= BASE_URL ?>">1. <?= mb_strtolower($chosen["CategoryName"], "utf-8") ?></a>
                </div>
                <div class="col-6 col-md-3 NavigationDiv d-flex justify-content-center Podkategorija">
                    <a class="Navigation px-3 align-self-center" href="<?= BASE_URL . "subcategories/keywords/" . $chosen["idSubcategory"] ?>">2. <?= mb_strtolower($chosen["name"], "utf-8") ?></a>
                </div>
            </div>
            <h2 class="SvetloRjava mb-4 FontCabin NotBold">
                <?= $chosen["name"] ?>
            </h2>
            <?php if (empty($besede)): ?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="alert alert-info text-center Barva ml-0">V izbrani podkategoriji ni gesel.</div>
                    </div>
                </div>
            <?php else: ?>
            <div class="row mx-auto w-100">
                <?php foreach($besede as $counter => $keyword): ?>
                    <div class="col-6 col-md-3 pl-0 pr-4 mb-5 text-center">
                        <div class="PictureKeywordContainer">
                            <img id="KeywordSubImage" class="img-fluid PictureKeywordSubcategory" src="<?=IMAGES_URL.$keyword['picture']?>"/>
                        </div>
                        <div id="KeywordSubDown" class="bg-light text-center py-3">
                            <p class="Siva FontBookman"> <?= $keyword['word'] ?> </p>
                            <a href="<?= BASE_URL . "Keyword?KeywordID=".$keyword['idKeyword'] ?>">
                                <div class="mx-auto h-50 ToKeyword text-right">
                                    <img class="img-fluid" src="<?= LOGOS_URL."biggerthan.png" ?>"/>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
    </body>
</html>