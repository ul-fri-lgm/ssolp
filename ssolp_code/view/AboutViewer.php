<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->

<!DOCTYPE html>

<html lang="en">
    <head>
        <?php include("view/includes/head.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL . "about.css" ?>">
    </head>
    <body>
        <?php include("view/includes/header.php"); ?>
        <div class="container mt-5">
            <div class="row">
				<div class=".col-md-12">

					<h2 class="SvetloRjava FontCabin NotBold">Projekt Slovar starega orodja v govoru Loškega Potoka</h2>

					<p>Projekt Slovar starega orodja v govoru Loškega Potoka je Študentski inovativni projekt za družbeno korist (ŠIPK) 2016-18. <br><br>
						Cilji projekta so naslednji:
						<ul>
							<li>začetek avdio in video dokumentacije snovne in nesnovne kulturne dediščine,</li>
							<li>znanstveni zapis (v fonetični transkripciji) narečnih poimenovanj za stara orodja, pripomočke in opravila,</li>
							<li>obstoječe zbirke predmetov opremiti s podnapisi v knjižni in narečni slovenščini ter angleščini, </li>
							<li>izdelati interaktivni tematski narečni slovar starega orodja kot zasnovo splošnega narečnega slovarja s celostno grafično podobo,</li>
							<li>lokalno skupnost različnih generacij še dodatno motivirati k ohranjanju (ne)snovne kulturne dediščine in tako prispevati k razvoju turizma.</li>
						</ul>
					</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Avtorja aplikacije</h4>
					<p> Avtorja spletne aplikacije sta Aleksandar Nusheski in Dimitrije Mitić, študenta Fakultete za računalništvo in informatiko Univerze v Ljubljani. Aplikacijo sta izdelala v okviru projekta ŠIPK Slovar starega orodja v govoru Loškega Potoka v letu 2018 pod mentorstvom viš. pred. dr. Alenke Kavčič (UL FRI).
					</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Aplikacija</h4>
					<p>Slovar starega orodja v govoru Loškega Potoka (SSOLP)<br>
					   Copyright &copy; 2018  Aleksandar Nusheski (an0548&commat;student.uni-lj.si) &amp; Dimitrije Mitić (dm0935&commat;student.uni-lj.si)</p>
					<p>Ta program spada med prosto programje; lahko ga razširjate in/ali spreminjate pod pogoji Splošnega dovoljenja GNU (GNU General Public License), različice 3, kot ga je objavila ustanova Free Software Foundation.</p>
					<p>Ta program se razširja v upanju, da bo uporaben, vendar BREZ VSAKRŠNEGA JAMSTVA; tudi brez posredne zagotovitve CENOVNE VREDNOSTI ali PRIMERNOSTI ZA DOLOČEN NAMEN. Za podrobnosti glejte besedilo GNU General Public License.</p>
					<p>Skupaj s tem programom bi morali prejeti izvod Splošnega dovoljenja GNU (GNU General Public License). Podrobnosti licence so dostopne tudi na spletni strani <a href="http://www.gnu.org/licenses" target="_blank">http://www.gnu.org/licenses</a>.</p>
					<p>Izvorna koda aplikacije je dosegljiva v repozitoriju Bitbucket: <a href="https://bitbucket.org/ul-fri-lgm/ssolp" target="_blank">https://bitbucket.org/ul-fri-lgm/ssolp</a>.</p>
			
					<h4 class="SvetloRjava FontCabin NotBold">Oblikovanje in video</h4>
					<p>Spletno aplikacijo je oblikovala Elena Plahuta, video in foto gradivo je posnela in uredila Špela Bricman, študentki Fakultete za naravoslovje in tehnologijo Univerze v Ljubljani, v okviru projekta ŠIPK Slovar starega orodja v govoru Loškega Potoka v letu 2018 pod mentorstvom izr. prof. dr. Helene Gabrijelčič Tomc (UL NTF).
					</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Vsebina</h4>
					<p>Vsebino Slovarja starega orodja v govoru Loškega Potoka (SSOLP) za temi Orodja za sekača in tesača ter Orodja za spravilo sena so na terenu posneli, uredili, zasnovali in slovarska gesla izdelali študenti Filozofske fakultete Univerze v Ljubljani pod mentorstvom prof. dr. Vere Smole Smole (UL FF): Špela Zupančič, Rok Mrvič, Dejan Gabrovšek in Maja Keržič. Za organizacijo terenskega dela je poskrbela profesorica slovenščine Bogdana Mohar z Osnovne šole dr. Antona Debeljaka Loški Potok, z različnimi zadolžitvami so sodelovali še študentje domačini Karmen Krnc (UL FF), Rok Bartol (VŠUP Grm) in Sebastian Bambič (UL BF).</p>
					
					<h4 class="SvetloRjava FontCabin NotBold">Pisava ZRCola</h4>
					<p>Narečno besedilo je bilo pripravljeno z vnašalnim sistemom ZRCola (<a href="http://zrcola.zrc-sazu.si" target="_blank">http://zrcola.zrc-sazu.si</a>), ki ga je na Znanstvenoraziskovalnem centru SAZU v Ljubljani (<a href="http://www.zrc-sazu.si" target="_blank">http://www.zrc-sazu.si</a>) razvil Peter Weiss.</p>
			
					<p class="Small">Zadnja sprememba: julij 2018, Alenka Kavčič</p>
					<p>&nbsp;</p>
				</div>
            </div>
        </div>
    </body>
</html>