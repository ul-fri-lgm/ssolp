<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

session_start();

require_once("controller/LoginController.php");
require_once("controller/SuperAdminController.php");
require_once("controller/AdminController.php");
require_once("controller/BasicUserController.php");

define("APP_NAME", "Slovar orodja");
define("ROOT_DIRECTORY", dirname(__FILE__) . DIRECTORY_SEPARATOR);
define("BASE_URL", $_SERVER["SCRIPT_NAME"] . "/");
define("PROJECT_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("VIDEOS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/videos/");
define("AUDIOS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/audios/");
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("LOGOS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/logos/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/css/");
define("JS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/js/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";
// ROUTER: defines mapping between URLS and controllers
$urls = [
    "/^$/" => function ($method) {
        if ($method == "GET"){
            BasicUserController::homeForm();
            User::setURL("index");
        }
        else ViewHelper::error405();
    },
	"/^Keyword$/" => function($method){
        if ($method == "GET") {
            BasicUserController::KeywordDetails();
            User::setURL("Keyword");
        }
        else ViewHelper::error405();
    },
    "/^subcategories\/keywords\/(\d+)$/" => function($method, $id){
        if ($method == "GET"){ 
            BasicUserController::GetAllKeywordsFromSubcategory($id);
            User::setURL("AllKeywordsInSubcategory");
        }
        else ViewHelper::error405();
    },
    "/^Search$/" => function($method){
        if ($method == "GET"){
            BasicUserController::SearchDatabase();
            User::setURL("Search");
        }
        else ViewHelper::error405();
    },
    "/^aboutTheProject$/" => function($method){
        if ($method == "GET"){
            BasicUserController::ShowAbout();
            User::setURL("About");
        }
        else ViewHelper::error405();
    },
    "/^aboutTheDictionary$/" => function($method){
        if ($method == "GET"){
            BasicUserController::ShowAboutDictionary();
            User::setURL("About");
        }
        else ViewHelper::error405();
    },

    "/^login$/" => function ($method) {
        if ($method == "POST") LoginController::login();
        else if ($method == "GET") LoginController::loginForm();
        else ViewHelper::error405();
    }, "/^logout$/" => function ($method) {
        if ($method == "GET") LoginController::logout();
        else ViewHelper::error405();
    }, "/^forgottenPassword$/" => function ($method) {
        if ($method == "POST") LoginController::forgottenPassword();
        else if ($method == "GET") LoginController::forgottenPasswordForm();
        else ViewHelper::error405();
    }, "/^resetPassword$/" => function ($method) {
        if ($method == "POST") LoginController::resetPassword();
        else if ($method == "GET") LoginController::resetPasswordForm();
        else ViewHelper::error405();
    },

    "/^editProfile$/" => function ($method) {
        if ($method == "GET") BasicUserController::editOwnProfileForm();
        else ViewHelper::error405();
    }, "/^editProfile\/attributes$/" => function ($method) {
        if ($method == "POST") BasicUserController::editOwnProfileAttributes();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "editProfile");
        else ViewHelper::error405();
    }, "/^editProfile\/password$/" => function ($method) {
        if ($method == "POST") BasicUserController::editOwnProfilePassword();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "editProfile");
        else ViewHelper::error405();
    },

    "/^admins$/" => function ($method) {
        if ($method == "GET") SuperAdminController::adminList();
        else ViewHelper::error405();
    }, "/^admins\/add$/" => function ($method) {
        if ($method == "POST") SuperAdminController::adminAdd();
        else if ($method == "GET") SuperAdminController::adminAddForm();
        else ViewHelper::error405();
    }, "/^admins\/edit\/(\d+)$/" => function ($method, $id) {
        if ($method == "GET") SuperAdminController::adminEditForm($id);
        else ViewHelper::error405();
    }, "/^admins\/edit\/attributes\/(\d+)$/" => function ($method, $id) {
        if ($method == "POST") SuperAdminController::adminAttributesEdit($id);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "admins/edit/" . $id);
        else ViewHelper::error405();
    }, "/^admins\/edit\/password\/(\d+)$/" => function ($method, $id) {
        if ($method == "POST") SuperAdminController::adminPasswordEdit($id);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "admins/edit/" . $id);
        else ViewHelper::error405();
    }, "/^admins\/toogleActivity$/" => function ($method) {
        if ($method == "POST") SuperAdminController::toggleAdminsActivity();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "admins");
        else ViewHelper::error405();
    },
    
    "/^categories$/" => function ($method) {
        if ($method == "GET") AdminController::CategoryList();
        else ViewHelper::error405();
    }, "/^categories\/add$/" => function ($method) {
        if ($method == "POST") AdminController::CategoryAdd();
        else if ($method == "GET") AdminController::CategoryAddForm();
        else ViewHelper::error405();
    }, "/^categories\/edit\/(\d+)$/" => function ($method, $id) {
        if ($method == "POST") AdminController::CategoryEdit($id);
        else if ($method == "GET") AdminController::CategoryEditForm($id);
        else ViewHelper::error405();
    }, "/^categories\/toogleActivity$/" => function ($method) {
        if ($method == "POST") AdminController::CategoryToggleActivity();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "categories");
        else ViewHelper::error405();
    },

    "/^subcategories$/" => function ($method) {
        if ($method == "GET") AdminController::SubcategoryList();
        else ViewHelper::error405();
    }, "/^subcategories\/add$/" => function ($method) {
        if ($method == "POST") AdminController::SubcategoryAdd();
        else if ($method == "GET") AdminController::SubcategoryAddForm();
        else ViewHelper::error405();
    }, "/^subcategories\/edit\/(\d+)$/" => function ($method, $id) {
        if ($method == "POST") AdminController::SubcategoryEdit($id);
        else if ($method == "GET") AdminController::SubcategoryEditForm($id);
        else ViewHelper::error405();
    }, "/^subcategories\/toogleActivity$/" => function ($method) {
        if ($method == "POST") AdminController::SubcategoryToggleActivity();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "subcategories");
        else ViewHelper::error405();
    },
    
    "/^keywords$/" => function ($method) {
        if ($method == "GET") AdminController::KeywordList();
        else ViewHelper::error405();
    }, "/^keywords\/add$/" => function ($method) {
        if ($method == "POST") AdminController::KeywordAdd();
        else if ($method == "GET") AdminController::KeywordAddForm();
        else ViewHelper::error405();
    }, "/^keywords\/edit\/(\d+)$/" => function ($method, $id) {
        if ($method == "POST") AdminController::KeywordEdit($id);
        else if ($method == "GET") AdminController::KeywordEditForm($id);
        else ViewHelper::error405();
    }, "/^keywords\/toogleActivity$/" => function ($method) {
        if ($method == "POST") AdminController::KeywordToggleActivity();
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "keywords");
        else ViewHelper::error405();
    },

    "/^keywords\/connections\/(\d+)$/" => function ($method, $id) {
        if ($method == "GET") AdminController::KeywordConnectionList($id);
        else ViewHelper::error405();
    }, "/^keywords\/connections\/(\d+)\/add$/" => function ($method, $id) {
        if ($method == "POST") AdminController::KeywordConnectionAdd($id);
        else if ($method == "GET") AdminController::KeywordConnectionAddForm($id);
        else ViewHelper::error405();
    }, "/^keywords\/connections\/(\d+)\/delete\/(\d+)$/" => function ($method, $id1, $id2) {
        if ($method == "POST") AdminController::KeywordConnectionDelete($id1, $id2);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "keywords/connections/" . $id1);
        else ViewHelper::error405();
    },
    
    "/^uploadFile$/" => function ($method) {
        if ($method == "POST") AdminController::UploadFile();
        else if ($method == "GET") AdminController::UploadFileForm();
        else ViewHelper::error405();
    }, "/^pictures$/" => function ($method) {
        if ($method == "GET") AdminController::PictureList();
        else ViewHelper::error405();
    }, "/^pictures\/(\d+)\/delete$/" => function ($method, $id) {
        if ($method == "POST") AdminController::PictureDelete($id);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "pictures");
        else ViewHelper::error405();
    }, "/^records$/" => function ($method) {
        if ($method == "GET") AdminController::RecordList();
        else ViewHelper::error405();
    }, "/^records\/(\d+)\/delete$/" => function ($method, $id) {
        if ($method == "POST") AdminController::RecordDelete($id);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "records");
        else ViewHelper::error405();
    },
    
    "/^pictures\/(\d+)\/keywords$/" => function ($method, $id) {
        if ($method == "GET") AdminController::PictureAddOrEditKeywordForm($id);
        else if ($method == "POST") AdminController::PictureAddOrEditKeyword($id);
        else ViewHelper::error405();
    }, "/^records\/(\d+)\/keywords$/" => function ($method, $id) {
        if ($method == "GET") AdminController::KeywordRecordListForm($id);
        else ViewHelper::error405();
    }, "/^records\/(\d+)\/keywords\/add$/" => function ($method, $id) {
        if ($method == "GET") AdminController::KeywordRecordAddForm($id);
        else if ($method == "POST") AdminController::KeywordRecordAdd($id);
        else ViewHelper::error405();
    }, "/^records\/(\d+)\/keywords\/(\d+)\/delete$/" => function ($method, $id1, $id2) {
        if ($method == "POST") AdminController::KeywordRecordDelete($id1, $id2);
        else if ($method == "GET") ViewHelper::redirect(BASE_URL . "records/" . $id1 . "/keywords");
        else ViewHelper::error405();
    },
    
    "/^importData$/" => function ($method) {
        if ($method == "POST") AdminController::ImportData();
        else if ($method == "GET") AdminController::ImportDataForm();
        else ViewHelper::error405();
    }
];

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            //ViewHelper::error404();
            echo $e;
        } catch (Exception $e) {
            //ViewHelper::displayError($e, true);
            echo $e;
        }
        exit();
    }
}

ViewHelper::error404();
