<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "Mail.php"; // Pear Mail Library
require_once("model/UserModel.php");
require_once("model/User.php");
require_once("includes/Validation.php");
require_once("ViewHelper.php");

class LoginController {
    public static function loginForm($status = null, $message = null) {
        if (!User::isLoggedIn()){
            ViewHelper::render("view/LoginViewer.php", [
                "formAction" => "login",
                "subcategories" => SubcategoryModel::getAll(),
                "status" => $status,
                "message" => $message
            ]);
        }else{
            ViewHelper::render("view/DisplayMessageViewer.php", [
                "subcategories" => SubcategoryModel::getAll(),
                "status" => "Failure",
                "message" => "Prijavljeni ste. Če se želite prijaviti z drugim elektronskim naslovom, se najprej odjavite in poskusite znova."
            ]);
        }
    }
    
    public static function login() {
        $data = filter_input_array(INPUT_POST, [
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            $user = UserModel::getUser($data["email"], $data["password"]);
            if ($user != null) {
                User::login($user);
                ViewHelper::redirect(BASE_URL);
            } else {
                self::loginForm("Failure", "Napaka, napačen elektronski naslov ali geslo. Poskusite znova.");
            }
        } else {
            self::loginForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function logout() {
        User::logout();
        ViewHelper::redirect(BASE_URL);
    }

    public static function forgottenPasswordForm($status = null, $message = null) {
        if (!User::isLoggedIn()){
            ViewHelper::render("view/ForgottenPasswordViewer.php", [
                "formAction" => "forgottenPassword",
                "subcategories" => SubcategoryModel::getAll(),
                "status" => $status,
                "message" => $message
            ]);
        }else{
            ViewHelper::render("view/DisplayMessageViewer.php", [
                "subcategories" => SubcategoryModel::getAll(),
                "status" => "Failure",
                "message" => "Prijavljeni ste. Če želite spremeniti geslo prek elektronskega naslova, se najprej odjavite in poskusite znova."
            ]);
        }
    }

    public static function forgottenPassword() {
        $data = filter_input_array(INPUT_POST, [
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            if (UserModel::checkEmail($data["email"])) {
                $token =  base64_encode(openssl_random_pseudo_bytes(30));
                $url = $_SERVER['HTTP_HOST'] . BASE_URL . "resetPassword?token=" . urlencode($token) . "&email=" . urlencode($data["email"]);
                $from = '<ssolp.app@gmail.com>';
                $to = '<' . $data["email"] . '>';
                $subject = '[Slovar orodja] Zahtevek za ponastavitev gesla';
                
                $headers = array(
                    'From' => $from,
                    'To' => $to,
                    'Subject' => $subject,
                    'MIME-Version' => 1,
                    'Content-type' => 'text/html; charset=UTF-8'
                );
                
                // TODO: change to https protocol in production
                $body = "Pozdravljeni,<br><br>Prosili ste za ponastavitev gesla. Ponastavite ga lahko prek naslednje povezave:" .
                    "<br><br><a href=http://" . $url . " target='_blank'>http://" . $url . "</a><br><br>" .
                    "Če povezave ne boste uporabili v eni uri, bo potekla.<br>" .
                    "Če ne želite ponastavitve gesla, spreglejte to sporočilo.<br><br>" .
                    "Lep pozdrav,<br>skrbnik spletne strani slovar orodja.";
                
                $smtp = Mail::factory('smtp', array(
                    'host' => 'ssl://smtp.gmail.com',
                    'port' => '465',
                    'auth' => true,
                    'username' => 'ssolp.app@gmail.com',
                    'password' => 'gHQV5D1we3Mga1iv'
                ));
                
                $mail = $smtp->send($to, $headers, $body);
                
                if (!PEAR::isError($mail)) {
                    $data["token"] = $token;
                    $data["expiration"] = mktime(date("H")+1, date("i"), date("s"), date("m"), date("d"), date("Y"));
                    UserModel::saveToken($data);
                    ViewHelper::render("view/DisplayMessageViewer.php", [
                        "subcategories" => SubcategoryModel::getAll(),
                        "status" => "Success",
                        "message" => "Sporočilo je bilo uspešno poslano. Na svoj elektronski naslov ste prejeli povezavo, preko katere lahko ponastavite geslo."
                    ]);
                } else {
                    //echo('<p>' . $mail->getMessage() . '</p>');
                    self::forgottenPasswordForm("Failure", "Napaka, sporočilo ni bilo poslano. Poskusite znova.");
                }
            } else {
                self::forgottenPasswordForm("Failure", "Napaka, elektronski naslov ne obstaja. Poskusite znova.");
            }
        } else {
            self::forgottenPasswordForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function resetPasswordForm() {
        $data = filter_input_array(INPUT_GET, [
            "token" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        
        if (Validation::checkValues($data)) {
            if (!User::isLoggedIn()){
                $data["expiration"] = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
                if (UserModel::checkToken($data)) {
                    ViewHelper::render("view/ResetPasswordViewer.php", [
                        "formAction" => "resetPassword",
                        "subcategories" => SubcategoryModel::getAll()
                    ]);
                } else {
                    self::forgottenPasswordForm("Failure", "Napaka, žeton ne obstaja, bil je že uporabljen ali je potekel. Poskusite znova.");
                }
            }else{
                ViewHelper::render("view/DisplayMessageViewer.php", [
                    "subcategories" => SubcategoryModel::getAll(),
                    "status" => "Failure",
                    "message" => "Prijavljeni ste. Če želite spremeniti geslo prek elektronskega naslova, se najprej odjavite in poskusite znova."
                ]);
            }
        } else {
            self::forgottenPasswordForm("Failure", "Napaka, zeton ni v veljaven format. Poskusite znova.");
        }
    }

    public static function resetPassword() {
        $data = filter_input_array(INPUT_POST, [
            "token" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "new-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "retype-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        
        if (Validation::checkValues($data)) {
            if ($data["new-password"]==$data["retype-password"]) {
                if (UserModel::changePasswordUsingToken($data)) {
                    self::loginForm("Success", "Uspešno ste spremenili geslo svojega profila.");
                } else {
                    ViewHelper::render("view/DisplayMessageViewer.php", [
                        "subcategories" => SubcategoryModel::getAll(),
                        "status" => "Failure",
                        "message" => "Napaka, žeton ne obstaja. Ponovno odprite povezavo, ki ste jo prejeli na elektronski naslov."
                    ]);
                }
            } else {
                ViewHelper::render("view/DisplayMessageViewer.php", [
                    "subcategories" => SubcategoryModel::getAll(),
                    "status" => "Failure",
                    "message" => "Napaka, gesla niso enaka. Ponovno odprite povezavo, ki ste jo prejeli na elektronski naslov."
                ]);
            }
        } else {
            ViewHelper::render("view/DisplayMessageViewer.php", [
                "subcategories" => SubcategoryModel::getAll(),
                "status" => "Failure",
                "message" => "Napaka, vnos ni veljaven. Poskusite znova."
            ]);
        }
    }
}