<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once("model/User.php");
require_once("model/UserModel.php");
require_once("includes/Validation.php");
require_once("ViewHelper.php");

class SuperAdminController {
    public static function adminAddForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsSuperAdmin()){
                ViewHelper::render("view/tableData/AdminAddViewer.php", [
                    "pageTitle" => "Ustvari novega uporabnika",
                    "formAction" => "admins/add",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function adminAdd() {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "surname" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "retype-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            if (UserModel::isUnique($data)) {
                if ($data["password"]==$data["retype-password"]) {
                    UserModel::insert($data);
                    self::adminList("Success", "Uspešno ste ustvarili novega uporabnika.");
                } else {
                    self::AdminAddForm("Failure", "Napaka, gesla niso enaka. Poskusite znova.");
                }
            } else {
                self::AdminAddForm("Failure", "Napaka, uporabnik s tem elektronskim naslovom že obstaja. Poskusite znova.");
            }
        } else {
            self::AdminAddForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function adminList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsSuperAdmin()) {
                $allData = UserModel::getAll();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Ustvarite enega!";
                }

                ViewHelper::render("view/tableData/AdminListViewer.php", [
                    "pageTitle" => "Seznam vseh uporabnikov",
                    "allData" => $allData,
                    "formAction" => "admins",
                    "status" => $status,
                    "message" => $message
                ]);
            } else {
                ViewHelper::error403();
            }
        } else {
            ViewHelper::error401();
        }
    }

    public static function adminEditForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsSuperAdmin()){
                ViewHelper::render("view/tableData/AdminEditViewer.php", [
                    "pageTitle" => "Posodobi izbranega uporabnika",
                    "formAction" => "admins/edit",
                    "data" => UserModel::get($id),
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function adminAttributesEdit($id) {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "surname" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (Validation::checkValues($data)) {
            $data["idUserWhoIsEditing"] = User::getId();
            $data["idUserToBeEdited"] = $id;
            if (UserModel::updateAttributes($data)){
                self::adminList("Success", "Uspešno ste spremenili podatke izbranega uporabnika.");
            } else {
                self::adminEditForm($id, "Failure", "Napaka, vneseno geslo se ne ujema z geslom v bazi. Poskusite znova.");
            }
        } else {
            self::adminEditForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function adminPasswordEdit($id) {
        $data = filter_input_array(INPUT_POST, [
            "password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "new-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "retype-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (Validation::checkValues($data)) {
            if ($data["new-password"]==$data["retype-password"]) {
                $data["idUserWhoIsEditing"] = User::getId();
                $data["idUserToBeEdited"] = $id;
                if (UserModel::changePassword($data)){
                    self::adminList("Success", "Uspešno ste spremenili geslo izbranega uporabnika.");
                } else {
                    self::adminEditForm($id, "Failure", "Napaka, vneseno geslo se ne ujema z geslom v bazi. Poskusite znova.");
                }
            } else {
                self::adminEditForm($id, "Failure", "Napaka, gesla niso enaka. Poskusite znova.");
            }
        } else {
            self::adminEditForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function toggleAdminsActivity() {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsSuperAdmin()){
                $data = filter_input_array(INPUT_POST, [
                    'toogleId' => [
                        'filter' => FILTER_VALIDATE_INT,
                        'options' => [
                            'min_range' => 2
                        ]
                    ]
                ]);
                if (Validation::checkValues($data)) {
                    UserModel::toogleActivity($data['toogleId']);
                    self::adminList("Success", "Uspešno ste spremenili aktivnost uporabnika.");
                } else {
                    self::adminList("Failure", "Napaka pri spreminjanju aktivnosti uporabnika.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
}