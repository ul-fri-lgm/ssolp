<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once("model/SubcategoryModel.php");
require_once("model/UserModel.php");
require_once("model/User.php");
require_once("includes/Validation.php");
require_once("ViewHelper.php");

class BasicUserController {
    public static function homeForm() {
        ViewHelper::render("view/HomeViewer.php", [
            "subcategories" => SubcategoryModel::getAll()
        ]);
    }

    public static function editOwnProfileAttributes() {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "surname" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "email" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (Validation::checkValues($data)) {
            $data["id"] = User::getId();
            if (UserModel::updateOwnAttributes($data)) {
                self::editOwnProfileForm("Success", "Uspešno ste spremenili podatke svojega profila.");
            } else {
                self::editOwnProfileForm("Failure", "Napaka, vneseno geslo se ne ujema z geslom v bazi. Poskusite znova.");
            }
        } else {
            self::editOwnProfileForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function editOwnProfilePassword() {
        $data = filter_input_array(INPUT_POST, [
            "old-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "new-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "retype-password" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (Validation::checkValues($data)) {
            if ($data["new-password"]==$data["retype-password"]) {
                $data["id"] = User::getId();
                if (UserModel::changeOwnPassword($data)) {
                    self::editOwnProfileForm("Success", "Uspešno ste spremenili geslo svojega profila.");
                } else {
                    self::editOwnProfileForm("Failure", "Napaka, vneseno geslo se ne ujema z geslom v bazi. Poskusite znova.");
                }
            } else {
                self::editOwnProfileForm("Failure", "Napaka, gesla niso enaka. Poskusite znova.");
            }
        } else {
            self::editOwnProfileForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function editOwnProfileForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            ViewHelper::render("view/ProfileEditViewer.php", [
                "pageTitle" => "Posodobi podatke svojega profila",
                "data" => UserModel::get(User::getId()),
                "formAction" => "editProfile",
                "status" => $status,
                "message" => $message
            ]);
        }else{
            ViewHelper::error401();
        }
    }
	
    public static function ShowAbout(){
        ViewHelper::render("view/AboutViewer.php", [
            "subcategories" => SubcategoryModel::getAll(),
            "pageTitle" => "O projektu"
        ]);
    }
    public static function ShowAboutDictionary(){
        ViewHelper::render("view/AboutDictionaryViewer.php", [
            "subcategories" => SubcategoryModel::getAll(),
            "pageTitle" => "O slovarju"
        ]);
    }
    
    public static function SearchDatabase(){
        $data = filter_input_array(INPUT_GET, [
            "keyword" => FILTER_SANITIZE_SPECIAL_CHARS
        ]);
        
        $result = KeywordModel::Search($data["keyword"]);
        foreach ($result as $id => $arr){
            echo "<li class='LiResult' id=".$arr['idKeyword']." tabindex='".$id."'>".$arr['word']."</li>";
        }
    }

    public static function KeywordDetails(){
        $data = array();
        if(isset($_GET["KeywordID"]) && $_GET["KeywordID"] != ""){
            $data = filter_input_array(INPUT_GET, [
                'KeywordID' => [
                    'filter' => FILTER_VALIDATE_INT,
                    'options' => ['min_range' => 1]
                ]
            ]);
        }else if(isset($_GET["KeywordName"]) && $_GET["KeywordName"] != ""){
            $data = filter_input_array(INPUT_GET, [
                'KeywordName' => [
                    'filter' => FILTER_SANITIZE_SPECIAL_CHARS
                ]
            ]);
        }
        if (Validation::checkValues($data)) {
            if(isset($data["KeywordID"])){
                $id = $data["KeywordID"];
            }else{
                $id = KeywordModel::getKeywordID($data["KeywordName"]);
                if($id == null) {
                    ViewHelper::render("view/KeywordViewer.php", [
                        "subcategories" => SubcategoryModel::getAll(),
                        "data" => NULL
                    ]);
                    return;
                }
            }
            $result = KeywordModel::getKeywordDetails($id);
            $pictures = KeywordModel::getKeywordPicture($id);
            $videos = KeywordModel::getKeywordVideo($id);
            $audios = KeywordModel::getKeywordAudio($id);
            $keywordConnections = KeywordModel::getKeywordConnections($id);
            $nadpomenke = array();
            $sestavniDeli = array();
            $orodjaZaVzdrzevanje = array();
            $besedeIstiKoren = array();
            $sopomenke = array();
            $vrste = array();
            foreach($keywordConnections as $idDict => $vrsta){
                if($vrsta["name"] == "Nadpomenke"){
                    $nadpomenke[$vrsta["word"]] = $vrsta["idKeyword"];
                }else if($vrsta["name"] == "Sopomenke"){
                    $sopomenke[$vrsta["word"]] = $vrsta["idKeyword"];
                }else if($vrsta["name"] == "Sestavni deli"){
                    $sestavniDeli[$vrsta["word"]] = $vrsta["idKeyword"];
                }else if($vrsta["name"] == "Orodje"){
                    $orodjaZaVzdrzevanje[$vrsta["word"]] = $vrsta["idKeyword"];
                }else if($vrsta["name"] == "Besedje z istim korenom"){
                    $besedeIstiKoren[$vrsta["word"]] = $vrsta["idKeyword"];
                }else if($vrsta["name"] == "Vrste"){
                    $vrste[$vrsta["word"]] = $vrsta["idKeyword"];
                }
            }
            ViewHelper::render("view/KeywordViewer.php", [
                "subcategories" => SubcategoryModel::getAll(),
                "data" => $result,
                "pictures" => $pictures,
                "videos" => $videos,
                "audios" => $audios,
                "Nadpomenke" => $nadpomenke,
                "SestavniDeli" => $sestavniDeli,
                "OrodjaZaVzdrzevanje" => $orodjaZaVzdrzevanje,
                "BesedeIstiKoren" => $besedeIstiKoren,
                "Sopomenke" => $sopomenke,
                "Vrste" => $vrste
            ]);
        } else {
            ViewHelper::render("view/KeywordViewer.php", [
                "subcategories" => SubcategoryModel::getAll(),
                "data" => NULL
            ]);
        }
    }

    public static function GetAllKeywordsFromSubcategory($id){
        $result = KeywordModel::GetAllKeywordsFromSubcategory($id);
        ViewHelper::render("view/AllKeywordsInSubcategoryViewer.php", [
            "chosen" => SubcategoryModel::get($id),
            "subcategories" => SubcategoryModel::getAll(),
            "besede" => $result
        ]);
    }
}