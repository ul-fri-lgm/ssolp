<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once("model/CategoryModel.php");
require_once("model/SubcategoryModel.php");
require_once("model/KeywordModel.php");
require_once("model/ConnectionModel.php");
require_once("model/TypeOfConnectionModel.php");
require_once("model/PictureModel.php");
require_once("model/RecordModel.php");
require_once("model/KeywordRecordModel.php");
require_once("model/ImportModel.php");
require_once("model/UserModel.php");
require_once("model/User.php");
require_once("includes/Validation.php");
require_once("ViewHelper.php");

class AdminController {
    public static function CategoryList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsAdmin()) {
                $allData = CategoryModel::getAllPlusInactive();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Ustvarite vsaj enega.";
                }
                
                ViewHelper::render("view/tableData/CategoryListViewer.php", [
                    "pageTitle" => "Seznam vseh kategorij",
                    "allData" => $allData,
                    "formAction" => "categories",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function CategoryAddForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/tableData/CategoryAddViewer.php", [
                    "pageTitle" => "Ustvari novo kategorijo",
                    "formAction" => "categories/add",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function CategoryAdd() {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            if (CategoryModel::isUnique($data["name"])) {
                CategoryModel::insert($data["name"]);
                self::CategoryList("Success", "Uspešno ste ustvarili novo kategorijo.");
            } else {
                self::CategoryAddForm("Failure", "Napaka, kategorija s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::CategoryAddForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function CategoryEditForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/tableData/CategoryEditViewer.php", [
                    "pageTitle" => "Posodobi izbrano kategorijo",
                    "formAction" => "categories/edit",
                    "data" => CategoryModel::get($id),
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function CategoryEdit($id) {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            if (CategoryModel::isUnique($data["name"])) {
                CategoryModel::update($id, $data["name"]);
                self::CategoryList("Success", "Uspešno ste posodobili izbrano kategorijo.");
            } else {
                self::CategoryEditForm($id, "Failure", "Napaka, kategorija s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::CategoryEditForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function CategoryToggleActivity() {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $data = filter_input_array(INPUT_POST, [
                    'toogleId' => [
                        'filter' => FILTER_VALIDATE_INT,
                        'options' => [
                            'min_range' => 1
                        ]
                    ]
                ]);

                if (Validation::checkValues($data)) {
                    CategoryModel::toogleActivity($data['toogleId']);
                    self::CategoryList("Success", "Uspešno ste spremenili aktivnost kategorije.");
                } else {
                    self::CategoryList("Failure", "Napaka pri spreminjanju aktivnosti kategorije.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function SubcategoryList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsAdmin()) {
                $allData = SubcategoryModel::getAllPlusInactive();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Ustvarite vsaj enega.";
                }

                ViewHelper::render("view/tableData/SubcategoryListViewer.php", [
                    "pageTitle" => "Seznam vseh podkategorij",
                    "allData" => $allData,
                    "formAction" => "subcategories",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function SubcategoryAddForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allCategories = CategoryModel::getAll();
                if ($message==null && empty($allCategories)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov o kategorijah. Ustvarite vsaj enega.";
                }

                ViewHelper::render("view/tableData/SubcategoryAddViewer.php", [
                    "pageTitle" => "Ustvari novo podkategorijo",
                    "formAction" => "subcategories/add",
                    "allCategories" => $allCategories,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function SubcategoryAdd() {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "idCategory" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        if (Validation::checkValues($data)) {
            if (SubcategoryModel::isUniqueByName($data["name"])) {
                SubcategoryModel::insert($data["idCategory"], $data["name"]);
                self::SubcategoryList("Success", "Uspešno ste ustvarili novo podkategorijo.");
            } else {
                self::SubcategoryAddForm("Failure", "Napaka, podkategorija s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::SubcategoryAddForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function SubcategoryEditForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/tableData/SubcategoryEditViewer.php", [
                    "pageTitle" => "Posodobi izbrano podkategorijo",
                    "formAction" => "subcategories/edit",
                    "data" => SubcategoryModel::get($id),
                    "allCategories" => CategoryModel::getAll(),
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function SubcategoryEdit($id) {
        $data = filter_input_array(INPUT_POST, [
            "name" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "idCategory" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        if (Validation::checkValues($data)) {
            if (SubcategoryModel::isUnique($data["idCategory"], $data["name"])) {
                SubcategoryModel::update($id, $data["idCategory"], $data["name"]);
                self::SubcategoryList("Success", "Uspešno ste posodobili izbrano podkategorijo.");
            } else {
                self::SubcategoryEditForm($id, "Failure", "Napaka, podkategorija s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::SubcategoryEditForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function SubcategoryToggleActivity() {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $data = filter_input_array(INPUT_POST, [
                    'toogleId' => [
                        'filter' => FILTER_VALIDATE_INT,
                        'options' => [
                            'min_range' => 1
                        ]
                    ]
                ]);

                if (Validation::checkValues($data)) {
                    SubcategoryModel::toogleActivity($data['toogleId']);
                    self::SubcategoryList("Success", "Uspešno ste spremenili aktivnost podkategorije.");
                } else {
                    self::SubcategoryList("Failure", "Napaka pri spreminjanju aktivnosti podkategorije.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function KeywordList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsAdmin()) {
                $allData = KeywordModel::getAll();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Ustvarite vsaj enega.";
                }

                ViewHelper::render("view/tableData/KeywordListViewer.php", [
                    "pageTitle" => "Seznam vseh gesel",
                    "numOfKeywordsWithoutPicture" => KeywordModel::getNumberOfKeywordsWithoutPicture(),
                    "allData" => $allData,
                    "formAction" => "keywords",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordAddForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allSubcategories = SubcategoryModel::getAll();
                if ($message==null && empty($allSubcategories)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov o podkategorijah. Ustvarite vsaj enega.";
                }

                ViewHelper::render("view/tableData/KeywordAddViewer.php", [
                    "pageTitle" => "Ustvari novo geslo",
                    "formAction" => "keywords/add",
                    "allSubcategories" => $allSubcategories,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordAdd() {
        $data = filter_input_array(INPUT_POST, [
            "word" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "idSubcategory" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ],
            "pronunciation" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "grammar" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "meaning" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (isset($_POST["etymology"]) && $_POST["etymology"] != "") {
            $data = $data + filter_input_array(INPUT_POST, [
                "etymology" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
            ]);
        }

        if (Validation::checkValues($data)) {
            if (KeywordModel::isUnique($data)) {
                KeywordModel::insert($data);
                self::KeywordList("Success", "Uspešno ste ustvarili novo geslo.");
            } else {
                self::KeywordAddForm("Failure", "Napaka, geslo s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::KeywordAddForm("Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function KeywordEditForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/tableData/KeywordEditViewer.php", [
                    "pageTitle" => "Posodobi izbrano geslo",
                    "formAction" => "keywords/edit",
                    "data" => KeywordModel::get($id),
                    "allSubcategories" => SubcategoryModel::getAll(),
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordEdit($id) {
        $data = filter_input_array(INPUT_POST, [
            "word" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "idSubcategory" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ],
            "pronunciation" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "grammar" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
            "meaning" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);
        if (isset($_POST["etymology"]) && $_POST["etymology"] != "") {
            $data = $data + filter_input_array(INPUT_POST, [
                "etymology" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
            ]);
        }

        if (Validation::checkValues($data)) {
            $data["idKeyword"] = $id;
            if (KeywordModel::isUniqueIfAlreadyCreated($data)) {
                KeywordModel::update($data);
                self::KeywordList("Success", "Uspešno ste posodobili izbrano geslo.");
            } else {
                self::KeywordEditForm($id, "Failure", "Napaka, geslo s tem imenom že obstaja. Poskusite znova.");
            }
        } else {
            self::KeywordEditForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function KeywordToggleActivity() {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $data = filter_input_array(INPUT_POST, [
                    'toogleId' => [
                        'filter' => FILTER_VALIDATE_INT,
                        'options' => [
                            'min_range' => 1
                        ]
                    ]
                ]);

                if (Validation::checkValues($data)) {
                    KeywordModel::toogleActivity($data['toogleId']);
                    self::KeywordList("Success", "Uspešno ste spremenili aktivnost gesla.");
                } else {
                    self::KeywordList("Failure", "Napaka pri spreminjanju aktivnosti gesla.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordConnectionList($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allData = ConnectionModel::getKeywordConnections($id);
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "Ni povezave v podatkovni bazi. Ustvarite eno!";
                }

                $chosenKeyword = KeywordModel::get($id);
                ViewHelper::render("view/tableData/KeywordConnectionListViewer.php", [
                    "pageTitle" => "Seznam vseh povezav za izbrano geslo '".$chosenKeyword["word"]."'",
                    "formAction" => "keywords/connections/".$id,
                    "chosenKeywordConnections" => $allData,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordConnectionAddForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allData = KeywordModel::getAll(); //KeywordModel::getAllNotConnectedWith($id);
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "Ne obstajajo gesla, s katerimi bi radi povezali izbrano geslo. Ustvarite novo!";
                }

                ViewHelper::render("view/tableData/KeywordConnectionAddViewer.php", [
                    "pageTitle" => "Poveži izbrano geslo",
                    "formAction" => "keywords/connections/".$id."/add",
                    "chosenKeyword" => KeywordModel::get($id),
                    "allKeywords" => $allData,
                    "allTypesOfConnections" => TypeOfConnectionModel::getAll(),
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function KeywordConnectionAdd($id) {
        $data = filter_input_array(INPUT_POST, [
            "idKeyword2" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ],
            "idTypeOfConnection" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ],
            "wayOfConnection" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
        ]);

        if (Validation::checkValues($data)) {
            $data["idKeyword1"] = $id;
            if (ConnectionModel::insert($data)) {
                $direction = ($data["wayOfConnection"] === "one-way") ? "enosmerno" : (($data["wayOfConnection"] === "both-ways") ? "dvosmerno" : "?smerna");
                $direction = "<b>" . $direction . "</b>";
                self::KeywordConnectionList($id, "Success", "Uspešno ste ustvarili novo " . $direction . " povezavo med gesloma.");
            } else {
                self::KeywordConnectionList($id, "Failure", "Napaka, povezava med gesli že obstaja.");
            }
        } else {
            self::KeywordConnectionAddForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }

    public static function KeywordConnectionDelete($id1, $id2, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ConnectionModel::delete($id1, $id2);
                self::KeywordConnectionList($id1, "Success", "Uspešno ste izbrišali povezavo med gesli.");
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function UploadFileForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/UploadFileViewer.php", [
                    "pageTitle" => "Naloži datoteko",
                    "formAction" => "uploadFile",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }

    public static function UploadFile() {
        if (!isset($_FILES["fileToUpload"])) {
            self::UploadFileForm("Failure", "Napaka, vaša datoteka je prevelika.");
        } else {
            $target_dir = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . "static" . DIRECTORY_SEPARATOR;
            $fileType = strtolower(pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION));
            $allowedImageTypes = array("jpg", "jpeg", "png", "nef");
            $allowedVideoTypes = array("avi", "mp4", "flv", "vmw", "mov");
            $allowedAudioTypes = array("wav", "mp3", "vma");
            
            $allowedFile = false; $typeOfFile = false;
            if (in_array($fileType, $allowedImageTypes)) {
                $target_dir = $target_dir . "images" . DIRECTORY_SEPARATOR;
                $allowedFile = true; $typeOfFile='i';
            } else if (in_array($fileType, $allowedVideoTypes)) {
                $target_dir = $target_dir . "videos" . DIRECTORY_SEPARATOR;
                $allowedFile = true; $typeOfFile='v';
            } else if (in_array($fileType, $allowedAudioTypes)) {
                $target_dir = $target_dir . "audios" . DIRECTORY_SEPARATOR;
                $allowedFile = true; $typeOfFile='a';
            }
            if (!$allowedFile) {
                self::UploadFileForm("Failure", "Napaka, dovoljeni so samo naslednji formati:<br>" . 
                    "- JPG, JPEG, PNG & NEF za fotografije<br>" .
                    "- AVI, MP4, FLV, VMW & MOV za video<br>" .
                    "- WAV, MP3 & VMA datoteke za avdio posnetke."
                );
            } else {
                $targetFile = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                if (file_exists($targetFile)) {
                    self::UploadFileForm("Failure", "Napaka, datoteka " . basename($_FILES["fileToUpload"]["name"]) . " že obstaja.");
                } else {
                    if ($typeOfFile=='v' || $typeOfFile=='a') {
                        $data = filter_input_array(INPUT_POST, [
                            "transcription" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS],
                            "translation" => ["filter" => FILTER_SANITIZE_SPECIAL_CHARS]
                        ]);
                        if (!Validation::checkValues($data)) {
                            self::UploadFileForm("Failure", "Napaka. Za nalaganje avdio ali video datoteke morata biti izpolnjeni polji Narečno in Knjižno. Trenutni vnos ni dovoljen. Prosimo, poskusite znova.");
                            return;
                        }
                    }
                    
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                        if ($typeOfFile=='i') {
                            PictureModel::insert(["name" => basename($_FILES["fileToUpload"]["name"])]);
                        } else if ($typeOfFile=='v' || $typeOfFile=='a') {
                            RecordModel::insert([
                                "name" => basename($_FILES["fileToUpload"]["name"]),
                                "transcription" => $data["transcription"],
                                "translation" => $data["translation"],
                                "type" => $typeOfFile
                            ]);
                        }
                        self::UploadFileForm("Success", "Datoteka " . basename($_FILES["fileToUpload"]["name"]) . " je bila uspešno naložena.");
                    } else {
                        self::UploadFileForm("Failure", "Napaka pri nalaganju vaše datoteke.");
                    }
                }
            }
        }
    }
    
    public static function PictureList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsAdmin()) {
                $allData = PictureModel::getAll();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Naložite vsaj enega.";
                }

                ViewHelper::render("view/tableData/PictureListViewer.php", [
                    "pageTitle" => "Seznam vseh slik",
                    "allData" => $allData,
                    "formAction" => "pictures",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function PictureDelete($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $file = PictureModel::get($id)["name"];
                $target_dir = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . "static" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR;
                if (file_exists($target_dir . $file)) {
                    if (unlink($target_dir . $file)) {
                        PictureModel::delete($id);
                        self::PictureList("Success", "Uspešno ste izbrišali sliko " . $file);
                    } else {
                        self::PictureList("Failure", "Napaka, poskusite znova.");
                    }
                } else {
                    PictureModel::delete($id);
                    self::PictureList("Failure", "Napaka, slika ne obstaja. Kontaktirajte skrbnika.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function PictureAddOrEditKeywordForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allData = KeywordModel::getAll();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov o geslih. Ustvarite vsaj enega.";
                }

                ViewHelper::render("view/tableData/PictureAddOrEditKeywordViewer.php", [
                    "pageTitle" => "Poveži izbrano sliko",
                    "formAction" => "pictures/".$id."/keywords",
                    "chosenPicture" => PictureModel::get($id),
                    "allKeywords" => $allData,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function PictureAddOrEditKeyword($id) {
        $data = filter_input_array(INPUT_POST, [
            "idKeyword" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        if (Validation::checkValues($data)) {
            $data["idPicture"] = $id;
            PictureModel::updateKeyword($data);
            self::PictureList("Success", "Uspešno ste povezali oz. spremenili povezavo med sliko in geslom.");
        } else {
            self::PictureAddOrEditKeywordForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function RecordList($status = null, $message = null) {
        if (User::isLoggedIn()) {
            if (User::isLoggedInAsAdmin()) {
                $allData = RecordModel::getAll();
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "V podatkovni bazi ni podatkov. Naložite vsaj enega.";
                }

                ViewHelper::render("view/tableData/RecordListViewer.php", [
                    "pageTitle" => "Seznam vseh posnetkov",
                    "allData" => $allData,
                    "formAction" => "records",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function RecordDelete($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $record = RecordModel::get($id);
                $file = $record["name"];
                if ($record["type"] == "v") $target_dir = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . "static" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR;
                else if ($record["type"] == "a") $target_dir = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . "static" . DIRECTORY_SEPARATOR . "audios" . DIRECTORY_SEPARATOR;
                else { self::RecordList("Failure", "Napaka, tip posnetka ni znan. Kontaktirajte skrbnika."); return; }
                
                if (file_exists($target_dir . $file)) {
                    if (unlink($target_dir . $file)) {
                        RecordModel::delete($id);
                        self::RecordList("Success", "Uspešno ste izbrišali posnetek " . $file);
                    } else {
                        self::RecordList("Failure", "Napaka, poskusite znova.");
                    }
                } else {
                    RecordModel::delete($id);
                    self::RecordList("Failure", "Napaka, posnetek ne obstaja. Kontaktirajte skrbnika.");
                }
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function KeywordRecordListForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allData = KeywordRecordModel::getAllKeywordsOfRecord($id);
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "Ni povezave v podatkovni bazi. Ustvarite eno!";
                }

                $chosenRecord = RecordModel::get($id);
                ViewHelper::render("view/tableData/RecordKeywordListViewer.php", [
                    "pageTitle" => "Seznam vseh gesel za izbran posnetek '".$chosenRecord["name"]."'",
                    "formAction" => "records/".$id."/keywords",
                    "chosenKeywordsOfRecord" => $allData,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function KeywordRecordAddForm($id, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                $allData = KeywordModel::getAllNotLinkedWithRecord($id);
                if ($message==null && empty($allData)) {
                    $status = "Info";
                    $message = "Ne obstajajo gesla, s katerimi bi radi povezali izbrani posnetek. Ustvarite novo!";
                }

                ViewHelper::render("view/tableData/RecordKeywordAddViewer.php", [
                    "pageTitle" => "Poveži izbran posnetek",
                    "formAction" => "records/".$id."/keywords/add",
                    "chosenRecord" => RecordModel::get($id),
                    "allKeywords" => $allData,
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function KeywordRecordAdd($id) {
        $data = filter_input_array(INPUT_POST, [
            "idKeyword" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        if (Validation::checkValues($data)) {
            $data["idRecord"] = $id;
            if (KeywordRecordModel::insert($data)) {
                self::KeywordRecordListForm($id, "Success", "Uspešno ste ustvarili novo povezavo med posnetkom in geslom.");
            } else {
                self::KeywordRecordListForm($id, "Failure", "Napaka, povezava med posnetkom in geslom že obstaja.");
            }
        } else {
            self::KeywordRecordAddForm($id, "Failure", "Napaka, vnos ni veljaven. Poskusite znova.");
        }
    }
    
    public static function KeywordRecordDelete($id1, $id2, $status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                KeywordRecordModel::delete($id1, $id2);
                self::KeywordRecordListForm($id1, "Success", "Uspešno ste izbrisali povezavo med posnetkom in geslom.");
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function ImportDataForm($status = null, $message = null) {
        if (User::isLoggedIn()){
            if (User::isLoggedInAsAdmin()){
                ViewHelper::render("view/ImportDataViewer.php", [
                    "pageTitle" => "Izberi datoteko za uvoz podatkov",
                    "formAction" => "importData",
                    "status" => $status,
                    "message" => $message
                ]);
            }else{
                ViewHelper::error403();
            }
        }else{
            ViewHelper::error401();
        }
    }
    
    public static function ImportData() {
        $target_file = basename($_FILES["fileToUpload"]["name"]);
        $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if ($fileType == "txt") {
            $fileContent = filter_var(file_get_contents($_FILES["fileToUpload"]["tmp_name"]), FILTER_SANITIZE_SPECIAL_CHARS);
            
            $splitted = explode("&#13;&#10;", $fileContent); // CRLF
            $splitted = array_filter($splitted);    // Removes empty lines
            $splitted = array_values($splitted);    // Re-Index the array
            // echo '<pre>' . var_export($splitted, true) . '</pre>';
            
            if($_POST["choose"] == "v" || $_POST["choose"] == "a") {
                $sizeSplitted = count($splitted);
                for($i = 0; $i < $sizeSplitted; $i++) {
                    $splitted2 = explode(";", $splitted[$i]);
                    ImportModel::insertRecord([
                        "name" => trim($splitted2[0]),
                        "transcription" => trim($splitted2[1]),
                        "translation" => trim($splitted2[2]),
                        "type" => $_POST["choose"],
                    ]);
                }
                self::RecordList("Success", "Uspešno ste naložili datoteko.");
            } else if ($_POST["choose"] == "p") {
                $sizeSplitted = count($splitted);
                for($i = 0; $i < $sizeSplitted; $i++) {
                    ImportModel::insertPicture([
                        "name" => trim($splitted[$i])
                    ]);
                }
                self::PictureList("Success", "Uspešno ste naložili datoteko.");
            }
        } else {
            self::ImportDataForm("Failure", "Napaka, datoteka ni ustreznega tipa. Uvozite lahko le datoteke v formatu .txt");
        }
    }
}
