/* Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. */

$(window).on('load', function(){
    $(".PictureKeywordSubcategory").each(function(){
        var height = this.naturalHeight;
        var width = this.naturalWidth;
        if(height > width){
            var RealWidth = $(this).parent().width();
            var RealHeight = $(this).parent().height();
            var position = $(this).parent().position();
			//$(this).height(RealWidth);
            //$(this).width(RealHeight);
            $(this).width(RealWidth);
            //$(this).addClass("rotate");
            var ThisPosition = $(this).position();
            //$(this).css({"margin-top": -ThisPosition.top-position.top});
            $(this).css({"margin-top": ThisPosition.top});
        }
    });
})
