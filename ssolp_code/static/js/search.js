/* Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. */

$( document ).ready(function() {
//	var active = "1";
	var ActiveItem = 0;
	var tmp = window.location.pathname.split('/');
	var BASE_URL = "/ssolp/index.php/Search";
	function getSearchResults(){
		var counter = 0;
		$('#SearchResults > li').each(function () { 
			counter = counter + 1;
		 });
		 return counter;
	}
    $("#SearchInput").on('input',function(e){
//		active = "1";
		var value = $.trim($("#SearchInput").val());
		if(value.length > 0){
			$.ajax({
				type: "GET",
				url: BASE_URL,
				data: { "keyword": value },
				success: function(data) {
					$("#SearchResults").html(data);
					$("#SearchResults").css("display", "block");
				}
			});
		}else{
			$("#SearchResults").css("display", "none");
			$("#SearchResults").empty();
		}
	});
    $("body").on("click", ".LiResult", function(e){
		var id = this.id;
		$("#KeywordID").val(id);
		var value = $(this).text();
		$("#SearchInput").val(value);
		$("#SearchForm").submit();
	});
	$( "#SearchInput" ).keypress(function( event ) {
		if (event.which == 13) { // Enter
			if($.trim( $('#SearchInput').val() ) != ""){
				var value = $("#SearchInput").val();
				$("#KeywordName").val(value);
				$("#SearchForm").submit();
			}else{
				event.preventDefault();
			}
		}
	});
	function RemoveActiveSearch(){
		$('#SearchResults > li').each(function () {
			$(this).removeClass("ActiveSearch");
		});
	}
	function returnActiveElement(){
		$('#SearchResults > li').each(function () {
			if($(this).hasClass("ActiveSearch")){
				return this;
			}
		});
	}
	function scrollList() {
		var list = $('#SearchResults'); // targets the <ul>
		var first = list.firstChild; // targets the first <li>
		var maininput = $('#SearchInput');  // targets the input, which triggers the functions populating the list
		document.onkeydown = function(e) { // listen to keyboard events
		  switch (e.keyCode) {
			case 38: // if the UP key is pressed
			  if (document.activeElement == (maininput || first)) { break; } // stop the script if the focus is on the input or first element
			  else { document.activeElement.parentNode.previousSibling.firstChild.addClass("ActiveSearch"); } // select the element before the current, and focus it
			  break;
			case 40: // if the DOWN key is pressed
			  if (document.activeElement == maininput) { first.firstChild.addClass("ActiveSearch"); } // if the currently focused element is the main input --> focus the first <li>
			  else { document.activeElement.parentNode.nextSibling.firstChild.addClass("ActiveSearch"); } // target the currently focused element -> <a>, go up a node -> <li>, select the next node, go down a node and focus it
			break;
		  }
		}
	  }
	$( "#SearchInput" ).keydown(function(e) {
		if($("#SearchResults").css("display") != "none"){
			var list = $('#SearchResults'); // targets the <ul>
			var first = list.firstChild; // targets the first <li>
			var $ActiveSearchItem = $("#SearchResults > li.ActiveSearch");
			var maininput = $('#SearchInput');  // targets the input, which triggers the functions populating the list

			RemoveActiveSearch();
			if(event.which == 38) { // Up
				if(ActiveItem == 0 && getSearchResults() > 0){
					$("#SearchResults > li:last-child").addClass("ActiveSearch");
					maininput.val($("#SearchResults > li:last-child").text());
					ActiveItem = 1;
					$ActiveSearchItem = $("#SearchResults > li:last-child");
				}else{
					if($ActiveSearchItem.prev().length > 0){
						console.log($ActiveSearchItem.text());
						$ActiveSearchItem.prev().addClass("ActiveSearch");
					}else{
						$("#SearchResults > li:last-child").addClass("ActiveSearch");
						$ActiveSearchItem = $("#SearchResults > li:last-child");
					}
					$ActiveSearchItem = $("#SearchResults > li.ActiveSearch");
					maininput.val($("#SearchResults > li.ActiveSearch").text());
				}
			}else if(event.which == 40){ // Down
				if(ActiveItem == 0 && getSearchResults() > 0){
					$("#SearchResults > li:first-child").addClass("ActiveSearch");
					maininput.val($("#SearchResults > li:first-child").text());
					ActiveItem = 1;
				}else{
					console.log("Da");
					if($ActiveSearchItem.next().length > 0){
						$ActiveSearchItem.next().addClass("ActiveSearch");
					}else{
						$("#SearchResults > li:first-child").addClass("ActiveSearch");
						$ActiveSearchItem = $("#SearchResults > li:first-child");
					}
					$ActiveSearchItem = $("#SearchResults > li.ActiveSearch");
					maininput.val($("#SearchResults > li.ActiveSearch").text());
				}
			}
		}
	});
	$("#SubmitSearch").click(function(){
		var value = $("#SearchInput").val();
		$("#KeywordName").val(value);
		$("#SearchForm").submit();
	});
});