/* Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. */

$( document ).ready(function() {
    var AVIContainerHeight = $("#AVIContainerInner").height();
    function ConfigStructure(){
        $("#KeywordDetails").removeClass("offset-md-1");
        $("#AVIContainer").removeClass("col-md-6");
        $("#AVIContainer").addClass("col-md-7");
    }
    function ConfigStructureBack(){
        $("#KeywordDetails").addClass("offset-md-1");
        $("#AVIContainer").addClass("col-md-6");
        $("#AVIContainer").removeClass("col-md-7");
    }
    function getVideoHeight(){
        var height = 0;
        if($("#VideosCarousel").hasClass("slide")){
            height = $("#VideosCarousel").outerHeight();
        }else{
            height = $("#VideosCarousel").height();
        }
        $("#VideoDetails").height(height);
    }
    $("#Video").click(function(){
        PauseAllVideos();
        PauseAllAudios();
        $("#PictureSlide").addClass("d-none");
        $("#OnePictureImage").addClass("d-none");
        $("#NoPictureImage").addClass("d-none");
        $("#VideosCarousel").removeClass("d-none");
        if($("#VideoDetails").length != 0){
            $("#KeywordDetails").addClass("d-none");
        }
        $("#VideoDetails").removeClass("d-none");
        $("#PicturesCarousel").addClass("d-none");
        $("#AudiosCarousel").addClass("d-none");
        getVideoHeight();
    });
    $("#Audio").click(function(){
        if($("#NoAudio").length == 0){
            ConfigStructure();
            $("#AudiosCarousel").removeClass("NoBackground");
        }
        PauseAllVideos();
        PauseAllAudios();
        $("#PictureSlide").addClass("d-none");
        $("#OnePictureImage").addClass("d-none");
        $("#NoPictureImage").addClass("d-none");
        $("#AudiosCarousel").removeClass("d-none");
        $("#PicturesCarousel").addClass("d-none");
        $("#VideosCarousel").addClass("d-none");
        $("#VideoDetails").addClass("d-none");
        $("#KeywordDetails").removeClass("d-none");
    });
    $("#Image").click(function(){
        if($("#NoPicture").length == 0 && $("#OnePicture").length == 0){
            ConfigStructure();
        }
        PauseAllVideos();
        PauseAllAudios();
        if($("#PicturesCarousel").length == 0) {         
            $("#OnePictureImage").removeClass("d-none");   
            $("#NoPictureImage").removeClass("d-none");
        }else{
            $("#PictureSlide").addClass("d-none");
        }
        $("#PicturesCarousel").removeClass("d-none");
        $("#VideosCarousel").addClass("d-none");
        $("#VideoDetails").addClass("d-none");
        $("#AudiosCarousel").addClass("d-none");
        $("#KeywordDetails").removeClass("d-none");
    });
    $(".close").click(function(){
        ConfigStructureBack();
        PauseAllVideos();
        PauseAllAudios();
        $("#OnePictureImage").removeClass("d-none");
        $("#PictureSlide").removeClass("d-none");
        $("#NoPictureImage").removeClass("d-none");
        $("#KeywordDetails").removeClass("d-none");
        $("#PicturesCarousel").addClass("d-none");
        $("#VideosCarousel").addClass("d-none");
        $("#VideoDetails").addClass("d-none");
        $("#AudiosCarousel").addClass("d-none");
    });
    $("#MoreInfoDiv").click(function(){
        $("#MoreInfoDiv").addClass("d-none");
        $("#MoreInfoKeyword").removeClass("d-none");
    });
    //Video pause on slide
    function PauseAllVideos(){
        $('.KeywordVideo').each(function() {
            $(this).get(0).pause();
        });
    }
    function PauseAllAudios(){
        $('.KeywordAudio').each(function() {
            $(this).get(0).pause();
        }); 
    }
    $(".carousel-control-prev").click(function(){
        PauseAllVideos();
        PauseAllAudios();
    });
    $(".carousel-control-next").click(function(){
        PauseAllVideos();
        PauseAllAudios();
    });
    // Margin-left-md-0 if carousel, else no
    if(!$("#VideosCarousel").hasClass("carousel")){
        $("#VideoDetails").removeClass("ml-md-0");
    }

    //To slide or not to slide, if video playing not to slide, else to slide. Dimitrije Mitić, 2018 AD.
    var ToSlide = true;
    window.setInterval(function(){
        if(!$('#VideosCarousel').hasClass("d-none") && $("#VideosCarousel").hasClass("slide")){
            ToSlide = true;
            $('.KeywordVideo').each(function() {
                if(!this.paused){
                    ToSlide = false;
                }
            });
            if(ToSlide == true){
                $("#VideosCarousel").carousel("cycle");
            }else{
                $("#VideosCarousel").carousel("pause");
            }
        }
        if(!$("#PicturesCarousel").hasClass("d-none")){
            $("#PicturesCarousel").carousel("cycle");
        }else{
            $("#PicturesCarousel").carousel("pause");
        }
    }, 2000);

    //On video slide
    $('#VideosCarousel').on('slide.bs.carousel', function (event) {
        $(".SlideText").addClass("d-none");
        $("#SlideText" + event.to + "1").removeClass("d-none");
        $("#SlideText" + event.to + "2").removeClass("d-none");
        console.log(event.to);
    });
});
$(window).on("load", function() {
    var heightt = 99999999999999999999;
    $("#PicturesCarousel").removeClass("d-none");
    var counter = 1;
    $('.img-carousel').each(function(i, obj) {
        $(obj).addClass("d-block");
        if(counter != 1){
            $(obj).parent().addClass("active");
        }
        if($(obj).height() < heightt){
            heightt = $(obj).height();
        }
        if(counter != 1){
            $(obj).parent().removeClass("active");
        }
        $(obj).removeClass("d-block");
        counter = counter + 1;
    });
    $("#PicturesCarousel").addClass("d-none");
    $("#PictureSlide").css("max-height", "300px");
    $(".img-carousel").css("max-height", heightt);
    $(".img-carousel").css("width", "auto");
    $(".img-carousel").removeClass("d-block");
});