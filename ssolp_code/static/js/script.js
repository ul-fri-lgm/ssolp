/* Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. */

$(window).on('load', function(){
    var heightt = 0;
    $("#DropdownStaraOrodja").addClass("show");
    $("#DropdownStaraOrodja").attr("aria-expanded", "true");
    $("#navbarDropdown").attr("aria-expanded", "true");
    $("#DroppDown").addClass("show");
    $('.dropdown-item').each(function(i, obj) {
        if($(this).height() > heightt){
            heightt = $(this).height();
        }
    });
    $('.dropdown-item').each(function(i, obj) {
        $(this).css("height", heightt + 5);
    });
    $("#DropdownStaraOrodja").removeClass("show");
    $("#DropdownStaraOrodja").attr("aria-expanded", "false");
    $("#navbarDropdown").attr("aria-expanded", "false");
    $("#DroppDown").removeClass("show");
});
$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
            $('#footer').fadeIn();
        } else {
            $('#footer').fadeOut();
        }
    });
})