# SSOLP

## Slovar starega orodja v govoru Loškega Potoka (SSOLP)
Študentski inovativni projekt za družbeno korist (ŠIPK) 2016-18

## Avtorja aplikacije
Avtorja spletne aplikacije sta Aleksandar Nusheski in Dimitrije Mitić, študenta Fakultete za računalništvo in informatiko Univerze v Ljubljani. Aplikacijo sta izdelala v okviru projekta ŠIPK Slovar starega orodja v govoru Loškega Potoka v letu 2018 pod mentorstvom viš. pred. dr. Alenke Kavčič (UL FRI).

## Aplikacija
Slovar starega orodja v govoru Loškega Potoka (SSOLP)
Copyright © 2018 Aleksandar Nusheski (an0548@student.uni-lj.si) & Dimitrije Mitić (dm0935@student.uni-lj.si)

Ta program spada med prosto programje; lahko ga razširjate in/ali spreminjate pod pogoji Splošnega dovoljenja GNU (GNU General Public License), različice 3, kot ga je objavila ustanova Free Software Foundation.
Ta program se razširja v upanju, da bo uporaben, vendar BREZ VSAKRŠNEGA JAMSTVA; tudi brez posredne zagotovitve CENOVNE VREDNOSTI ali PRIMERNOSTI ZA DOLOČEN NAMEN. Za podrobnosti glejte besedilo GNU General Public License.
Skupaj s tem programom bi morali prejeti izvod Splošnega dovoljenja GNU (GNU General Public License). Podrobnosti licence so dostopne tudi na spletni strani http://www.gnu.org/licenses.

Izvorna koda aplikacije je dosegljiva v repozitoriju Bitbucket: https://bitbucket.org/ul-fri-lgm/ssolp.

## Oblikovanje in video
Spletno aplikacijo je oblikovala Elena Plahuta, video in foto gradivo je posnela in uredila Špela Bricman, študentki Fakultete za naravoslovje in tehnologijo Univerze v Ljubljani, v okviru projekta ŠIPK Slovar starega orodja v govoru Loškega Potoka v letu 2018 pod mentorstvom izr. prof. dr. Helene Gabrijelčič Tomc (UL NTF).

## Vsebina
Vsebino Slovarja starega orodja v govoru Loškega Potoka (SSOLP) za temi Orodja za sekača in tesača ter Orodja za spravilo sena so na terenu posneli, uredili, zasnovali in slovarska gesla izdelali študenti Filozofske fakultete Univerze v Ljubljani pod mentorstvom prof. dr. Vere Smole (UL FF): Špela Zupančič, Rok Mrvič, Dejan Gabrovšek in Maja Keržič. Za organizacijo terenskega dela je poskrbela profesorica slovenščine Bogdana Mohar z Osnovne šole dr. Antona Debeljaka Loški Potok, z različnimi zadolžitvami so sodelovali še študentje domačini Karmen Krnc (UL FF), Rok Bartol (VŠUP Grm) in Sebastian Bambič (UL BF). 

## Pisava ZRCola
Narečno besedilo je bilo pripravljeno z vnašalnim sistemom ZRCola (http://zrcola.zrc-sazu.si), ki ga je na Znanstvenoraziskovalnem centru SAZU v Ljubljani (http://www.zrc-sazu.si) razvil Peter Weiss.
