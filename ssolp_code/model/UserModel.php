<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class UserModel {
/*  For creating passwords, use: http://php.net/manual/en/function.password-hash.php
    For checking passwords, use: http://php.net/manual/en/function.password-verify.php */
    
    public static function getUser($email, $password) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("
            SELECT idUser, email, password, type 
            FROM User 
            WHERE email = :email AND activated = 1
        ");
        $statement->bindValue(":email", $email);
        $statement->execute();
        
        $user = $statement->fetch();
        if (password_verify($password, $user["password"])) {
            unset($user["password"]);
            return $user;
        } else {
            return false;
        }
    }
    
    public static function checkEmail($email) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("
            SELECT idUser 
            FROM User 
            WHERE email = :email AND activated = 1
        ");
        $statement->bindValue(":email", $email);
        $statement->execute();
        
        $user = $statement->fetch();
        return ($user != null);
    }
    
    public static function saveToken(array $params){
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            UPDATE User 
            SET resetPwToken = :token, resetPwExpiration = :expiration, resetPwUsed = 0 
            WHERE email = :email
        ");
        $params["token"]=password_hash($params["token"], PASSWORD_BCRYPT);
        $statement->bindParam(":token", $params["token"]);
        $statement->bindParam(":expiration", $params["expiration"]);
        $statement->bindParam(":email", $params["email"]);
        $statement->execute();
    }
    
    public static function checkToken(array $params){
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT resetPwToken 
            FROM User 
            WHERE email = :email AND resetPwExpiration > :expiration AND resetPwUsed = 0
        ");
        $statement->bindParam(":email", $params["email"]);
        $statement->bindParam(":expiration", $params["expiration"]);
        $statement->execute();
        $user = $statement->fetch();
        if ($user != null) {
            return (password_verify($params["token"], $user["resetPwToken"]));
        } else {
            return false;
        }
    }
    
    public static function changePasswordUsingToken(array $params){
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT resetPwToken 
            FROM User 
            WHERE email = :email
        ");
        $statement->bindParam(":email", $params["email"]);
        $statement->execute();
        $user = $statement->fetch();
        if ($user != null && password_verify($params["token"], $user["resetPwToken"])) {
            $statement2 = $db->prepare("
                UPDATE User 
                SET password = :password, resetPwUsed = 1 
                WHERE email = :email
            ");
            $statement2->bindParam(":email", $params["email"]);
            $params["new-password"]=password_hash($params["new-password"], PASSWORD_BCRYPT);
            $statement2->bindParam(":password", $params["new-password"]);
            $statement2->execute();
            return true;
        } else {
            return false;
        }
    }
    
    public static function getAll(){
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT idUser, name, surname, email, activated 
            FROM User 
            WHERE type != 's'
            ORDER BY email
        ");
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT idUser, name, surname, email, activated 
            FROM User 
            WHERE idUser = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }

    public static function isUnique(array $params) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM User 
            WHERE email = :email
        ");
        $statement->bindParam(":email", $params["email"]);
        $statement->execute();
        $result = $statement->fetch();
        
        return ($result == null);
    }

    public static function insert(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO User (name, surname, email, password, type, activated)
            VALUES (:name, :surname, :email, :password, 'a', 1)
        ");
        $statement->bindParam(":name", $params["name"]);
        $statement->bindParam(":surname", $params["surname"]);
        $statement->bindParam(":email", $params["email"]);
        $params["password"]=password_hash($params["password"], PASSWORD_BCRYPT);
        $statement->bindParam(":password", $params["password"]);
        $statement->execute();
    }

    public static function updateAttributes(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT password
            FROM User
            WHERE idUser = :id
        ");
        $statement->bindParam(":id", $params["idUserWhoIsEditing"], PDO::PARAM_INT);
        $statement->execute();
        $user = $statement->fetch();
        if (password_verify($params["password"], $user["password"])) {
            $statement2 = $db->prepare("
                UPDATE User
                SET name = :name, surname = :surname, email = :email
                WHERE idUser = :id
            ");
            $statement2->bindParam(":id", $params["idUserToBeEdited"], PDO::PARAM_INT);
            $statement2->bindParam(":name", $params["name"]);
            $statement2->bindParam(":surname", $params["surname"]);
            $statement2->bindParam(":email", $params["email"]);
            $statement2->execute();
            return true;
        } else {
            return false;
        }
    }

    public static function updateOwnAttributes(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT password
            FROM User
            WHERE idUser = :id
        ");
        $statement->bindParam(":id", $params["id"], PDO::PARAM_INT);
        $statement->execute();
        $user = $statement->fetch();
        if (password_verify($params["password"], $user["password"])) {
            $statement2 = $db->prepare("
                UPDATE User
                SET name = :name, surname = :surname, email = :email
                WHERE idUser = :id
            ");
            $statement2->bindParam(":id", $params["id"], PDO::PARAM_INT);
            $statement2->bindParam(":name", $params["name"]);
            $statement2->bindParam(":surname", $params["surname"]);
            $statement2->bindParam(":email", $params["email"]);
            $statement2->execute();
            return true;
        } else {
            return false;
        }
    }

    public static function changePassword(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT password
            FROM User
            WHERE idUser = :id
        ");
        $statement->bindParam(":id", $params["idUserWhoIsEditing"], PDO::PARAM_INT);
        $statement->execute();
        $user = $statement->fetch();
        if (password_verify($params["password"], $user["password"])) {
            $statement2 = $db->prepare("
                UPDATE User
                SET password = :password
                WHERE idUser = :id
            ");
            $params["new-password"]=password_hash($params["new-password"], PASSWORD_BCRYPT);
            $statement2->bindParam(":id", $params["idUserToBeEdited"], PDO::PARAM_INT);
            $statement2->bindParam(":password", $params["new-password"]);
            $statement2->execute();
            return true;
        } else {
            return false;
        }
    }

    public static function changeOwnPassword(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT password
            FROM User
            WHERE idUser = :id
        ");
        $statement->bindParam(":id", $params["id"], PDO::PARAM_INT);
        $statement->execute();
        $user = $statement->fetch();
        if (password_verify($params["old-password"], $user["password"])) {
            $statement2 = $db->prepare("
                UPDATE User
                SET password = :password
                WHERE idUser = :id
            ");
            $params["new-password"]=password_hash($params["new-password"], PASSWORD_BCRYPT);
            $statement2->bindParam(":id", $params["id"], PDO::PARAM_INT);
            $statement2->bindParam(":password", $params["new-password"]);
            $statement2->execute();
            return true;
        } else {
            return false;
        }
    }

    public static function toogleActivity($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT activated 
            FROM User 
            WHERE idUser = :id
        ");
        $statement->bindValue(":id", $id);
        $statement->execute();
        $is_activated_str = ($statement->fetch())["activated"];

        if ($is_activated_str === '1')
            $is_activated = '0';
        else
            $is_activated = '1';

        $statement2 = $db->prepare("
            UPDATE User 
            SET activated = :is_activated 
            WHERE idUser = :id
        ");
        $statement2->bindValue(":id", $id);
        $statement2->bindParam(":is_activated", $is_activated);
        $statement2->execute();
    }
}