<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class SubcategoryModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT sc.idSubcategory, sc.name AS SubcategoryName, c.name AS CategoryName, sc.activated 
            FROM Subcategory AS sc 
            JOIN Category AS c ON c.idCategory = sc.idCategory
            WHERE sc.activated = 1
            ORDER BY SubcategoryName
        ");
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function getAllPlusInactive() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT sc.idSubcategory, sc.name AS SubcategoryName, c.name AS CategoryName, sc.activated 
            FROM Subcategory AS sc 
            JOIN Category AS c ON c.idCategory = sc.idCategory
            ORDER BY SubcategoryName
        ");
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT sc.idSubcategory, sc.name, c.name AS CategoryName, sc.activated 
            FROM Subcategory AS sc 
            JOIN Category AS c ON c.idCategory = sc.idCategory 
            WHERE idSubcategory = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }

    public static function insert($id, $name) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Subcategory (idCategory, name, activated) 
            VALUES (:id, :name, 1)
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->bindParam(":name", $name);
        $statement->execute();
    }

    public static function update($id, $idCategory, $name) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            UPDATE Subcategory 
            SET idCategory = :idCategory, name = :name 
            WHERE idSubcategory = :id
        ");
        $statement->bindParam(":idCategory", $idCategory, PDO::PARAM_INT);
        $statement->bindParam(":name", $name);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }

    public static function isUniqueByName($name) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Subcategory 
            WHERE name = :name
        ");
        $statement->bindParam(":name", $name);
        $statement->execute();
        $result = $statement->fetch();

        if ($result == null) {
            return true;
        } else {
            return false;
        }
    }

    public static function isUnique($idCategory, $name) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Subcategory 
            WHERE idCategory = :idCategory AND name = :name
        ");
        $statement->bindParam(":idCategory", $idCategory, PDO::PARAM_INT);
        $statement->bindParam(":name", $name);
        $statement->execute();
        $result = $statement->fetch();

        if ($result == null) {
            return true;
        } else {
            return false;
        }
    }

    public static function toogleActivity($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT activated 
            FROM Subcategory 
            WHERE idSubcategory = :id
        ");
        $statement->bindValue(":id", $id);
        $statement->execute();
        $is_activated_str = ($statement->fetch())["activated"];

        if ($is_activated_str === '1')
            $is_activated = '0';
        else
            $is_activated = '1';

        $statement2 = $db->prepare("
            UPDATE Subcategory 
            SET activated = :is_activated 
            WHERE idSubcategory = :id
        ");
        $statement2->bindValue(":id", $id);
        $statement2->bindParam(":is_activated", $is_activated);
        $statement2->execute();
    }
}
