<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class CategoryModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT * 
            FROM Category 
            WHERE activated = 1
            ORDER BY name
        ");
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function getAllPlusInactive() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT * 
            FROM Category
            ORDER BY name
        ");
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Category 
            WHERE idCategory = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }
    
    public static function insert($name) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Category (name, activated) 
            VALUES (:name, 1)
        ");
        $statement->bindParam(":name", $name);
        $statement->execute();
    }
    
    public static function update($id, $name) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            UPDATE Category 
            SET name = :name 
            WHERE idCategory = :id
        ");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function isUnique($name) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Category 
            WHERE name = :name
        ");
        $statement->bindParam(":name", $name);
        $statement->execute();
        $result = $statement->fetch();

        if ($result == null) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function toogleActivity($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT activated 
            FROM Category 
            WHERE idCategory = :id
        ");
        $statement->bindValue(":id", $id);
        $statement->execute();
        $is_activated_str = ($statement->fetch())["activated"];

        if ($is_activated_str === '1')
            $is_activated = '0';
        else
            $is_activated = '1';

        $statement2 = $db->prepare("
            UPDATE Category 
            SET activated = :is_activated 
            WHERE idCategory = :id
        ");
        $statement2->bindValue(":id", $id);
        $statement2->bindParam(":is_activated", $is_activated);
        $statement2->execute();
    }
}
