<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class KeywordModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT k.idKeyword, p.idPicture, word, name AS SubcategoryName, pronunciation, grammar, meaning, etymology, k.activated 
            FROM Keyword AS k 
            JOIN Subcategory AS sc ON k.idSubcategory = sc.idSubcategory 
            LEFT JOIN (
                SELECT idPicture, idKeyword 
                FROM Picture 
                GROUP BY idKeyword
            ) AS p ON k.idKeyword = p.idKeyword
            ORDER BY word
        ");
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function getAllNotConnectedWith($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT idKeyword, word, name AS SubcategoryName, pronunciation, grammar, meaning, etymology, k.activated 
            FROM Keyword AS k 
            JOIN Subcategory AS sc ON k.idSubcategory = sc.idSubcategory 
            WHERE k.idKeyword NOT IN (
                SELECT idKeyword2 
                FROM Connection 
                WHERE idKeyword1 = :id
            ) AND idKeyword != :id AND k.activated = 1
            ORDER BY word
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function getAllNotLinkedWithRecord($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT idKeyword, word 
            FROM Keyword AS k 
            WHERE k.idKeyword NOT IN (
                SELECT idKeyword 
                FROM Keyword_Record 
                WHERE idRecord = :id
            ) AND k.activated = 1
            ORDER BY word
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT idKeyword, word, name AS SubcategoryName, pronunciation, grammar, meaning, etymology, k.activated 
            FROM Keyword AS k 
            JOIN Subcategory AS sc ON k.idSubcategory = sc.idSubcategory 
            WHERE idKeyword = :id AND k.activated = 1
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }
    
    public static function getNumberOfKeywordsWithoutPicture() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT COUNT(k.idKeyword) AS numOfKeywords
            FROM Keyword AS k 
            LEFT JOIN (
                SELECT idPicture, idKeyword 
                FROM Picture 
                GROUP BY idKeyword
            ) AS p ON k.idKeyword = p.idKeyword 
            WHERE p.idPicture is NULL AND k.activated = 1
        ");
        $statement->execute();
        return $statement->fetch()["numOfKeywords"];
    }

    public static function insert(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Keyword (idSubcategory, word, pronunciation, grammar, meaning, etymology, activated) 
            VALUES (:idSubcategory, :word, :pronunciation, :grammar, :meaning, :etymology, 1)
        ");
        $statement->bindParam(":idSubcategory", $params["idSubcategory"], PDO::PARAM_INT);
        $statement->bindParam(":word", $params["word"]);
        $statement->bindParam(":pronunciation", $params["pronunciation"]);
        $statement->bindParam(":grammar", $params["grammar"]);
        $statement->bindParam(":meaning", $params["meaning"]);
        $statement->bindParam(":etymology", $params["etymology"]);
        $statement->execute();
    }
    
    public static function update(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            UPDATE Keyword 
            SET idSubcategory = :idSubcategory, word = :word, pronunciation = :pronunciation, grammar = :grammar, meaning = :meaning, etymology = :etymology 
            WHERE idKeyword = :idKeyword
        ");
        $statement->bindParam(":idKeyword", $params["idKeyword"], PDO::PARAM_INT);
        $statement->bindParam(":idSubcategory", $params["idSubcategory"], PDO::PARAM_INT);
        $statement->bindParam(":word", $params["word"]);
        $statement->bindParam(":pronunciation", $params["pronunciation"]);
        $statement->bindParam(":grammar", $params["grammar"]);
        $statement->bindParam(":meaning", $params["meaning"]);
        $statement->bindParam(":etymology", $params["etymology"]);
        $statement->execute();
    }
    
    public static function isUnique(array $params) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Keyword 
            WHERE word = :word
        ");
        $statement->bindParam(":word", $params["word"]);
        $statement->execute();
        $result = $statement->fetch();

        return ($result == null);
    }
    
    public static function isUniqueIfAlreadyCreated(array $params) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Keyword 
            WHERE word = :word AND idKeyword != :idKeyword
        ");
        $statement->bindParam(":word", $params["word"]);
        $statement->bindParam(":idKeyword", $params["idKeyword"]);
        $statement->execute();
        $result = $statement->fetch();

        return ($result == null);
    }
    
    public static function toogleActivity($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT activated 
            FROM Keyword 
            WHERE idKeyword = :id
        ");
        $statement->bindValue(":id", $id);
        $statement->execute();
        $is_activated_str = ($statement->fetch())["activated"];

        if ($is_activated_str === '1')
            $is_activated = '0';
        else
            $is_activated = '1';

        $statement2 = $db->prepare("
            UPDATE Keyword 
            SET activated = :is_activated 
            WHERE idKeyword = :id
        ");
        $statement2->bindValue(":id", $id);
        $statement2->bindParam(":is_activated", $is_activated);
        $statement2->execute();
    }
	
	    public static function Search($value){
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT k.idKeyword, s.name, k.word, k.activated 
            FROM Keyword AS k, Subcategory AS s 
            WHERE k.idSubcategory = s.idSubcategory AND k.activated = 1 AND k.word LIKE :value
            ORDER BY s.name ASC, k.word ASC LIMIT 7
        ");
        $statement->bindValue(":value", $value . "%");
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    public static function getKeywordDetails($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT k.idKeyword, k.word, k.pronunciation, k.grammar, k.meaning, k.etymology, s.name AS subcategory, s.idSubcategory, c.name AS category
            FROM `Keyword` k, `Subcategory` s, `Category` c
            WHERE k.idSubcategory = s.idSubcategory AND s.idCategory = c.idCategory
            AND k.idKeyword = :id AND k.activated = '1';
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    public static function getKeywordID($Keyword){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT idKeyword 
            FROM Keyword 
            WHERE word = :keyword
        ");

        $statement->bindParam(":keyword", $Keyword);
        $statement->execute();
        $result = $statement->fetch();
        
        if ($result != null) {
            return $result["idKeyword"];
        } else {
            return $result;
        }
    }

    public static function getKeywordConnections($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT k.word, k.idKeyword, toc.name
            FROM `Keyword` k, `Connection` c, `TypeOfConnection` toc
            WHERE c.idKeyword2 = k.idKeyword
            AND c.idTypeOfConnection = toc.idTypeOfConnection
            AND c.idKeyword1 = :id
            AND k.activated = 1
            ORDER BY k.word, toc.name
        ");

        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    public static function getKeywordPicture($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT p.name AS Picture
            FROM `Keyword` k, `Picture` p
            WHERE k.idKeyword = p.idKeyword AND p.activated = '1' AND k.idKeyword = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    public static function getKeywordVideo($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT r.name AS Record, r.transcription AS RecordTranscription, r.translation AS RecordTranslation, r.type as RecordType 
            FROM `Keyword` k, `Record` r, `Keyword_Record` kr
            WHERE kr.idKeyword = k.idKeyword AND kr.idRecord = r.idRecord AND r.activated = '1'
            AND k.idKeyword = :id AND r.type = 'v'
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
    
    public static function getKeywordAudio($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT r.name AS Record, r.transcription AS RecordTranscription, r.translation AS RecordTranslation, r.type as RecordType 
            FROM `Keyword` k, `Record` r, `Keyword_Record` kr
            WHERE kr.idKeyword = k.idKeyword AND kr.idRecord = r.idRecord AND r.activated = '1'
            AND k.idKeyword = :id AND r.type = 'a'
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
    
    public static function GetAllKeywordsFromSubcategory($id){
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT k.idKeyword, k.word, p.picture, s.idSubcategory, s.name AS subcategory, s.idCategory, c.name AS category
            FROM Keyword AS k
            JOIN Subcategory AS s ON k.idSubcategory = s.idSubcategory
            JOIN Category AS c ON s.idCategory = c.idCategory
            JOIN (
                SELECT name AS picture, idKeyword
                FROM Picture
                GROUP BY idKeyword
            ) AS p ON k.idKeyword = p.idKeyword
            WHERE s.idSubcategory = :id AND k.activated = 1
            ORDER BY k.word ASC
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
}