<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class PictureModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT p.*, k.word 
            FROM Picture AS p 
            LEFT JOIN Keyword AS k ON p.idKeyword = k.idKeyword
            ORDER BY p.name
        ");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Picture 
            WHERE idPicture = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }
    
    public static function insert(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Picture (name) 
            VALUES (:name)
        ");
        $statement->bindParam(":name", $params["name"]);
        
        try{
            $statement->execute();
            return true;
        } catch (Exception $e){
            return false;
        }
    }
    
    public static function updateKeyword(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            UPDATE Picture 
            SET idKeyword = :idKeyword 
            WHERE idPicture = :idPicture
        ");
        $statement->bindParam(":idKeyword", $params["idKeyword"], PDO::PARAM_INT);
        $statement->bindParam(":idPicture", $params["idPicture"], PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function delete($id) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            DELETE FROM Picture 
            WHERE idPicture = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
}