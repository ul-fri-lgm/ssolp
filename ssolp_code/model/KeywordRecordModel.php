<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class KeywordRecordModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT * 
            FROM Keyword_Record
        ");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Keyword_Record 
            WHERE idKeywordRecord = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }
    
    public static function getAllKeywordsOfRecord($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT k.idKeyword, k.word 
            FROM Keyword_Record AS kr 
            JOIN Keyword AS k ON k.idKeyword = kr.idKeyword 
            WHERE kr.idRecord = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function insert(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Keyword_Record (idKeyword, idRecord) 
            VALUES (:idKeyword, :idRecord)
        ");
        $statement->bindParam(":idKeyword", $params["idKeyword"], PDO::PARAM_INT);
        $statement->bindParam(":idRecord", $params["idRecord"], PDO::PARAM_INT);
        
        try{
            $statement->execute();
            return true;
        } catch (Exception $e){
            return false;
        }
    }
    
    public static function delete($id1, $id2) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            DELETE FROM Keyword_Record 
            WHERE idRecord = :id1 AND idKeyword = :id2
        ");
        $statement->bindParam(":id1", $id1, PDO::PARAM_INT);
        $statement->bindParam(":id2", $id2, PDO::PARAM_INT);
        $statement->execute();
    }
}