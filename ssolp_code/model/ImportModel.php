<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class ImportModel {
    public static function insertRecord(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Record (name, transcription, translation, type) 
            VALUES (:name, :transcription, :translation, :type)
        ");
        $statement->bindParam(":name", $params["name"]);
        $statement->bindParam(":transcription", $params["transcription"]);
        $statement->bindParam(":translation", $params["translation"]);
        $statement->bindParam(":type", $params["type"]);
        
        try{
            $statement->execute();
            return true;
        } catch (Exception $e){
            var_dump($e);
            return false;
        }
    }
    
    public static function insertPicture(array $params) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            INSERT INTO Picture (name) 
            VALUES (:name)
        ");
        $statement->bindParam(":name", $params["name"]);
        
        try{
            $statement->execute();
            return true;
        } catch (Exception $e){
            var_dump($e);
            return false;
        }
    }
    
}