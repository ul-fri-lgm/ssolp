<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

require_once "DBInit.php";

class ConnectionModel {
    public static function getAll() {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            SELECT * 
            FROM Connection
        ");
        $statement->execute();
        return $statement->fetchAll();
    }
    
    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT * 
            FROM Connection 
            WHERE idConnection = :id
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if ($result != null) {
            return $result;
        } else {
            throw new InvalidArgumentException("No record with id $id");
        }
    }

    public static function getKeywordConnections($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("
            SELECT c.idConnection, c.idKeyword1, k1.word AS k1word, c.idKeyword2, k2.word AS k2word, c.idTypeOfConnection, tc.name AS nameOfConnection 
            FROM Connection AS c 
            JOIN Keyword AS k1 ON k1.idKeyword = c.idKeyword1 
            JOIN Keyword AS k2 ON k2.idKeyword = c.idKeyword2 
            JOIN TypeOfConnection AS tc ON tc.idTypeOfConnection = c.idTypeOfConnection 
            WHERE c.idKeyword1 = :id AND k2.activated = 1
            ORDER BY k2word
        ");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function insert(array $params) {
        $db = DBInit::getInstance();
        $query = null;
        if ($params["wayOfConnection"] == 'one-way') {
            $query = "
                INSERT INTO Connection (idKeyword1, idKeyword2, idTypeOfConnection) 
                VALUES (:idKeyword1, :idKeyword2, :idTypeOfConnection)
            ";
        } else if ($params["wayOfConnection"] == 'both-ways') {
            $query = "
                INSERT INTO Connection (idKeyword1, idKeyword2, idTypeOfConnection) 
                VALUES (:idKeyword1, :idKeyword2, :idTypeOfConnection); 
                INSERT INTO Connection (idKeyword1, idKeyword2, idTypeOfConnection) 
                VALUES (:idKeyword2, :idKeyword1, :idTypeOfConnection)
            ";
        }
        $statement = $db->prepare($query);
        $statement->bindParam(":idKeyword1", $params["idKeyword1"], PDO::PARAM_INT);
        $statement->bindParam(":idKeyword2", $params["idKeyword2"], PDO::PARAM_INT);
        $statement->bindParam(":idTypeOfConnection", $params["idTypeOfConnection"], PDO::PARAM_INT);
        
        try{
            $statement->execute();
            return true;
        } catch (Exception $e){
            return false;
        }
    }
    
    public static function delete($id1, $id2) {
        $db = DBInit::getInstance();
        $statement = $db->prepare("
            DELETE FROM Connection 
            WHERE idKeyword1 = :id1 AND idKeyword2 = :id2; 
            DELETE FROM Connection 
            WHERE idKeyword1 = :id2 AND idKeyword2 = :id1
        ");
        $statement->bindParam(":id1", $id1, PDO::PARAM_INT);
        $statement->bindParam(":id2", $id2, PDO::PARAM_INT);
        $statement->execute();
    }
}