<!-- Slovar starega orodja v govoru Loškega Potoka (SSOLP)
    Copyright (C) 2018  Aleksandar Nusheski (an0548@student.uni-lj.si) &
	                    Dimitrije Mitić (dm0935@student.uni-lj.si)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<?php

// used to store and use session variables
class User {
	public function login($user) {
		$_SESSION["user"] = $user;
	}
	public function logout() {
		session_destroy();
	}
	public function isLoggedIn() {
		return isset($_SESSION["user"]);
	}
	public function isLoggedInAsSuperAdmin() {
		return $_SESSION["user"]["type"]==="s";
	}
	public function isLoggedInAsAdmin() {
		return $_SESSION["user"]["type"]==="a";
	}
	public function isLoggedInAsBasicUser() {
		return $_SESSION["user"]["type"]==="b";
	}
	public function getId() {
		return $_SESSION["user"]["idUser"];
	}
	public function getTypeOfUser() {
		return $_SESSION["user"]["type"];
	}
	public function getURL(){
		return $_SESSION["CURRENT_URL"];
	}
	public function setURL($url){
		$_SESSION["CURRENT_URL"] = $url;
	}
}